#include "log.h"
#include <compiler.h>
#include <contracts.h>
#include <control_status_register.h>
#include <riscv_cause.h>
#include <riscv_csr.h>
#include <stdio.h>

// These are hacks since we are using riscv identifiers in a C program
// targetting x86
#define hpmcounter10 0xC0A
#define hpmcounter11 0xC0B
#define hpmcounter12 0xC0C

#define MACHINE_STATUS_POINTER ((mstatus_t*)&cpu_state->csrs[MSTATUS])

void csr_init(cpu_state_t* cpu_state) {
    (void)cpu_state;
}

uint32_t csr_read(cpu_state_t* cpu_state, ControlStatusRegisterName_t name) {
    // TODO: Technically we should check for permissions here.
    return cpu_state->csrs[name];
}

void csr_update(cpu_state_t* cpu_state) {
    cpu_state->csrs[CYCLE]++;
    cpu_state->csrs[INSTRET]++;
    cpu_state->csrs[INSTRUCTION_CACHE_HIT]++;
    cpu_state->csrs[DATA_CACHE_HIT]++;
}

static void csr_setStatus(cpu_state_t* cpu_state, uint32_t value) {
    cpu_state->csrs[SSTATUS] = value & SSTATUS_WRITE_MASK;
    cpu_state->csrs[MSTATUS] = value & MSTATUS_WRITE_MASK;
}

void csr_write(cpu_state_t*                cpu_state,
               ControlStatusRegisterName_t name,
               uint32_t                    value) {
    switch (name) {
        case MEDELEG:
        case MIDELEG:
        case MTVEC:
        case MSCRATCH:
        case MEPC:
        case MCAUSE:
        case MTVAL:
            if (cpu_state->currentMode != MACHINE_MODE) {
                log_fatalf("Insufficient permissions for %#x mode: %x",
                           name,
                           cpu_state->currentMode);
                cpu_state->halted = true;
                break;
            }
            /* fall-through */
        case STVEC:
        case SSCRATCH:
        case SEPC:
        case SCAUSE:
        case STVAL:
        case SATP:
            if (cpu_state->currentMode != MACHINE_MODE
                && cpu_state->currentMode != SUPERVISOR_MODE) {
                log_fatalf("Insufficient permissions for %#x mode: %x",
                           name,
                           cpu_state->currentMode);
                cpu_state->halted = true;
                break;
            }
            cpu_state->csrs[name] = value;
            break;
        case SSTATUS:
            if (cpu_state->currentMode != MACHINE_MODE
                && cpu_state->currentMode != SUPERVISOR_MODE) {
                log_fatalf("Insufficient permissions for SSTATUS mode: %x",
                           cpu_state->currentMode);
                cpu_state->halted = true;
                break;
            }
            value = (value & SSTATUS_WRITE_MASK)
                    | (cpu_state->csrs[MSTATUS] & ~SSTATUS_WRITE_MASK);
            /* fall through */
        case MSTATUS:
            if (name == MSTATUS && cpu_state->currentMode != MACHINE_MODE) {
                log_fatalf("Insufficient permissions for MSTATUS mode: %x",
                           cpu_state->currentMode);
                cpu_state->halted = true;
                break;
            }
            csr_setStatus(cpu_state, value);
            break;
        default:
            fprintf(stderr,
                    "Invalid CSR Write: %d [%#x] = %d (%#x)\n",
                    name,
                    name,
                    value,
                    value);
            cpu_state->halted = true;
            break;
    }
}

static bool csr_isInterruptCause(uint32_t cause) {
    return cause & CAUSE_INTERRUPT_MASK;
}

static bool csr_interruptDelegated(cpu_state_t* cpu_state,
                                   uint32_t     interruptCause) {
    ASSERT(csr_isInterruptCause(interruptCause));
    return cpu_state->csrs[MIDELEG]
           & (1 << (interruptCause & CAUSE_INDEX_MASK));
}

static bool csr_exceptionDelegated(cpu_state_t* cpu_state,
                                   uint32_t     exceptionCause) {
    ASSERT(!csr_isInterruptCause(exceptionCause));
    return cpu_state->csrs[MEDELEG]
           & (1 << (exceptionCause & CAUSE_INDEX_MASK));
}


/**
 * This function determines if the specified interrupt can arrive "now"
 *
 * The cause must be an interrupt (since exceptions are always accepted)
 */
bool csr_interruptEnabled(cpu_state_t* cpu_state, uint32_t cause) {
    STATIC_ASSERT(sizeof(mstatus_t) == sizeof(uint32_t));
    ASSERT(csr_isInterruptCause(cause));

    mstatus_t* status = MACHINE_STATUS_POINTER;

    switch (cpu_state->currentMode) {
        // All interrupts are always enabled if we are in user-mode
        case USER_MODE:
            return true;
        case SUPERVISOR_MODE:
            // If we are in supervisor mode and the interrupt is delegated to
            // supervisor mode, we use the supervisor interrupts enabled flag to
            // determine if the interrupt is enabled, otherwise the interrupt
            // must be enabled and will be taken in machine mode.

            if (csr_interruptDelegated(cpu_state, cause)) {
                // The interrupt is delegated to supervisor mode
                return status->sie == 1;
            }

            log_warnf(
                "Interrupt not delegated: cause: %#x MIDELEG: %#x MEDELEG: %#x",
                cause,
                cpu_state->csrs[MIDELEG],
                cpu_state->csrs[MEDELEG]);
            return true;
        case MACHINE_MODE:
            return status->mie == 1;
        default:
            log_fatalf("Unknown state: %x", cpu_state->currentMode);
            cpu_state->halted = true;
            return false;
    }
}

void csr_deliverInterrupt(cpu_state_t* cpu_state,
                          uint32_t     cause,
                          uint32_t     val,
                          uint32_t     epc) {
    mstatus_t* status     = MACHINE_STATUS_POINTER;
    mstatus_t  nextStatus = *status;

    switch (cpu_state->currentMode) {
        // All user mode interrupts/exceptions are taken at a higher privilege
        // level
        case USER_MODE: /* fall-through */
        case SUPERVISOR_MODE:
            if ((csr_isInterruptCause(cause)
                 && csr_interruptDelegated(cpu_state, cause))
                || (!csr_isInterruptCause(cause)
                    && csr_exceptionDelegated(cpu_state, cause))) {
                // This should be handled by supervisor mode
                cpu_state->csrs[SCAUSE] = cause;
                cpu_state->csrs[SEPC]   = epc;
                cpu_state->csrs[STVAL]  = val;

                nextStatus.sie  = INTERRUPT_DISABLED;
                nextStatus.spie = status->sie;
                nextStatus.spp  = cpu_state->currentMode;

                cpu_state->currentMode = SUPERVISOR_MODE;
                cpu_state->pc          = cpu_state->csrs[STVEC];
                break;
            }
            // If the interrupt/exception isn't delegated to supervisor mode,
            // take it in machine mode.
            /* fall-through */
        case MACHINE_MODE:
            cpu_state->csrs[MCAUSE] = cause;
            cpu_state->csrs[MEPC]   = epc;
            cpu_state->csrs[MTVAL]  = val;

            nextStatus.mie  = INTERRUPT_DISABLED;
            nextStatus.mpie = status->mie;
            nextStatus.mpp  = cpu_state->currentMode;

            cpu_state->currentMode = MACHINE_MODE;
            cpu_state->pc          = cpu_state->csrs[MTVEC];
            break;
        default:
            log_fatalf("Unknown mode: %x cause: %#x val: %#x epc: %#x",
                       cpu_state->currentMode,
                       cause,
                       val,
                       epc);
            cpu_state->halted = true;
            return;
    }
    csr_setStatus(cpu_state, *(uint32_t*)&nextStatus);
}

void csr_mret(cpu_state_t* cpu_state) {
    mstatus_t* status = MACHINE_STATUS_POINTER;

    mstatus_t nextStatus = *status;

    nextStatus.mie  = status->mpie;
    nextStatus.mpie = INTERRUPT_ENABLED;
    nextStatus.mpp  = USER_MODE;

    cpu_state->currentMode = status->mpp;

    csr_setStatus(cpu_state, *(uint32_t*)&nextStatus);

    cpu_state->pc = cpu_state->csrs[MEPC];
}

void csr_sret(cpu_state_t* cpu_state) {
    mstatus_t* status = MACHINE_STATUS_POINTER;

    mstatus_t nextStatus = *status;

    nextStatus.sie  = status->spie;
    nextStatus.spie = INTERRUPT_ENABLED;
    nextStatus.spp  = USER_MODE;

    cpu_state->currentMode = status->spp;

    csr_setStatus(cpu_state, *(uint32_t*)&nextStatus);

    cpu_state->pc = cpu_state->csrs[SEPC];
}