#ifndef _LOG_H
#define _LOG_H

#include <stdio.h>

#define TRACE_LEVEL 3
#define DEBUG_LEVEL 5
#define INFO_LEVEL 10
#define WARN_LEVEL 15
#define ERROR_LEVEL 20
#define FATAL_LEVEL 25
#define OFF_LEVEL 100

#define LOG_LEVEL DEBUG_LEVEL

#define _log_level(prefix, level, fmt, ...)                                    \
    do {                                                                       \
        if (level >= LOG_LEVEL) {                                              \
            fprintf(stderr,                                                    \
                    prefix "|%s:%d|%s|" fmt "\n",                              \
                    __FILE__,                                                  \
                    __LINE__,                                                  \
                    __func__,                                                  \
                    ##__VA_ARGS__);                                            \
        }                                                                      \
    } while (0)

#define log_tracef(fmt, ...) _log_level("TRACE", TRACE_LEVEL, fmt, __VA_ARGS__)
#define log_debugf(fmt, ...) _log_level("DEBUG", DEBUG_LEVEL, fmt, __VA_ARGS__)
#define log_infof(fmt, ...) _log_level("INFO ", INFO_LEVEL, fmt, __VA_ARGS__)
#define log_warnf(fmt, ...) _log_level("WARN ", WARN_LEVEL, fmt, __VA_ARGS__)
#define log_errorf(fmt, ...) _log_level("ERROR", ERROR_LEVEL, fmt, __VA_ARGS__)
#define log_fatalf(fmt, ...) _log_level("FATAL", FATAL_LEVEL, fmt, __VA_ARGS__)

#define log_trace(msg) log_tracef("%s", msg)
#define log_debug(msg) log_debugf("%s", msg)
#define log_info(msg) log_infof("%s", msg)
#define log_warn(msg) log_warnf("%s", msg)
#define log_error(msg) log_errorf("%s", msg)
#define log_fatal(msg) log_fatalf("%s", msg)

#endif