//# PRINT AFTER chip_interface.sv
`default_nettype none

`include "config.vh"
`include "compiler.vh"
`include "memory.vh"
`include "interrupt.vh"
`include "dram.vh"
`include "keyboard.vh"
`include "svo_defines.vh"
`include "basic_io.vh"
`include "remote.vh"
`include "video.vh"

module Chip #(
    `SVO_DEFAULT_PARAMS
    )(
    KeyboardInterface.Controller keyboard,
    VideoInterfaceInternal.Controller video,
    DRAMAXIInterface.Chip dram,

    MemoryMappedIOInterface.Device axiRemoteRead, axiRemoteWrite,
    BasicIO.Remote basicIO,

    input logic clock, clear
);

    // Verify the magic IO regions are well defined
    `STATIC_ASSERT(`VRAM_START < `VRAM_END, vram_range);
    `STATIC_ASSERT(`VRAM_END <= `REMOTE_START, vram_remote);
    `STATIC_ASSERT(`REMOTE_START < `REMOTE_END, remote_range);

    logic dump;

    MemoryInterface instructionMemory();
    MemoryInterface dataMemory();
    DRAMInterface dramMemory();
    MemoryMappedIOInterface memoryVRAM();
    MemoryMappedIOInterface memoryRemote();
    MemoryControl memoryControl();

    Memory memory(
        .instruction(instructionMemory.Memory),
        .data(dataMemory.Memory),
        .dram(dramMemory.Memory),
        .vram(memoryVRAM.Requester),
        .remote(memoryRemote.Requester),
        .control(memoryControl.Memory),
        .clock,
        .clear
    );

    RemoteDRAMInterface dramRemote();

    DRAMWrapper dramWrapper(
        .memory(dramMemory.DRAM),
        .axi(dram),
        .remote(dramRemote),
        .clock,
        .clear
    );

    InterruptDevice keyboardInterrupt();

    KeyboardController keyboardController(
        .keyboard(keyboard),
        .interruptController(keyboardInterrupt.Device)
    );

    MemoryMappedIOInterface displayVRAM();

    DisplayController #(`SVO_PASS_PARAMS) displayController(
        .videoMemory(displayVRAM.Requester),
        .videoInternal(video)
    );

    VRAM vram(
        `ifdef SIMULATION_18447
        .dump(dump),
        `endif
        .memory(memoryVRAM.Device),
        .display(displayVRAM.Device)
    );

    InterruptDevice timerInterrupt();
    RemoteTimerInterface timerRemote();

    TimerController timer(
        .interruptController(timerInterrupt.Device),
        .remote(timerRemote.Timer),
        .clock,
        .clear
    );

    RemoteCoreInterface coreRemote();

    Core core(
        .instruction(instructionMemory.Requester),
        .data(dataMemory.Requester),
        .memoryControl(memoryControl.Core),
        .remote(coreRemote.Core),
        .keyboard(keyboardInterrupt.InterruptController),
        .timer(timerInterrupt.InterruptController),
        .clock,
        .clear
    );

    Remote remote(
        .core(coreRemote.Remote),
        .dram(dramRemote.Remote),
        .timer(timerRemote.Remote),
        .basicIO(basicIO),
        .axiRead(axiRemoteRead),
        .axiWrite(axiRemoteWrite),
        .memory(memoryRemote.Device),
        .dump,
        .clock,
        .clear
    );

endmodule