//# PRINT AFTER keyboard/keyboard_mock.sv
`default_nettype none

`ifdef SIMULATION_18447

`include "memory.vh"
`include "remote.vh"
`include "riscv_csr.vh"
`include "config.vh"
`include "riscv_types.vh"

module RemoteMock(
    MemoryMappedIOInterface.Requester remoteRead,
    MemoryMappedIOInterface.Requester remoteWrite,
    input logic clock, clear
);

    `MEMORY_FUNCTIONS

    localparam STDOUT = 32'h8000_0002;
    localparam PhysicalAddress_t HALT_ADDRESS = {
        18'b0,
        `REMOTE_CONTROL_PREFIX,
        `REMOTE_HALT,
        2'b0
    };
    localparam PhysicalAddress_t DEBUG_LOG_BASE = {
        18'b0,
        `REMOTE_IO_PREFIX,
        12'b0
    };
    localparam PhysicalAddress_t CYCLE_ADDRESS = {18'b0, 2'b11, CYCLE, 2'b0};
    localparam PhysicalAddress_t CYCLEH_ADDRESS = {18'b0, 2'b11, CYCLEH, 2'b0};
    localparam PhysicalAddress_t DEBUG_FAST_ENABLED = {
        18'b0,
        `REMOTE_IO_PREFIX,
        12'h200
    };
    localparam Word_t DEBUG_FAST_MAGIC = 32'hDEB6FA57;

    class RemoteThread;
        int debug_fd;
        logic [`REMOTE_DEBUG_STRING_LIMIT-1:0][7:0] debugLogData;

        function void clearDebugLog();
            this.debugLogData <= {$bits(debugLogData){1'b0}};
        endfunction

        function void init();
            this.debug_fd = $fopen("simulation_debug.log");
            this.clearDebugLog();
            remoteWrite.request <= '{default:0};
            remoteRead.request <= '{default:0};
        endfunction

        task WriteRequest(input PhysicalAddress_t address, input Word_t value);
            remoteWrite.request <= simpleMemoryWrite(address, 4'hf, value);
            @(posedge clock);
            remoteWrite.request <= '{default:0};
            @(posedge clock);
        endtask

        task ReadRequest(input PhysicalAddress_t address, output Word_t result);
            remoteRead.request <= simpleMemoryRead(address);
            @(posedge clock);
            remoteRead.request <= '{default:0};
            @(posedge clock);
            result = remoteRead.response.readData;
        endtask

        function void printLogLine(int fd);
            $fwrite(fd, "debug: ");
            for(int i = 0;
                i < `REMOTE_DEBUG_STRING_LIMIT
                && debugLogData[i] != 8'b0
                && debugLogData[i] != 8'ha; // '\n'
                i++
            ) begin
                $fwrite(fd, "%c", debugLogData[i]);
            end
            $fwrite(fd, "\n");
        endfunction

        task CollectAndPrintDebugLogLine();
            automatic Word_t debugResult;
            automatic Word_t arg1, arg2;

            this.ReadRequest(DEBUG_FAST_ENABLED, debugResult);
            if(!$isunknown(debugResult) &&
                debugResult == DEBUG_FAST_MAGIC
            ) begin
                this.ReadRequest(DEBUG_FAST_ENABLED+34'd4, arg1);
                this.ReadRequest(DEBUG_FAST_ENABLED+32'd8, arg2);
                $fwrite(STDOUT, "%d debugFAST: %x %x\n", $time, arg1, arg2);
                $fwrite(debug_fd, "%d debugFAST: %x %x\n", $time, arg1, arg2);
            end else begin
                this.clearDebugLog();
                for(int i = 0; i < `REMOTE_DEBUG_STRING_LIMIT/4; i++) begin
                    this.ReadRequest(DEBUG_LOG_BASE + i * 4, debugResult);
                    debugLogData[i*4] <= debugResult[7:0];
                    debugLogData[i*4+1] <= debugResult[15:8];
                    debugLogData[i*4+2] <= debugResult[23:16];
                    debugLogData[i*4+3] <= debugResult[31:24];
                end
                for(int i = 0; i < `REMOTE_DEBUG_STRING_LIMIT/4; i++) begin
                    this.WriteRequest(DEBUG_LOG_BASE + i * 4, 32'hDDDDDDDD);
                end
                this.ReadRequest(DEBUG_LOG_BASE, debugResult);
                assert(debugResult == 32'hDDDDDDDD);
                $fwrite(STDOUT, $time, " ");
                printLogLine(STDOUT);
                printLogLine(debug_fd);
            end


        endtask


    endclass

    Word_t readResult;
    initial begin
        RemoteThread rt = new;
        rt.init();

        #1; // Need to wait to make sure the clear block executes before us

        // Wait until we aren't clearing
        while(clear) begin
            @(posedge clock);
        end
        // Delay a bit more to simulate the real environment
        repeat(3) @(posedge clock);
        // Start the core
        rt.WriteRequest(HALT_ADDRESS, 32'b0);

        // Do the main loop
        forever begin
            rt.ReadRequest(HALT_ADDRESS, readResult);
            if(readResult[1]) begin
                // There is a pending debug halt
                rt.CollectAndPrintDebugLogLine();
                // Then clear the halt and resume the core
                rt.WriteRequest(HALT_ADDRESS, 32'b0);
            end else if(readResult[0]) begin
                // There is a pending primary halt, stop the simulation
                $display("remote_mock detected a primary halt without a debug \
                    message; Terminating simulation: %x", readResult);
                // Make sure everything settles down
                repeat(5) @(posedge clock);
                #100 $finish;
            end
            @(posedge clock);
            rt.ReadRequest(CYCLEH, readResult);
            if(readResult != 32'b0) begin
                $display("cycle_high overflow detected;\
                 Terminating simulation: %x", readResult);
                #100 $finish;
            end
            `ifdef SIMULATION_CYCLE_LIMIT
            rt.ReadRequest(CYCLE, readResult);
            if(readResult > `SIMULATION_CYCLE_LIMIT) begin
                $display("cycle count exceeded limit %x > %x;\
                 Terminating simulation", readResult, `SIMULATION_CYCLE_LIMIT);
                #100 $finish;
            end
            `endif
            @(posedge clock);
        end
    end

    // Ignored
    assign remoteRead.clock = clock;
    assign remoteRead.clear = clear;
    assign remoteWrite.clock = clock;
    assign remoteWrite.clear = clear;


endmodule

`endif