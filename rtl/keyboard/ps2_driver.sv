//# PRINT AFTER include/keyboard/keyboard.vh
`ifndef SIMULATION_18447

`default_nettype none

`include "keyboard.vh"
`include "board_config.vh"

`ifdef PS2_IO

module PS2Driver(
    inout wire ps2_clk, ps2_data,
    KeyboardInterface.Device keyboard
);

    assign keyboard.readch[7] = 1'b0; // Nothing for this bit

    ps2 keyb_mouse(
        .clk(keyboard.clock),
        .rst(keyboard.clear),
        .PS2_K_CLK_IO(ps2_clk),
        .PS2_K_DATA_IO(ps2_data),
        .PS2_M_CLK_IO(),
        .PS2_M_DATA_IO(),
        .ascii_code(keyboard.readch[6:0]),
        .ascii_data_ready_out(keyboard.ascii_ready),
        .rx_translated_scan_code_out(keyboard.scancode),
        .rx_ascii_read(keyboard.ascii_ready)
    );


endmodule

`endif
`endif