//# PRINT AFTER core/interrupt_controller.sv
`default_nettype none

`include "keyboard.vh"
`include "interrupt.vh"
`include "config.vh"

module KeyboardController(
    KeyboardInterface.Controller keyboard,
    InterruptDevice.Device interruptController
);

    logic [7:0] keyboardData, interruptData;
    logic isFull, isEmpty;
    FIFO #(.SIZE(`KEYBOARD_QUEUE_SIZE), .WIDTH(8)) keyboardQueue(
        .producerData(keyboardData),
        .enqueueValid(keyboard.ascii_ready & ~isFull),
        .consumerData(interruptData),
        .dequeueValid(interruptController.interruptAccepted),
        .isEmpty,
        .isFull,
        .clock(keyboard.clock),
        .clear(keyboard.clear)
    );

    // TODO: This should probably be scancode
    assign keyboardData = keyboard.readch;

    // If there is something in the queue, we have an interrupt ready to go
    logic dataPending;
    // We need to register the !isEmpty flag since the data isn't valid until
    // the next clock cycle (sequential read)
    Register #(.WIDTH($bits(dataPending))) pendingRegister(
        .d(!isEmpty),
        .q(dataPending),
        .clock(keyboard.clock),
        .clear(keyboard.clear),
        .enable(1'b1)
    );
    assign interruptController.interruptPending = dataPending;
    assign interruptController.tval = {24'b0, interruptData};

endmodule