//# PRINT AFTER include/riscv/riscv_csr.vh
`default_nettype none

module Register #(parameter WIDTH=32, CLEAR_VALUE = 0) (
  output logic [WIDTH-1:0] q    ,
  input  logic [WIDTH-1:0] d    ,
  input  logic             clock, enable, clear
);

  always_ff @(posedge clock)
    if(clear)
      q <= CLEAR_VALUE;
    else if (enable)
      q <= d;

endmodule // register