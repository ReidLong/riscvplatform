//# PRINT AFTER library/eviction_policy.sv
`default_nettype none

module EdgeTrigger(
    input logic signal,
    output logic isEdge,
    input logic clock, clear);

    logic current, next;

    assign isEdge = !next && current;

    always_ff @(posedge clock) begin
        if(clear) begin
            current <= 1'b0;
            next <= 1'b0;
        end else begin
            current <= signal;
            next <= current;
        end
    end

endmodule