//# PRINT AFTER library/sync.sv
`default_nettype none

`include "compiler.vh"

module NotMostRecentlyUsedPolicy #(parameter WIDTH=32, RECENT=2)(
    // index is latched when select is asserted with a sequential delay
    // e.g. select is asserted and on the next cycle index is valid
    input logic select,
    output logic [WIDTH-1:0] index,

    // Bit mask indicating which items in the recent array should be latched
    input logic [RECENT-1:0] recentValid,
    input logic [RECENT-1:0][WIDTH-1:0] recent,

    input logic clock, clear
);

    logic [WIDTH-1:0] index_next;

    Register #(.WIDTH(WIDTH)) lastRegister(
        .q(index),
        .d(index_next),
        .enable(select),
        .clock,
        .clear
    );

    logic [RECENT-1:0][WIDTH-1:0] recent_saved;

    generate
        genvar recentIndex;
        for(recentIndex = 0; recentIndex < RECENT; recentIndex++) begin : recent_latch
            Register #(.WIDTH(WIDTH)) recentRegister(
                .q(recent_saved[recentIndex]),
                .d(recent[recentIndex]),
                .enable(recentValid[recentIndex]),
                .clock,
                .clear
            );
        end
    endgenerate

    // This is a completely absurd constraint, but works for now and ensures
    // that synthesis doesn't go crazy.
    `STATIC_ASSERT(RECENT == 2, recent_limit);

    logic [WIDTH-1:0] index_plus1, index_plus2;
    assign index_plus1 = index + 2'd1;
    assign index_plus2 = index + 2'd2;
    always_comb begin
        if(index_plus1 != recent_saved[0] && index_plus1 != recent_saved[1]) begin
            index_next = index_plus1;
        end else if(index_plus2 != recent_saved[0] && index_plus2 != recent_saved[1]) begin
            index_next = index_plus2;
        end else begin
            index_next = index + 2'd3;
        end
    end

endmodule