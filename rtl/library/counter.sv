//# PRINT AFTER library/edge_trigger.sv
`default_nettype none

module Counter #(parameter WIDTH = 8, INCREMENT=1) (
   input  logic         clock  ,
   input  logic         clear  ,
   input  logic         enable ,
   output logic [WIDTH-1:0] q
);

   always_ff @(posedge clock)
      if(clear)
         q <= 0;
      else if (enable)
         q <= q + INCREMENT;

endmodule: Counter