//# PRINT AFTER library/xilinx_bram_double_clock.sv
`default_nettype none

module xilinx_sram#(
    parameter NUM_WORDS = 128,
    parameter WORD_WIDTH = 62)(
    input logic clock, clear,
    input logic writeEnable,
    input logic [$clog2(NUM_WORDS)-1:0] readAddress, writeAddress,
    input logic [WORD_WIDTH-1:0] writeData,
    output logic [WORD_WIDTH-1:0] readData
);

    logic [NUM_WORDS-1:0][WORD_WIDTH-1:0] memory;

    always_ff @(posedge clock) begin
        if(clear)
            memory <= '{default: 0};
        else if(writeEnable)
            memory[writeAddress] <= writeData;
    end

    assign readData = memory[readAddress];

endmodule