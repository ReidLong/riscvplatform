//# PRINT AFTER library/register.sv
`default_nettype none

module Sync #(parameter WIDTH = 1) (
  input  logic             clock ,
  input  logic [WIDTH-1:0] sig  ,
  output logic [WIDTH-1:0] sig_s
);

  logic [3:0][WIDTH-1:0] sig_i;

  always_ff @(posedge clock) begin
    sig_i <= {sig_i[2:0], sig};
  end

  assign sig_s = sig_i[3];

endmodule