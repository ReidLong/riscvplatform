//# PRINT AFTER include/memory/dram.vh
`default_nettype none

`include "dram.vh"
`include "remote.vh"
`include "compiler.vh"
`include "riscv_types.vh"

typedef logic [$clog2(`DRAM_TRANSACTION_SIZE):0] Index_t;

module DRAMWrapper(
    DRAMInterface.DRAM memory,
    DRAMAXIInterface.Chip axi,
    RemoteDRAMInterface.DRAM remote,
    input logic clock, clear
);

    noReadError: assert property ( @(posedge clock) disable iff(clear)
        !remote.readError
    );

    noWriteError: assert property ( @(posedge clock) disable iff(clear)
        !remote.writeError
    );

    noWriteEnableMissing: assert property ( @(posedge clock) disable iff(clear)
        !remote.writeEnableMissing
    );

    logic loadReadArray;
    logic loadWriteArray;
    Word_t [`DRAM_TRANSACTION_SIZE-1:0] writeArray;
    Word_t [`DRAM_TRANSACTION_SIZE-1:0] readArray;

    // Big enough that it shouldn't overflow unless reset
    Index_t index;
    logic counterIncrement;
    logic counterClear;

    Counter #(.WIDTH($bits(index))) indexCounter(
        .clock,
        .clear(clear | counterClear),
        .enable(counterIncrement),
        .q(index)
    );

    logic loadAddress;
    Register #(.WIDTH($bits(memory.physicalAddress))) addressRegister(
        .q(axi.addressBase),
        .d(memory.physicalAddress),
        .clock,
        .enable(loadAddress),
        .clear
    );

    assign remote.index = index;
    assign remote.writeArray = writeArray;
    assign remote.addressBase = axi.addressBase;

    dramZeroBase: assert property (@(posedge clock) disable iff(clear) axi.addressBase[5:0] == 6'h0);

    assign axi.writeData = writeArray;

    Register #(.WIDTH($bits(readArray))) readArrayRegister(
        .q(readArray),
        .d(axi.readData),
        .enable(loadReadArray),
        .clock,
        .clear
    );

    `STATIC_ASSERT($bits(Index_t) >= 4, index_width);
    `STATIC_ASSERT($clog2(`DRAM_TRANSACTION_SIZE) == 4, transation_size);
    Array #(.WIDTH($bits(Word_t)), .ELEMENTS(`DRAM_TRANSACTION_SIZE)) writeArrayArray(
        .index(index[3:0]),
        .element(memory.writeData),
        .array(writeArray),
        .clock,
        .clear,
        .enable(loadWriteArray)
    );

    enum {
        IDLE,
        READ_REQUEST,
        READ_RESPONSE,
        READ_UNLOAD,
        WRITE_LOAD,
        WRITE_REQUEST,
        WRITE_WAIT,
        WRITE_RESPONSE,
        FATAL_ERROR
    } state, nextState;

    always_ff @(posedge clock)
        if(clear)
            state <= IDLE;
        else
            state <= nextState;


    firstAddressAligned: assert property (@(posedge clock) disable iff(clear)
        state == IDLE && (memory.readEnable || memory.writeEnable) |->
        memory.physicalAddress[5:0] == 6'b0
    );

    nextAddressIncremented: assert property (@(posedge clock) disable iff(clear)
        memory.writeEnable && memory.responseValid && memory.physicalAddress[5:2] != 4'hf |=>
        memory.physicalAddress[5:2] == $past(memory.physicalAddress[5:2] + 4'd1, 1)
    );

    always_comb begin
        nextState = state;

        counterIncrement = 1'b0;
        counterClear = 1'b0;

        loadAddress = 1'b0;
        loadReadArray = 1'b0;
        loadWriteArray = 1'b0;

        axi.readEnable = 1'b0;
        axi.writeEnable = 1'b0;

        remote.writeError = 1'b0;
        remote.readError = 1'b0;
        remote.writeEnableMissing = 1'b0;

        memory.responseValid = 1'b0;
        memory.transactionComplete = 1'b0;
        memory.readData = `WORD_POISON;

        unique case(state)
            IDLE: begin
                counterClear = 1'b1;
                if(memory.readEnable) begin
                    loadAddress = 1'b1;
                    nextState = READ_REQUEST;
                end else if(memory.writeEnable) begin
                    loadAddress = 1'b1;
                    nextState = WRITE_LOAD;
                    loadWriteArray = 1'b1; // Load the first data element
                    counterIncrement = 1'b1;
                    counterClear = 1'b0; // Stop clearing
                end
            end
            READ_REQUEST: begin
                axi.readEnable = 1'b1;
                if(axi.requestAccepted)
                    nextState = READ_RESPONSE;
            end
            READ_RESPONSE: begin
                if(axi.requestCompleted) begin
                    loadReadArray = 1'b1;
                    counterClear = 1'b1;
                    if(axi.requestError) begin
                        nextState = FATAL_ERROR;
                        remote.readError = 1'b1;
                    end else begin
                        nextState = READ_UNLOAD;

                    end
                end
            end
            READ_UNLOAD: begin
                memory.responseValid = 1'b1;
                memory.readData = readArray[index];
                counterIncrement = 1'b1;
                if(index == `DRAM_TRANSACTION_SIZE - 1) begin
                    nextState = IDLE;
                    memory.transactionComplete = 1'b1; // Ignored in the current implementation on reads
                    counterClear = 1'b1;
                end
            end
            WRITE_LOAD: begin
                // Assuming request.writeEnable
                memory.responseValid = 1'b1;
                counterIncrement = 1'b1;
                loadWriteArray = 1'b1;

                if(index == `DRAM_TRANSACTION_SIZE - 1)
                    nextState = WRITE_REQUEST;

                if(!memory.writeEnable) begin
                    nextState = FATAL_ERROR;
                    remote.writeEnableMissing = 1'b1;
                end
            end
            WRITE_REQUEST: begin
                memory.responseValid = 1'b1;
                nextState = WRITE_WAIT;
            end
            WRITE_WAIT: begin
                axi.writeEnable = 1'b1;
                if(axi.requestAccepted)
                    nextState = WRITE_RESPONSE;
            end
            WRITE_RESPONSE: begin
                if(axi.requestCompleted) begin
                    if(axi.requestError) begin
                        nextState = FATAL_ERROR;
                        remote.writeError = 1'b1;
                    end else begin
                        counterClear = 1'b1;
                        memory.transactionComplete = 1'b1;
                        nextState = IDLE;
                    end
                end
            end
            FATAL_ERROR: begin
                nextState = FATAL_ERROR; // spin forever
            end
        endcase
    end

endmodule