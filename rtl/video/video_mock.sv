//# PRINT AFTER memory/dram_mock.sv
`ifdef SIMULATION_18447

`default_nettype none

`include "video.vh"

module VideoMock(
    VideoInterfaceInternal.Driver video,
    input logic clock, clear
);

    assign video.ready = 1'b1;
    assign video.clock = clock;
    assign video.clear = clear;

    // TODO: Dump the pixels to a frame buffer to display over x-forwarding

endmodule

`endif