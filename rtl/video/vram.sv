//# PRINT AFTER memory/dram_fill.sv
`default_nettype none

`include "memory.vh"
`include "compiler.vh"

module VRAM(
    `ifdef SIMULATION_18447
    input logic dump,
    `endif
    MemoryMappedIOInterface.Device memory, display
);

    `MEMORY_FUNCTIONS

    displayReadOnly: assert property(
        @(posedge display.clock) disable iff(display.clear)
        !(|display.request.writeEnable)
    );

    memoryWriteInRange: assert property (
        @(posedge memory.clock) disable iff(memory.clear)
        |memory.request.writeEnable |-> isInVRAM(memory.request.physicalAddress)
    );

    `STATIC_ASSERT(`VRAM_SIZE > 0, vram_size_gt_zero);
    `STATIC_ASSERT(`VRAM_START % `PAGE_SIZE == 0, vram_start_page_size);
    `STATIC_ASSERT(`VRAM_END % `PAGE_SIZE == 0, vram_end_page_size);

    localparam NB_COL = 4;
    localparam COL_WIDTH = 8;

    `STATIC_ASSERT($bits(Word_t) == NB_COL * COL_WIDTH, word_width);

    xilinx_bram_double_clock #(
        .NB_COL(NB_COL),
        .COL_WIDTH(COL_WIDTH),
        .RAM_DEPTH(1024)
    ) vram (
        .addra(display.request.physicalAddress[11:2]          ),
        .dina (`WORD_POISON           ),
        .clka (display.clock               ),
        .wea  (4'b0                   ),
        .douta(display.response.readData),

        .addrb(memory.request.physicalAddress[11:2]         ),
        .dinb (memory.request.writeData),
        .clkb (memory.clock),
        .web  (memory.request.writeEnable),
        .doutb(memory.response.readData               )
    );

    SimpleMemoryRequest_t memoryRequest_saved, displayRequest_saved;

    Register #(.WIDTH($bits(memoryRequest_saved))) memoryRequestRegister (
        .q  (memoryRequest_saved),
        .d   (memory.request    ),
        .enable (1'b1            ),
        .clock(memory.clock       ),
        .clear(memory.clear)
    );
    Register #(.WIDTH($bits(displayRequest_saved))) displayRequestRegister (
        .q  (displayRequest_saved),
        .d   (display.request     ),
        .enable (1'b1           ),
        .clock(display.clock       ),
        .clear(display.clear)
    );

    // Visible on the next clock edge
    always_comb begin
        memory.response.request = memoryRequest_saved;
        display.response.request = displayRequest_saved;
        memory.response.responseValid = isInVRAM(
            memoryRequest_saved.physicalAddress
        );
        display.response.responseValid = isInVRAM(
            displayRequest_saved.physicalAddress
        );
    end

    `ifdef SIMULATION_18447
    // The file handle number for stdout
    localparam STDOUT = 32'h8000_0002;
    int fd;
    always_ff @(posedge memory.clock) begin
        if(!memory.clear && dump) begin
            $display("VRAM Dump at Cycle %0d", $time);
            $display("---------------------------------------------\n");
            display_characters();
            fd = $fopen("simulation.vram");
            print_vram_state(fd);
            $fclose(fd);
        end
    end

    function automatic string append_character(
        string current,
        logic[7:0] character
    );
        if(character != 8'b0) return {current, string'(character)};
        else return {current, " "};
    endfunction

    function automatic void display_characters();
        string line;
        line = "";
        for (PhysicalAddress_t address = `VRAM_START;
            address < `VRAM_END;
            address += 4) begin

            line = append_character(
                append_character(line, vram.BRAM[address[11:2]][7:0]),
                vram.BRAM[address[11:2]][23:16]
            );
            if(line.len() >= `CONSOLE_WIDTH) begin
                $display(line);
                line = "";
            end
        end
    endfunction

    function automatic void print_vram_state(int fd);
        $fdisplay(fd, "Segment: VRAM");
        $fdisplay(fd, "%-10s  %-4s %-4s %-4s %-4s",
            "Address", "+0", "+1", "+2", "+3");
        $fdisplay(fd, "-------------------------------");
        for (PhysicalAddress_t address = `VRAM_START;
            address < `VRAM_END;
            address += 4) begin
            $fdisplay(fd, "0x%08x: 0x%02x 0x%02x 0x%02x 0x%02x",
                address,
                vram.BRAM[address[11:2]][7:0],
                vram.BRAM[address[11:2]][15:8],
                vram.BRAM[address[11:2]][23:16],
                vram.BRAM[address[11:2]][31:24]
            );
        end
    endfunction

    `endif

endmodule