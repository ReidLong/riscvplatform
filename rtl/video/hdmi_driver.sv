//# PRINT AFTER video/svo_enc.v
`ifndef SIMULATION_18447

`include "board_config.vh"

`ifdef HDMI_OUTPUT
`default_nettype none

`include "video.vh"
`include "svo_defines.vh"

module HDMIDriver #(`SVO_DEFAULT_PARAMS)(
    VideoInterfaceInternal.Driver internalDriver,

    output logic       tmds_clk_n,
    output logic       tmds_clk_p,
    output logic [2:0] tmds_d_n,
    output logic [2:0] tmds_d_p,

    input logic clk_pixel, clk_5x_pixel, clk_pixel_resetn
);

    `SVO_DECLS

    assign internalDriver.clock = clk_pixel;
    assign internalDriver.clear = !clk_pixel_resetn;

    logic video_enc_tvalid;
    logic video_enc_tready;
    logic [SVO_BITS_PER_PIXEL-1:0] video_enc_tdata;
    logic [3:0] video_enc_tuser;

    svo_enc #( `SVO_PASS_PARAMS ) svo_enc (
        .clk(clk_pixel),
        .resetn(clk_pixel_resetn),

        .in_axis_tvalid(internalDriver.valid),
        .in_axis_tready(internalDriver.ready),
        .in_axis_tdata(internalDriver.data),
        .in_axis_tuser(internalDriver.control[0]),

        .out_axis_tvalid(video_enc_tvalid),
        .out_axis_tready(video_enc_tready),
        .out_axis_tdata(video_enc_tdata),
        .out_axis_tuser(video_enc_tuser)
    );

    assign video_enc_tready = 1;

    logic [2:0] tmds_d;
    logic [2:0] tmds_serdes_shift1;
    logic [2:0] tmds_serdes_shift2;
    logic [2:0] tmds_d0, tmds_d1, tmds_d2, tmds_d3, tmds_d4;
    logic [2:0] tmds_d5, tmds_d6, tmds_d7, tmds_d8, tmds_d9;

    OBUFDS tmds_bufds [3:0] (
        .I({clk_pixel, tmds_d}),
        .O({tmds_clk_p, tmds_d_p}),
        .OB({tmds_clk_n, tmds_d_n})
    );

    OSERDESE2 #(
        .DATA_RATE_OQ("DDR"),
        .DATA_RATE_TQ("SDR"),
        .DATA_WIDTH(10),
        .INIT_OQ(1'b0),
        .INIT_TQ(1'b0),
        .SERDES_MODE("MASTER"),
        .SRVAL_OQ(1'b0),
        .SRVAL_TQ(1'b0),
        .TBYTE_CTL("FALSE"),
        .TBYTE_SRC("FALSE"),
        .TRISTATE_WIDTH(1)
    ) tmds_serdes_lo [2:0] (
        .OFB(),
        .OQ(tmds_d),
        .SHIFTOUT1(),
        .SHIFTOUT2(),
        .TBYTEOUT(),
        .TFB(),
        .TQ(),
        .CLK(clk_5x_pixel),
        .CLKDIV(clk_pixel),
        .D1(tmds_d0),
        .D2(tmds_d1),
        .D3(tmds_d2),
        .D4(tmds_d3),
        .D5(tmds_d4),
        .D6(tmds_d5),
        .D7(tmds_d6),
        .D8(tmds_d7),
        .OCE(1'b1),
        .RST(~clk_pixel_resetn),
        .SHIFTIN1(tmds_serdes_shift1),
        .SHIFTIN2(tmds_serdes_shift2),
        .T1(1'b0),
        .T2(1'b0),
        .T3(1'b0),
        .T4(1'b0),
        .TBYTEIN(1'b0),
        .TCE(1'b0)
    );

    OSERDESE2 #(
        .DATA_RATE_OQ("DDR"),
        .DATA_RATE_TQ("SDR"),
        .DATA_WIDTH(10),
        .INIT_OQ(1'b0),
        .INIT_TQ(1'b0),
        .SERDES_MODE("SLAVE"),
        .SRVAL_OQ(1'b0),
        .SRVAL_TQ(1'b0),
        .TBYTE_CTL("FALSE"),
        .TBYTE_SRC("FALSE"),
        .TRISTATE_WIDTH(1)
    ) tmds_serdes_hi [2:0] (
        .OFB(),
        .OQ(),
        .SHIFTOUT1(tmds_serdes_shift1),
        .SHIFTOUT2(tmds_serdes_shift2),
        .TBYTEOUT(),
        .TFB(),
        .TQ(),
        .CLK(clk_5x_pixel),
        .CLKDIV(clk_pixel),
        .D1(1'b0),
        .D2(1'b0),
        .D3(tmds_d8),
        .D4(tmds_d9),
        .D5(1'b0),
        .D6(1'b0),
        .D7(1'b0),
        .D8(1'b0),
        .OCE(1'b1),
        .RST(~clk_pixel_resetn),
        .SHIFTIN1(1'b0),
        .SHIFTIN2(1'b0),
        .T1(1'b0),
        .T2(1'b0),
        .T3(1'b0),
        .T4(1'b0),
        .TBYTEIN(1'b0),
        .TCE(1'b0)
    );

    svo_tmds svo_tmds_0 (
        .clk(clk_pixel),
        .resetn(clk_pixel_resetn),
        .de(!video_enc_tuser[3]),
        .ctrl(video_enc_tuser[2:1]),
        .din(video_enc_tdata[23:16]),
        .dout({tmds_d9[0], tmds_d8[0], tmds_d7[0], tmds_d6[0], tmds_d5[0],
               tmds_d4[0], tmds_d3[0], tmds_d2[0], tmds_d1[0], tmds_d0[0]})
    );

    svo_tmds svo_tmds_1 (
        .clk(clk_pixel),
        .resetn(clk_pixel_resetn),
        .de(!video_enc_tuser[3]),
        .ctrl(2'b0),
        .din(video_enc_tdata[15:8]),
        .dout({tmds_d9[1], tmds_d8[1], tmds_d7[1], tmds_d6[1], tmds_d5[1],
               tmds_d4[1], tmds_d3[1], tmds_d2[1], tmds_d1[1], tmds_d0[1]})
    );

    svo_tmds svo_tmds_2 (
        .clk(clk_pixel),
        .resetn(clk_pixel_resetn),
        .de(!video_enc_tuser[3]),
        .ctrl(2'b0),
        .din(video_enc_tdata[7:0]),
        .dout({tmds_d9[2], tmds_d8[2], tmds_d7[2], tmds_d6[2], tmds_d5[2],
               tmds_d4[2], tmds_d3[2], tmds_d2[2], tmds_d1[2], tmds_d0[2]})
    );

endmodule

`endif
`endif