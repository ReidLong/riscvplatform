//# PRINT AFTER top.v
`ifndef SIMULATION_18447

`default_nettype none

`include "svo_defines.vh"
`include "video.vh"
`include "dram.vh"
`include "memory.vh"
`include "basic_io.vh"
`include "keyboard.vh"
`include "board_config.vh"

module ChipInterface
    #(
    // HDMI Display
    parameter SVO_MODE             =   "1280x720",
    parameter SVO_FRAMERATE        =   60,

    // DRAM
    parameter integer C_M00_AXI_ADDR_WIDTH  = 32,
    parameter integer C_M00_AXI_DATA_WIDTH  = 32,
    parameter integer C_M00_AXI_TRANSACTIONS_NUM    = 16,
    parameter integer C_M00_PHYSICAL_ADDRESS_OFFSET = 'h8000000,

    // Parameters of Axi Slave Bus Interface S00_AXI (debug core)
    parameter integer C_S00_AXI_DATA_WIDTH  = 32,
    parameter integer C_S00_AXI_ADDR_WIDTH  = 16
    ) (

    input logic clock, resetn,

    // Basic IO
    `ifdef PYNQ_Z2
    input logic [1:0] switch,
    output logic [5:0] rgb_led,
    output logic [3:0] led,
    input logic [3:0] button,
    `elsif ZEDBOARD
    input logic [7:0] switch,
    output logic [7:0] led,
    input logic button_C,
    input logic button_D,
    input logic button_L,
    input logic button_R,
    input logic button_U,
    `endif

    // Ports of Axi Master Bus Interface M00_AXI for dram controller
    input logic  m00_axi_aclk,
    input logic  m00_axi_aresetn,
    output logic [C_M00_AXI_ADDR_WIDTH-1 : 0] m00_axi_awaddr,
    output logic [2 : 0] m00_axi_awprot,
    output logic  m00_axi_awvalid,
    input logic  m00_axi_awready,
    output logic [C_M00_AXI_DATA_WIDTH-1 : 0] m00_axi_wdata,
    output logic [C_M00_AXI_DATA_WIDTH/8-1 : 0] m00_axi_wstrb,
    output logic  m00_axi_wvalid,
    input logic  m00_axi_wready,
    input logic [1 : 0] m00_axi_bresp,
    input logic  m00_axi_bvalid,
    output logic  m00_axi_bready,
    output logic [C_M00_AXI_ADDR_WIDTH-1 : 0] m00_axi_araddr,
    output logic [2 : 0] m00_axi_arprot,
    output logic  m00_axi_arvalid,
    input logic  m00_axi_arready,
    input logic [C_M00_AXI_DATA_WIDTH-1 : 0] m00_axi_rdata,
    input logic [1 : 0] m00_axi_rresp,
    input logic  m00_axi_rvalid,
    output logic  m00_axi_rready,

    // Remote Controller
    input logic  s00_axi_aclk,
    input logic  s00_axi_aresetn,
    input logic [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
    input logic [2 : 0] s00_axi_awprot,
    input logic  s00_axi_awvalid,
    output logic  s00_axi_awready,
    input logic [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
    input logic [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
    input logic  s00_axi_wvalid,
    output logic  s00_axi_wready,
    output logic [1 : 0] s00_axi_bresp,
    output logic  s00_axi_bvalid,
    input logic  s00_axi_bready,
    input logic [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
    input logic [2 : 0] s00_axi_arprot,
    input logic  s00_axi_arvalid,
    output logic  s00_axi_arready,
    output logic [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
    output logic [1 : 0] s00_axi_rresp,
    output logic  s00_axi_rvalid,
    input logic  s00_axi_rready,

    // Video clocks
    input logic clk_pixel, clk_5x_pixel, locked,

`ifdef HDMI_OUTPUT
    // HDMI Out
    output logic       tmds_clk_n,
    output logic       tmds_clk_p,
    output logic [2:0] tmds_d_n,
    output logic [2:0] tmds_d_p,
`elsif VGA_OUTPUT
    output logic [3:0] vga_r, vga_g, vga_b,
    output logic vga_vs, vga_hs,
`endif

`ifdef PS2_IO
    // PS/2
    inout wire ps2_clk, ps2_data
`elsif PS2_ASCII
    input logic [7:0] readch, scancode,
    input logic ascii_ready
`endif
);

    // Synchronize inputs

    logic [3:0] locked_clk_q;
    logic [3:0] resetn_clk_pixel_q;

    always @(posedge clock)
        locked_clk_q <= {locked_clk_q, locked};

    always @(posedge clk_pixel)
        resetn_clk_pixel_q <= {resetn_clk_pixel_q[2:0], resetn};

    logic clk_resetn;
    assign clk_resetn = resetn && locked_clk_q[3];
    logic clk_pixel_resetn;
    assign clk_pixel_resetn = locked && resetn_clk_pixel_q[3];

    logic clearn;
    assign clearn = clk_resetn;

    logic clear;
    assign clear = ~clearn;

    localparam SVO_BITS_PER_PIXEL   = 24;

    KeyboardInterface keyboard(.clock, .clear);
    VideoInterfaceInternal video();
    DRAMAXIInterface dram();
    MemoryMappedIOInterface axiRemoteRead();
    MemoryMappedIOInterface axiRemoteWrite();
    BasicIO basicIO();

    Chip #(`SVO_PASS_PARAMS) chip(
        .keyboard(keyboard.Controller),
        .video(video.Controller),
        .dram(dram.Chip),
        .axiRemoteRead(axiRemoteRead.Device),
        .axiRemoteWrite(axiRemoteWrite.Device),
        .basicIO(basicIO.Remote),
        .clock,
        .clear
    );
`ifdef HDMI_OUTPUT
    HDMIDriver #(`SVO_PASS_PARAMS) hdmiDriver(
        .internalDriver(video.Driver),
        .tmds_clk_n,
        .tmds_clk_p,
        .tmds_d_n,
        .tmds_d_p,
        .clk_pixel,
        .clk_5x_pixel,
        .clk_pixel_resetn
    );
`elsif VGA_OUTPUT
    VGADriver #(`SVO_PASS_PARAMS) vgaDriver(
        .internalDriver(video.Driver),
        .vga_r,
        .vga_g,
        .vga_b,
        .vga_hs,
        .vga_vs,
        .clk_pixel,
        .clk_5x_pixel,
        .clk_pixel_resetn
    );
`endif

    RemoteAXIDriver #(
        .C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
        .C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
    ) remoteDriver(
        .remoteRead(axiRemoteRead.Requester),
        .remoteWrite(axiRemoteWrite.Requester),
        .S_AXI_ACLK(s00_axi_aclk),
        .S_AXI_ARESETN(s00_axi_aresetn),
        .S_AXI_AWADDR(s00_axi_awaddr),
        .S_AXI_AWPROT(s00_axi_awprot),
        .S_AXI_AWVALID(s00_axi_awvalid),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WDATA(s00_axi_wdata),
        .S_AXI_WSTRB(s00_axi_wstrb),
        .S_AXI_WVALID(s00_axi_wvalid),
        .S_AXI_WREADY(s00_axi_wready),
        .S_AXI_BRESP(s00_axi_bresp),
        .S_AXI_BVALID(s00_axi_bvalid),
        .S_AXI_BREADY(s00_axi_bready),
        .S_AXI_ARADDR(s00_axi_araddr),
        .S_AXI_ARPROT(s00_axi_arprot),
        .S_AXI_ARVALID(s00_axi_arvalid),
        .S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_RDATA(s00_axi_rdata),
        .S_AXI_RRESP(s00_axi_rresp),
        .S_AXI_RVALID(s00_axi_rvalid),
        .S_AXI_RREADY(s00_axi_rready)
    );

    DRAMAXIDriver #(
        .C_M_AXI_ADDR_WIDTH(C_M00_AXI_ADDR_WIDTH),
        .C_M_AXI_DATA_WIDTH(C_M00_AXI_DATA_WIDTH),
        .C_M_TRANSACTIONS_NUM(C_M00_AXI_TRANSACTIONS_NUM),
        .PHYSICAL_ADDRESS_OFFSET(C_M00_PHYSICAL_ADDRESS_OFFSET)
        ) dramDriver(
        .dram(dram.AXI),
        .M_AXI_ACLK(m00_axi_aclk),
        .M_AXI_ARESETN(m00_axi_aresetn),
        .M_AXI_AWADDR(m00_axi_awaddr),
        .M_AXI_AWPROT(m00_axi_awprot),
        .M_AXI_AWVALID(m00_axi_awvalid),
        .M_AXI_AWREADY(m00_axi_awready),
        .M_AXI_WDATA(m00_axi_wdata),
        .M_AXI_WSTRB(m00_axi_wstrb),
        .M_AXI_WVALID(m00_axi_wvalid),
        .M_AXI_WREADY(m00_axi_wready),
        .M_AXI_BRESP(m00_axi_bresp),
        .M_AXI_BVALID(m00_axi_bvalid),
        .M_AXI_BREADY(m00_axi_bready),
        .M_AXI_ARADDR(m00_axi_araddr),
        .M_AXI_ARPROT(m00_axi_arprot),
        .M_AXI_ARVALID(m00_axi_arvalid),
        .M_AXI_ARREADY(m00_axi_arready),
        .M_AXI_RDATA(m00_axi_rdata),
        .M_AXI_RRESP(m00_axi_rresp),
        .M_AXI_RVALID(m00_axi_rvalid),
        .M_AXI_RREADY(m00_axi_rready)
    );

`ifdef PS2_IO
    PS2Driver ps2Driver(
        .ps2_clk,
        .ps2_data,
        .keyboard(keyboard.Device)
    );
`elsif PS2_ASCII

    assign keyboard.scancode = scancode;
    assign keyboard.readch = readch;
    assign keyboard.ascii_ready = ascii_ready;

`endif

`ifdef PYNQ_Z2
    Sync #($bits(switch)) switch_sync(.clock, .sig(switch), .sig_s(basicIO.switch));
    Sync #($bits(button)) button_sync(.clock, .sig(button), .sig_s(basicIO.button));

    always_comb begin
        rgb_led = {basicIO.rgb_led[1], basicIO.rgb_led[0]};
        led = basicIO.led;
    end
`elsif ZEDBOARD
    Sync #($bits(switch)) switch_sync(.clock, .sig(switch), .sig_s(basicIO.switch));
    Sync #($bits(button_C)) button_C_sync(.clock, .sig(button_C), .sig_s(basicIO.button_C));
    Sync #($bits(button_D)) button_D_sync(.clock, .sig(button_D), .sig_s(basicIO.button_D));
    Sync #($bits(button_L)) button_L_sync(.clock, .sig(button_L), .sig_s(basicIO.button_L));
    Sync #($bits(button_R)) button_R_sync(.clock, .sig(button_R), .sig_s(basicIO.button_R));
    Sync #($bits(button_U)) button_U_sync(.clock, .sig(button_U), .sig_s(basicIO.button_U));

    always_comb begin
        led = basicIO.led;
    end
`elsif VFPGA
    assign basicIO._sv2v_unused = 1'b0;
`endif

endmodule

`endif
