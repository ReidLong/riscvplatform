//# PRINT FIRST
`ifndef __CONFIG_VH
`define __CONFIG_VH

`include "board_config.vh"
`include "riscv_types.vh"

//////////////////////////////////////////////////
// Memory Config
//////////////////////////////////////////////////

`define VRAM_START 32'h1000
`define VRAM_SIZE `PAGE_SIZE
`define VRAM_END (`VRAM_START + `VRAM_SIZE)

`define REMOTE_START 32'h2000
`define REMOTE_SIZE `PAGE_SIZE
`define REMOTE_END (`REMOTE_START + `REMOTE_SIZE)

`define REMOTE_DEBUG_STRING_LIMIT 256

`define RESERVED_LOW `VRAM_START
`define RESERVED_HIGH `REMOTE_END

`define TEXT_START 32'h100000

`define PHYSICAL_MEMORY_SIZE (64 * 1024 * 1024)

//////////////////////////////////////////////////
// Micro-architecture Config
//////////////////////////////////////////////////

`define KEYBOARD_MIN_TICK 32'd500000

`ifdef VFPGA
`define KEYBOARD_QUEUE_SIZE 8

// TODO: Investigate decreasing this until the incorrect predictions become too high
`define BTB_ENTRIES 8
`define BTB_RA_STACK_SIZE 4

`define TLB_ENTRIES 4

// SINGLE_SET must be defined if CACHE_SETS == 1
`define SINGLE_SET
`define CACHE_SETS 1

`else /* VFPGA */

`define KEYBOARD_QUEUE_SIZE 64

`define BTB_ENTRIES 512
`define BTB_RA_STACK_SIZE 64

`define TLB_ENTRIES 16

// SINGLE_SET must be defined if CACHE_SETS == 1
// `define SINGLE_SET
`define CACHE_SETS 8

`endif /* VFPGA */

`ifdef SIMULATION_18447
`define MUX_CHARMAP
`elsif VFPGA
`define MUX_CHARMAP
`else
`define BRAM_CHARMAP
`endif

//////////////////////////////////////////////////
// Timer Config
//////////////////////////////////////////////////

`ifdef SIMULATION_18447
`define TIMER_TICK_CYCLES 32'd100000
`else
`define TIMER_TICK_CYCLES 32'd1000000
`endif

// `define TIMER_TICK_CYCLES 32'd25000

//////////////////////////////////////////////////
// Simulation Config
//////////////////////////////////////////////////

// `define SIMULATION_CYCLE_LIMIT 64'd100

`define DISPLAY_PERFORMANCE_COUNTER_CSV

`ifdef SIMULATION_18447
`define TRACE
`endif

//////////////////////////////////////////////////
// Machine ABI Config
//////////////////////////////////////////////////

/* a0 */
`define ECALL_ARG_REGISTER 5'ha

`define ECALL_ARG_HALT 32'ha
`define ECALL_ARG_DEBUG 32'hd

`endif