//# PRINT AFTER core/core.sv
`ifndef __REGISTER_FILE_VH
`define __REGISTER_FILE_VH

`include "riscv_types.vh"
`include "riscv_isa.vh"

typedef struct packed {
    isa_reg_t number;
    logic isValid; // TODO: rename to valid
} Register_t;

typedef struct packed {
    Register_t register;
    Word_t result; // TODO: rename to value
    logic resultValid;
} RegisterResponse_t;

interface RegisterFileRequest();

    Register_t request;
    // The valid bit in the value is used to indicate that the request cannot be
    // satisfied and needs to stall
    RegisterResponse_t response;

    modport Requester(
        output request,
        input response
    );

    modport RegisterFile(
        input request,
        output response
    );

endinterface

interface RegisterReport();

    RegisterResponse_t response;

    modport Reporter(
        output response
    );

    modport RegisterFile(
        input response
    );

endinterface

`endif