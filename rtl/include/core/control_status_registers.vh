//# PRINT AFTER core/simple_register_file.sv
`ifndef __CONTROL_STATUS_REGISTER_VH
`define __CONTROL_STATUS_REGISTER_VH

`include "riscv_types.vh"
`include "riscv_csr.vh"
`include "decode.vh"

interface CSRReadInterface();
    ControlStatusRegisterName_t name;
    Word_t value;

    modport ControlStatusRegister(
        input name,
        output value
    );

    modport Requester(
        output name,
        input value
    );

endinterface

interface CSRWriteInterface();
    ControlStatusRegisterName_t name;
    Word_t value;
    logic writeEnable;

    modport ControlStatusRegister(
        input name, value, writeEnable
    );

    modport Requester(
        output name, value, writeEnable
    );

endinterface

interface CSRInterruptDelivery();

    // Interrupt Request
    logic deliverInterrupt;
    ExceptionCause_t cause;
    Word_t value, epc;

    // Interrupt Response
    Word_t trapVector;

    modport ControlStatusRegister(
        input deliverInterrupt,
        input cause, value, epc,
        output trapVector
    );

    modport ControlFlow(
        output deliverInterrupt,
        output cause, value, epc,
        input trapVector
    );

endinterface

interface CSRInterruptReturn();
    // [U|S|M]RET
    InstructionType_t instructionType;
    Word_t epc;

    modport ControlStatusRegister(
        input instructionType,
        output epc
    );

    modport ControlFlow(
        output  instructionType,
        input epc
    );

endinterface

interface CSRCoreObserver();

    Status_t status;
    PrivilegeLevel_t currentLevel;
    Word_t mideleg;

    modport ControlStatusRegister(
        output status, currentLevel,
        output mideleg
    );

    modport Observer(
        input status, currentLevel,
        input mideleg
    );
endinterface

`endif