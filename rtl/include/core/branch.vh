//# PRINT AFTER core/execute.sv
`ifndef __BRANCH_VH
`define __BRANCH_VH

`include "fetch.vh"
`include "riscv_types.vh"

typedef enum {
    INVALID_DIRECTION,
    FORWARD,
    BACKWARD
} Direction_t;

typedef enum {
    UNPREDICTABLE = 'd0, // this is used for things like fences and interrupts
    CORRECT_PREDICTION,
    INCORRECT_PREDICTION
} PredictionResult_t;

typedef struct packed {
    Direction_t simpleDirection; // Valid if decode.instructionType == BRANCH || JUMP
    PredictionResult_t predictionResult;
    Word_t actualNextPC;
} BranchResolveState_t;

interface BranchResolve();

    InstructionFetchState_t fetch_BR;

    BranchResolveState_t branch_BR;

    modport Branch(
        output fetch_BR,
        output branch_BR
    );

    modport ControlFlow(
        input fetch_BR,
        input branch_BR
    );


endinterface


`endif