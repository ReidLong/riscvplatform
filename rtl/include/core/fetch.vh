//# PRINT AFTER core/control_flow.sv
`ifndef __FETCH_VH
`define __FETCH_VH

`include "branch_predictor.vh"
`include "riscv_types.vh"

typedef struct packed {
    logic valid;
    Word_t pc;
    Prediction_t prediction;
} InstructionFetchState_t;

typedef struct packed {
    MemoryResponse_t instruction;
} InstructionWaitState_t;

interface FetchPrediction();

    Word_t pc;
    Prediction_t prediction;
    logic forcePC;

    modport Fetch(
        output pc,
        input prediction,
        input forcePC
    );

    modport ControlFlow(
        input pc,
        output prediction,
        output forcePC
    );

endinterface

`endif