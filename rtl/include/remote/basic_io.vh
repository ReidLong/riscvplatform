//# PRINT AFTER include/remote/remote.vh
`ifndef __BASIC_IO_VH
`define __BASIC_IO_VH

`include "board_config.vh"

`ifdef PYNQ_Z2
interface BasicIO();
    logic [1:0] switch;
    logic [1:0][2:0] rgb_led;
    logic [3:0] led;
    logic [3:0] button;

    modport Remote(
        input switch, button,
        output led, rgb_led
    );

endinterface
`elsif ZEDBOARD

interface BasicIO();
    logic [7:0] switch;
    logic [7:0] led;
    logic button_C;
    logic button_D;
    logic button_L;
    logic button_R;
    logic button_U;

    modport Remote(
        input switch,
        input button_C, button_D, button_L, button_R, button_U,
        output led
    );

endinterface

`elsif VFPGA
interface BasicIO ();

    logic _sv2v_unused;
    modport Remote(input _sv2v_unused);

endinterface
`endif

`endif