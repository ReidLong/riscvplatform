//# PRINT AFTER include/compiler.vh
`ifndef __RISCV_TYPES_VH
`define __RISCV_TYPES_VH

`define XLEN 32
`define BYTE_WIDTH 8
`define XLEN_BYTES (`XLEN / `BYTE_WIDTH)

`define PAGE_SIZE 'd4096

`define WORD_POISON 32'hBADF00D
// It is important that the these are not 4-byte aligned so they cause faults
// when used as addresses.
`define VIRTUAL_ADDRESS_POISON 32'HA0BADADD
`define PHYSICAL_ADDRESS_POISON 34'h300BADADD

typedef logic [`XLEN-1:0] VirtualAddress_t;
typedef logic [`XLEN-1:0] Word_t;

// RISC-V Physical Addresses are 34-bits wide
typedef logic [33:0] PhysicalAddress_t;


`endif