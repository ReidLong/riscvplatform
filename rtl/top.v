//# PRINT AFTER library/array.sv
`ifndef SIMULATION_18447

`include "board_config.vh"

module top
    #(
    parameter SVO_MODE             =   "1280x720",
    parameter SVO_FRAMERATE        =   60,
    parameter integer C_M00_AXI_ADDR_WIDTH  = 32,
    parameter integer C_M00_AXI_DATA_WIDTH  = 32,
    parameter integer C_M00_AXI_TRANSACTIONS_NUM    = 16,
    parameter integer C_M00_PHYSICAL_ADDRESS_OFFSET = 'h8000000,
    parameter integer C_S00_AXI_DATA_WIDTH  = 32,
    parameter integer C_S00_AXI_ADDR_WIDTH  = 16
    )(
    input wire clock, resetn,

    // Basic IO
    `ifdef PYNQ_Z2
    input wire [1:0] switch,
    output wire [5:0] rgb_led,
    output wire [3:0] led,
    input wire [3:0] button,
    `elsif ZEDBOARD
    input wire [7:0] switch,
    output wire [7:0] led,
    input wire button_C,
    input wire button_D,
    input wire button_L,
    input wire button_R,
    input wire button_U,
    `endif

    // Ports of Axi Master Bus Interface M00_AXI
    input wire  m00_axi_aclk,
    input wire  m00_axi_aresetn,
    output wire [C_M00_AXI_ADDR_WIDTH-1 : 0] m00_axi_awaddr,
    output wire [2 : 0] m00_axi_awprot,
    output wire  m00_axi_awvalid,
    input wire  m00_axi_awready,
    output wire [C_M00_AXI_DATA_WIDTH-1 : 0] m00_axi_wdata,
    output wire [C_M00_AXI_DATA_WIDTH/8-1 : 0] m00_axi_wstrb,
    output wire  m00_axi_wvalid,
    input wire  m00_axi_wready,
    input wire [1 : 0] m00_axi_bresp,
    input wire  m00_axi_bvalid,
    output wire  m00_axi_bready,
    output wire [C_M00_AXI_ADDR_WIDTH-1 : 0] m00_axi_araddr,
    output wire [2 : 0] m00_axi_arprot,
    output wire  m00_axi_arvalid,
    input wire  m00_axi_arready,
    input wire [C_M00_AXI_DATA_WIDTH-1 : 0] m00_axi_rdata,
    input wire [1 : 0] m00_axi_rresp,
    input wire  m00_axi_rvalid,
    output wire  m00_axi_rready,

    // Remote Controller
    input wire  s00_axi_aclk,
    input wire  s00_axi_aresetn,
    input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
    input wire [2 : 0] s00_axi_awprot,
    input wire  s00_axi_awvalid,
    output wire  s00_axi_awready,
    input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
    input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
    input wire  s00_axi_wvalid,
    output wire  s00_axi_wready,
    output wire [1 : 0] s00_axi_bresp,
    output wire  s00_axi_bvalid,
    input wire  s00_axi_bready,
    input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
    input wire [2 : 0] s00_axi_arprot,
    input wire  s00_axi_arvalid,
    output wire  s00_axi_arready,
    output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
    output wire [1 : 0] s00_axi_rresp,
    output wire  s00_axi_rvalid,
    input wire  s00_axi_rready,

    // video clocks
    input wire clk_pixel, clk_5x_pixel, locked,

    // output signals
`ifdef HDMI_OUTPUT
    output wire       tmds_clk_n,
    output wire       tmds_clk_p,
    output wire [2:0] tmds_d_n,
    output wire [2:0] tmds_d_p,
`elsif VGA_OUTPUT
    output wire [3:0] vga_r, vga_g, vga_b,
    output wire vga_vs, vga_hs,
`endif

`ifdef PS2_IO
    // PS/2
    inout wire ps2_clk, ps2_data
`elsif PS2_ASCII
    input wire [7:0] readch, scancode,
    input wire ascii_ready
`endif

);

    ChipInterface #(
        .SVO_MODE(SVO_MODE),
        .SVO_FRAMERATE(SVO_FRAMERATE),
        .C_M00_AXI_ADDR_WIDTH(C_M00_AXI_ADDR_WIDTH),
        .C_M00_AXI_DATA_WIDTH(C_M00_AXI_DATA_WIDTH),
        .C_M00_AXI_TRANSACTIONS_NUM(C_M00_AXI_TRANSACTIONS_NUM),
        .C_M00_PHYSICAL_ADDRESS_OFFSET(C_M00_PHYSICAL_ADDRESS_OFFSET),
        .C_S00_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
        .C_S00_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
    ) chip_interface(
        .clock(clock),
        .resetn(resetn),

`ifdef PYNQ_Z2
        .switch(switch),
        .rgb_led(rgb_led),
        .led(led),
        .button(button),
`elsif ZEDBOARD
        .switch(switch),
        .led(led),
        .button_C(button_C),
        .button_D(button_D),
        .button_L(button_L),
        .button_R(button_R),
        .button_U(button_U),
`endif

        .m00_axi_aclk(m00_axi_aclk),
        .m00_axi_aresetn(m00_axi_aresetn),
        .m00_axi_awaddr(m00_axi_awaddr),
        .m00_axi_awprot(m00_axi_awprot),
        .m00_axi_awvalid(m00_axi_awvalid),
        .m00_axi_awready(m00_axi_awready),
        .m00_axi_wdata(m00_axi_wdata),
        .m00_axi_wstrb(m00_axi_wstrb),
        .m00_axi_wvalid(m00_axi_wvalid),
        .m00_axi_wready(m00_axi_wready),
        .m00_axi_bresp(m00_axi_bresp),
        .m00_axi_bvalid(m00_axi_bvalid),
        .m00_axi_bready(m00_axi_bready),
        .m00_axi_araddr(m00_axi_araddr),
        .m00_axi_arprot(m00_axi_arprot),
        .m00_axi_arvalid(m00_axi_arvalid),
        .m00_axi_arready(m00_axi_arready),
        .m00_axi_rdata(m00_axi_rdata),
        .m00_axi_rresp(m00_axi_rresp),
        .m00_axi_rvalid(m00_axi_rvalid),
        .m00_axi_rready(m00_axi_rready),

        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awprot(s00_axi_awprot),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_awready(s00_axi_awready),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .s00_axi_wready(s00_axi_wready),
        .s00_axi_bresp(s00_axi_bresp),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_arprot(s00_axi_arprot),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_arready(s00_axi_arready),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rresp(s00_axi_rresp),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_rready(s00_axi_rready),

        .clk_pixel(clk_pixel),
        .clk_5x_pixel(clk_5x_pixel),
        .locked(locked),

`ifdef HDMI_OUTPUT
        .tmds_clk_n(tmds_clk_n),
        .tmds_clk_p(tmds_clk_p),
        .tmds_d_n(tmds_d_n),
        .tmds_d_p(tmds_d_p),
`elsif VGA_OUTPUT
        .vga_r(vga_r),
        .vga_g(vga_g),
        .vga_b(vga_b),
        .vga_hs(vga_hs),
        .vga_vs(vga_vs),
`endif

`ifdef PS2_IO
        .ps2_clk(ps2_clk),
        .ps2_data(ps2_data)
`elsif PS2_ASCII
        .readch(readch),
        .scancode(scancode),
        .ascii_ready(ascii_ready)
`endif
    );

endmodule

`endif