//# PRINT AFTER include/core/control_flow.vh
`default_nettype none

`include "control_status_registers.vh"
`include "control_flow.vh"
`include "branch_predictor.vh"
`include "fetch.vh"
`include "branch.vh"
`include "pipeline.vh"
`include "decode.vh"
`include "riscv_csr.vh"
`include "riscv_types.vh"

typedef struct packed {
    logic flush, stall;
} BackPressure_t;

module ControlFlow(
    CSRInterruptDelivery.ControlFlow csrInterruptDelivery,
    CSRInterruptReturn.ControlFlow csrInterruptReturn,
    CSRCoreObserver.Observer csrObserver,

    ControlFlowAsyncInterrupt.ControlFlow asyncInterrupt,

    PipelineControl.ControlFlow fetch,
    FetchPrediction.ControlFlow fetchPrediction,

    PipelineControl.ControlFlow instructionWait,

    PipelineControl.ControlFlow decode,

    PipelineControl.ControlFlow execute,

    PipelineControl.ControlFlow branch,
    BranchResolve.ControlFlow branchResolve,

    PipelineControl.ControlFlow memoryRequest,

    PipelineControl.ControlFlow memoryWait,

    input MemoryWaitExport_t writeBack,

    input logic remoteHalted,
    output logic coreHalt, coreDebug,

    input logic clock, clear
);

    `STATE_FUNCTIONS
    `DECODE_FUNCTIONS

    fetchNoFencing: assert property (@(posedge clock) disable iff(clear)
        !fetch.fencing
    );

    branchPredictionValid: assert property (@(posedge clock) disable iff(clear)
        branchResolve.branch_BR.predictionResult == CORRECT_PREDICTION ||
        branchResolve.branch_BR.predictionResult == INCORRECT_PREDICTION |->
        branchResolve.fetch_BR.valid
    );

    writeBackInstructionTypeValid: assert property (
        @(posedge clock) disable iff(clear)
        writeBack.fetch.valid |->
        writeBack.decode.instructionType != INVALID_INSTRUCTION
    );

    writeBackInvalid: assert property (@(posedge clock) disable iff(clear)
        !writeBack.fetch.valid |->
        writeBack.decode.instructionType == INVALID_INSTRUCTION
    );

    writeBackInvalidInstructionType: assert property (
        @(posedge clock) disable iff(clear)
        writeBack.decode.instructionType == INVALID_INSTRUCTION |->
        !writeBack.fetch.valid
    );

    writeBackFenceNextInstruction: assert property (
        @(posedge clock) disable iff(clear)
        writeBack.decode.instructionType == FENCE |->
        writeBack.branch.actualNextPC == (writeBack.fetch.pc + 32'd4)
    );

    writeBackDebugNextInstruction: assert property (
        @(posedge clock) disable iff(clear)
        coreDebug |->
        writeBack.branch.actualNextPC == (writeBack.fetch.pc + 32'd4)
    );


    ExceptionCause_t exceptionCause_WB;
    Word_t tval_WB;

    ExceptionSelection exceptionSelection(
        .writeBack,
        .exceptionCause(exceptionCause_WB),
        .tval(tval_WB),
        .clock,
        .clear
    );

    Prediction_t prediction_IF;

    BranchPredictor branchPredictor(
        .pc_IF(fetchPrediction.pc),
        .prediction_IF,

        .prediction_BR(branchResolve.fetch_BR.prediction),
        .actualPC_BR(branchResolve.branch_BR.actualNextPC),
        .decode_BR(branch.decode),

        .clock,
        .clear
    );

    // Since this check isn't a critical path operation, we will defer it a
    // cycle to simply other timing constraints
    logic pipelineEmpty;

    logic [7:0] idleState, idleState_next;


    assign idleState_next = {
        fetch.idle,
        instructionWait.idle,
        decode.idle,
        execute.idle,
        branch.idle,
        memoryRequest.idle,
        memoryWait.idle,
        !writeBack.fetch.valid
    };

    Register #(.WIDTH($bits(idleState))) idleRegister(
        .d(idleState_next),
        .q(idleState),
        .enable(1'b1),
        .clock,
        .clear
    );

    assign pipelineEmpty = &idleState;

    // If we claim the pipeline is empty, then it should still be empty on this
    // cycle or we don't care since there isn't an interrupt pending. This extra check is necessary to make the assertion happy when we are just starting up
    idleNext: assert property (@(posedge clock) disable iff(clear)
        pipelineEmpty |-> &idleState_next || !asyncInterrupt.interruptPending
    );

    // Unfortunately it is hard to say anything about the state of
    // instructionWait with respect to the inputs shown through the
    // PipelineControl, and it doesn't seem worth it to add another bit beyond
    // the idle bit to validate against.

    decodeIdleWhenInvalid: assert property (@(posedge clock) disable iff(clear)
        decode.decode.instructionType == INVALID_INSTRUCTION |->
        decode.idle || decode.fencing
    );

    decodeInvalidIdle: assert property (@(posedge clock) disable iff(clear)
        decode.idle |-> decode.decode.instructionType == INVALID_INSTRUCTION
    );

    executeIdleWhenInvalid: assert property (@(posedge clock) disable iff(clear)
        execute.decode.instructionType == INVALID_INSTRUCTION |->
        execute.idle || execute.fencing
    );

    executeInvalidIdle: assert property (@(posedge clock) disable iff(clear)
        execute.idle |-> execute.decode.instructionType == INVALID_INSTRUCTION
    );

    branchIdleWhenInvalid: assert property (@(posedge clock) disable iff(clear)
        branch.decode.instructionType == INVALID_INSTRUCTION |->
        branch.idle || branch.fencing
    );

    branchInvalidIdle: assert property (@(posedge clock) disable iff(clear)
        branch.idle |-> branch.decode.instructionType == INVALID_INSTRUCTION
    );

    memoryRequestIdleWhenInvalid: assert property (
        @(posedge clock) disable iff(clear)
        memoryRequest.decode.instructionType == INVALID_INSTRUCTION |->
        memoryRequest.idle || memoryRequest.fencing
    );

    memoryRequestInvalidIdle: assert property (@(posedge clock) disable iff(clear)
        memoryRequest.idle |->
        memoryRequest.decode.instructionType == INVALID_INSTRUCTION
    );

    memoryWaitIdleWhenInvalid: assert property (
        @(posedge clock) disable iff(clear)
        memoryWait.decode.instructionType == INVALID_INSTRUCTION |->
        memoryWait.idle || memoryWait.fencing
    );

    memoryWaitInvalidIdle: assert property (@(posedge clock) disable iff(clear)
        memoryWait.idle |->
        memoryWait.decode.instructionType == INVALID_INSTRUCTION
    );

    logic writeBackForcePC;
    logic branchForcePC;

    // Tracked based on the PC retired in WB and the current state at that time
    // This needs to include code to track MRET/ECALL semantics since interrupts
    // might occur just after an MRET where the expectedNextInstruction is not
    // well defined in a traditional semantic sense.
    Word_t expectedNextRetiredPC;
    Word_t expectedNextRetiredPC_WB;

    Register #(.WIDTH($bits(expectedNextRetiredPC)), .CLEAR_VALUE(`TEXT_START))
    nextPCRegister(
        .q(expectedNextRetiredPC),
        .d(expectedNextRetiredPC_WB),
        .enable(writeBack.fetch.valid || writeBackForcePC),
        .clock,
        .clear
    );

    // This is an important assertion which must be true; otherwise, we may lose
    // instructions when an interrupt arrives at an unfortunate time.
    writeBackNextPredictionCorrect: assert property (
        @(posedge clock) disable iff(clear)
        writeBack.fetch.valid |-> writeBack.fetch.pc == expectedNextRetiredPC
    );

    logic csrConflictStall;

    Word_t nextPC, nextPC_saved;

    always_comb begin
        // By default, just use what branch resolution determines
        expectedNextRetiredPC_WB = writeBack.branch.actualNextPC;
        if(writeBackForcePC) begin
            // Unless there is some extra-ordinary control flow which we should pick up
            expectedNextRetiredPC_WB = nextPC;
        end
    end

    // This may be forced when the instruction stream is stalling, but that is
    // okay, because it will keep being forced until we stop stalling
    assign fetchPrediction.forcePC = writeBackForcePC || branchForcePC;

    pendingExceptionValid: assert property (@(posedge clock) disable iff(clear)
        hasPendingException(writeBack) |-> writeBack.fetch.valid
    );

    incorrectBranchIncorrectNextPC: assert property (
        @(posedge clock) disable iff(clear)
        branchResolve.branch_BR.predictionResult == INCORRECT_PREDICTION |->
        branchResolve.branch_BR.actualNextPC !=
        branchResolve.fetch_BR.prediction.nextPC
    );

    incorrectBranchValid: assert property (@(posedge clock) disable iff(clear)
        branchResolve.branch_BR.predictionResult == INCORRECT_PREDICTION |->
        branchResolve.fetch_BR.valid
    );

    logic generateCoreHalt;
    logic [1:0] faultCount;
    logic startFault;
    Counter #(.WIDTH($bits(faultCount))) faultCounter(
        .q(faultCount),
        .enable(startFault),
        .clock,
        .clear(clear || (writeBack.fetch.valid && !startFault))
    );


    logic forcePC_saved;

    always_comb begin
        fetchPrediction.prediction = prediction_IF;
        if(forcePC_saved) begin
            fetchPrediction.prediction.nextPC = nextPC_saved;
        end

    end

    Register #(.WIDTH($bits(nextPC_saved))) pcSavedRegister(
        .q(nextPC_saved),
        .d(nextPC),
        .clock,
        .clear,
        .enable(1'b1)
    );

    Register #(.WIDTH($bits(forcePC_saved))) forcePCSavedRegister(
        .d(fetchPrediction.forcePC),
        .q(forcePC_saved),
        .clock,
        .clear,
        .enable(1'b1)
    );

    always_comb begin
        nextPC = prediction_IF.nextPC;

        csrInterruptReturn.instructionType = INVALID_INSTRUCTION;

        csrInterruptDelivery.deliverInterrupt = 1'b0;
        csrInterruptDelivery.cause = `WORD_POISON;
        csrInterruptDelivery.value = `WORD_POISON;
        csrInterruptDelivery.epc = `VIRTUAL_ADDRESS_POISON;

        asyncInterrupt.interruptAccepted = 1'b0;

        writeBackForcePC = 1'b0;
        branchForcePC = 1'b0;

        generateCoreHalt = 1'b0;
        coreDebug = 1'b0;

        startFault = 1'b0;

        if(writeBack.decode.instructionType == PRIVILEGE_RAISE) begin
            writeBackForcePC = 1'b1;
            unique case (csrObserver.currentLevel)
                USER_MODE: begin
                    // Deliver ECALL to supervisor mode
                    csrInterruptDelivery.deliverInterrupt = 1'b1;
                    csrInterruptDelivery.cause = USER_ECALL;
                    csrInterruptDelivery.value = 32'b0;
                    // NOTE: The software will need to adjust this to PC+4
                    // before resuming
                    csrInterruptDelivery.epc = writeBack.fetch.pc;
                    nextPC = csrInterruptDelivery.trapVector;
                end
                SUPERVISOR_MODE: begin
                    // Deliver ECALL to machine mode
                    csrInterruptDelivery.deliverInterrupt = 1'b1;
                    csrInterruptDelivery.cause = SUPERVISOR_ECALL;
                    csrInterruptDelivery.value = 32'b0;
                    // NOTE: The software will need to adjust this to PC+4
                    // before resuming
                    csrInterruptDelivery.epc = writeBack.fetch.pc;
                    nextPC = csrInterruptDelivery.trapVector;
                end
                MACHINE_MODE: begin
                    unique case(writeBack.decode.rs1.result)
                        `ECALL_ARG_HALT: begin
                            generateCoreHalt = 1'b1;
                            nextPC = 32'b0;
                        end
                        `ECALL_ARG_DEBUG: begin
                            nextPC = writeBack.branch.actualNextPC;
                                // This is combinational and the remote
                                // interface will halt the core when it sees the
                                // debug symbol. We'll also setup instruction
                                // fetch to pickup PC+4, but it won't be able to
                                // continue until halted is cleared.
                            coreDebug = 1'b1;
                        end
                        default: begin
                            // Ignore ECALLS that we aren't handling (PC+4)
                            nextPC = writeBack.branch.actualNextPC;
                        end
                    endcase
                end
            endcase
        end else if(hasPendingException(writeBack)) begin
            // FIXME: This branch of the if statement should probably be first.
            // It is unclear why privilege raise is given priority (other than
            // they can't happen at the same time anyway)
            startFault = 1'b1;
            writeBackForcePC = 1'b1;

`ifdef SIMULATION_18447
            if(faultCount != 2'd0) begin
                $display("Double Fault: %d %d", faultCount, $time);
            end
`endif

            // TODO: It might be nice to have some double fault semantics
            if(faultCount <= 2'd1) begin
                nextPC = csrInterruptDelivery.trapVector;

                csrInterruptDelivery.deliverInterrupt = 1'b1;
                csrInterruptDelivery.cause = exceptionCause_WB;
                csrInterruptDelivery.epc = writeBack.fetch.pc;
                csrInterruptDelivery.value = tval_WB;
            end else begin
                // Triple fault
                `ifdef SIMULATION_18447
                $error("Triple Fault! %d",$time);
                `endif
                generateCoreHalt = 1'b1;
                nextPC = 32'd3;
            end
        end else if (writeBack.decode.instructionType == PRIVILEGE_LOWER_SRET ||
            writeBack.decode.instructionType == PRIVILEGE_LOWER_MRET
        ) begin
            writeBackForcePC = 1'b1;
            nextPC = csrInterruptReturn.epc;

            csrInterruptReturn.instructionType = writeBack.decode.instructionType;
        end else if (decodeGenerateFence(writeBack.decode)) begin
            writeBackForcePC = 1'b1;
            nextPC = writeBack.branch.actualNextPC;
        end else if(branchResolve.branch_BR.predictionResult == INCORRECT_PREDICTION) begin
            branchForcePC = 1'b1;
            nextPC = branchResolve.branch_BR.actualNextPC;
        end else if (pipelineEmpty && asyncInterrupt.interruptPending && !remoteHalted) begin
            writeBackForcePC = 1'b1;
            nextPC = csrInterruptDelivery.trapVector;

            csrInterruptDelivery.deliverInterrupt = 1'b1;
            csrInterruptDelivery.cause = asyncInterrupt.cause;
            csrInterruptDelivery.value = asyncInterrupt.tval;
            csrInterruptDelivery.epc = expectedNextRetiredPC;

            asyncInterrupt.interruptAccepted = 1'b1;
        end

    end

    `ifdef TRACE
    always @(posedge clock) begin
        if(writeBack.decode.instructionType == PRIVILEGE_LOWER_MRET ||
           writeBack.decode.instructionType == PRIVILEGE_LOWER_SRET
        ) begin
            $display("%20s %d: epc: %x type: %s",
                "Interrupt Return ",
                $time,
                nextPC,
                writeBack.decode.instructionType.name());
        end else if(asyncInterrupt.interruptAccepted) begin
            $display("%20s %d: epc: %x cause: %x value: %x tvec: %x",
                "Interrupt Delivered",
                $time,
                csrInterruptDelivery.epc,
                csrInterruptDelivery.cause,
                csrInterruptDelivery.value,
                csrInterruptDelivery.trapVector
            );
        end
    end

    `endif

    CSRConflictDetector csrConflictDetector(
        .decode_ID(decode.decode),
        .decode_EX(execute.decode),
        .decode_BR(branch.decode),
        .decode_MR(memoryRequest.decode),
        .decode_MW(memoryWait.decode),
        .decode_WB(writeBack.decode),
        .csrConflictStall
    );

    // Flush/Stall bits are set following a strict prioritization based on
    // instruction age. The bad situation we are trying to avoid is having a
    // miss-prediction in BR get dropped due to a stall being generated in MR/MW
    // Flush: set based on the next stages pending exceptions/fences
    // Stall: set based on the current stages ready status

    pipelineEmptyWhenHalted: assert property (@(posedge clock) disable iff(clear)
        remoteHalted |-> &idleState_next ##1 pipelineEmpty
    );

    // This actually isn't strictly true; a bad user could break this invariant
    // and screw themselves
    noPriorFlushStallOnHalt: assert property (@(posedge clock) disable iff(clear)
        remoteHalted |-> !instructionWait.flush && !instructionWait.stall
    );

    BackPressure_t instructionWaitBackPressure;
    BackPressure_t decodeBackPressure;
    BackPressure_t executeBackPressure;
    BackPressure_t branchBackPressure;
    BackPressure_t memoryRequestBackPressure;
    BackPressure_t memoryWaitBackPressure;
    BackPressure_t writeBackBackPressure;

    fetchNeverFencing: assert property (@(posedge clock) disable iff(clear)
        !fetch.fencing
    );

    logic fetchDrain;

    always_comb begin
        fetchDrain = 1'b0;
        if(!remoteHalted && asyncInterrupt.interruptPending) begin
            fetchDrain = 1'b1;
        end
    end

    PipelineControlFlow fetchControl(
        .backPressure_older(instructionWaitBackPressure),
        .fencing(fetch.fencing),
        .ready(fetch.ready && !fetchDrain && !remoteHalted),
        .flush(fetch.flush),
        .stall(fetch.stall),
        .backPressure() // Not connected, nothing before fetch
    );


    PipelineControlFlow instructionWaitControl(
        .backPressure_older(decodeBackPressure),
        .fencing(instructionWait.fencing),
        .ready(instructionWait.ready),
        .flush(instructionWait.flush),
        .stall(instructionWait.stall),
        .backPressure(instructionWaitBackPressure)
    );

    PipelineControlFlow decodeControl(
        .backPressure_older(executeBackPressure),
        .fencing(decode.fencing),
        .ready(decode.ready && !csrConflictStall),
        .flush(decode.flush),
        .stall(decode.stall),
        .backPressure(decodeBackPressure)
    );

    // TODO: This is true for now, but might change if we support hardware multiply
    executeAlwaysReady: assert property (@(posedge clock) disable iff(clear)
        execute.ready
    );

    PipelineControlFlow executeControl(
        .backPressure_older(branchBackPressure),
        .fencing(execute.fencing),
        .ready(execute.ready),
        .flush(execute.flush),
        .stall(execute.stall),
        .backPressure(executeBackPressure)
    );

    branchAlwaysReady: assert property (@(posedge clock) disable iff(clear)
        branch.ready
    );

    PipelineControlFlow branchControl(
        .backPressure_older(memoryRequestBackPressure),
        .fencing(branch.fencing || branchForcePC),
        .ready(branch.ready),
        .flush(branch.flush),
        .stall(branch.stall),
        .backPressure(branchBackPressure)
    );

    PipelineControlFlow memoryRequestControl(
        .backPressure_older(memoryWaitBackPressure),
        .fencing(memoryRequest.fencing),
        .ready(memoryRequest.ready),
        .flush(memoryRequest.flush),
        .stall(memoryRequest.stall),
        .backPressure(memoryRequestBackPressure)
    );

    PipelineControlFlow memoryWaitControl(
        .backPressure_older(writeBackBackPressure),
        .fencing(memoryWait.fencing),
        .ready(memoryWait.ready),
        .flush(memoryWait.flush),
        .stall(memoryWait.stall),
        .backPressure(memoryWaitBackPressure)
    );

    assign writeBackBackPressure.flush = writeBackForcePC;
    assign writeBackBackPressure.stall = 1'b0;

    // Flush and stall should not be asserted at the same time because the flush
    // will override
    fetchSafety: assert property(@(posedge clock) disable iff(clear)
        !(fetch.flush && fetch.stall)
    );

    instructionWaitSafety: assert property (@(posedge clock) disable iff(clear)
        !(instructionWait.flush && instructionWait.stall)
    );

    decodeSafety: assert property (@(posedge clock) disable iff(clear)
        !(decode.flush && decode.stall)
    );

    executeSafety: assert property (@(posedge clock) disable iff(clear)
        !(execute.flush && execute.stall)
    );

    branchResolveSafety: assert property (@(posedge clock) disable iff(clear)
        !(branch.flush && branch.stall)
    );

    memoryRequestSafety: assert property (@(posedge clock) disable iff(clear)
        !(memoryRequest.flush && memoryRequest.stall)
    );

    memoryWaitSafety: assert property (@(posedge clock) disable iff(clear)
        !(memoryWait.flush && memoryWait.stall)
    );

    logic savedCoredHalt;
    assign coreHalt = savedCoredHalt || generateCoreHalt;

    Register #(.WIDTH(1)) coreHaltOneWayRegister(
        .q(savedCoredHalt),
        .d(generateCoreHalt),
        .enable(generateCoreHalt),
        .clock,
        .clear
    );

endmodule

module PipelineControlFlow(
    input BackPressure_t backPressure_older,
    input logic ready, fencing,
    output logic flush, stall,
    output BackPressure_t backPressure
);

    always_comb begin
        flush = 1'b0;
        stall = 1'b0;
        backPressure.flush = 1'b0;
        backPressure.stall = 1'b0;
        if(backPressure_older.flush || backPressure_older.stall) begin
            flush = backPressure_older.flush;
            stall = backPressure_older.stall;
            backPressure.flush = backPressure_older.flush;
            backPressure.stall = backPressure_older.stall;
        end else if(!ready) begin
            flush = 1'b1;
            backPressure.stall = 1'b1;
        end else if(fencing) begin
            backPressure.flush = 1'b1;
        end
    end

endmodule

module CSRConflictDetector(
    input InstructionDecodeState_t decode_ID,

    input InstructionDecodeState_t decode_EX,
    input InstructionDecodeState_t decode_BR,
    input InstructionDecodeState_t decode_MR,
    input InstructionDecodeState_t decode_MW,
    input InstructionDecodeState_t decode_WB,

    output logic csrConflictStall
);

    function automatic logic csrMatch(InstructionDecodeState_t state, ControlStatusRegisterName_t name);
        if(state.csrOperation == NO_CSR_OPERATION) return 1'b0;
        return state.csrName == name;
    endfunction

    // In theory we could also do some fancy forwarding here, but since CSR
    // operations aren't very common we won't optimize the forwarding path
    always_comb begin
        csrConflictStall = 1'b0;
        if(decode_ID.csrOperation == NO_CSR_OPERATION) begin
            csrConflictStall = 1'b0;
        end else begin
            if(csrMatch(decode_EX, decode_ID.csrName)) begin
                csrConflictStall = 1'b1;
            end else if(csrMatch(decode_BR, decode_ID.csrName)) begin
                csrConflictStall = 1'b1;
            end else if(csrMatch(decode_MR, decode_ID.csrName)) begin
                csrConflictStall = 1'b1;
            end else if(csrMatch(decode_MW, decode_ID.csrName)) begin
                csrConflictStall = 1'b1;
            end else if(csrMatch(decode_WB, decode_ID.csrName)) begin
                csrConflictStall = 1'b1;
            end
        end
    end

endmodule

module ExceptionSelection(
    input MemoryWaitExport_t writeBack,
    output ExceptionCause_t exceptionCause,
    output Word_t tval,
    input logic clock, clear
);
    `STATE_FUNCTIONS

    writeBackHasExceptionValid: assert property (@(posedge clock) disable iff(clear)
        hasPendingException(writeBack) |-> writeBack.fetch.valid
    );

    writeBackDataPageFaultHasMemoryOperation: assert property (@(posedge clock) disable iff(clear)
        getDataResponseType(writeBack) == PAGE_FAULT |-> getMemoryOperation(writeBack) != NO_MEMORY_OPERATION
    );

    always_comb begin
        exceptionCause = RESERVED_INVALID_CAUSE;
        tval = `WORD_POISON;
        if(getInstructionResponseType(writeBack) == PAGE_FAULT) begin
            exceptionCause = INSTRUCTION_PAGE_FAULT;
            tval = writeBack.instructionWait.instruction.request.virtualAddress;
        end else if(getDataResponseType(writeBack) == PAGE_FAULT) begin
            tval = writeBack.memoryWait.response.request.virtualAddress;
            unique case(getMemoryOperation(writeBack))
                LOAD_BYTE: exceptionCause = LOAD_PAGE_FAULT;
                LOAD_HALF_WORD: exceptionCause = LOAD_PAGE_FAULT;
                LOAD_WORD: exceptionCause = LOAD_PAGE_FAULT;
                LOAD_BYTE_UNSIGNED: exceptionCause = LOAD_PAGE_FAULT;
                LOAD_HALF_WORD_UNSIGNED: exceptionCause = LOAD_PAGE_FAULT;
                STORE_BYTE: exceptionCause = STORE_PAGE_FAULT;
                STORE_HALF_WORD: exceptionCause = STORE_PAGE_FAULT;
                STORE_WORD: exceptionCause = STORE_PAGE_FAULT;
                LOAD_RESERVED: exceptionCause = STORE_PAGE_FAULT;
                STORE_CONDITIONAL: exceptionCause = STORE_PAGE_FAULT;
            endcase
        end else if(writeBack.decode.instructionType == ILLEGAL_INSTRUCTION_FAULT) begin
            exceptionCause = INSTRUCTION_ILLEGAL;
            tval = writeBack.instructionWait.instruction.readData;
        end
    end

endmodule