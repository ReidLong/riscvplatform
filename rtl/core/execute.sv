//# PRINT AFTER include/core/execute.vh
`default_nettype none

`include "pipeline.vh"
`include "decode.vh"
`include "execute.vh"
`include "register_file.vh"
`include "riscv_types.vh"

module ExecuteStage(
    PipelineControl.Stage controlFlow,

    input InstructionDecodeExport_t decode_EX,

    RegisterReport.Reporter rd_EX,

    output ExecuteExport_t execute_BR,

    input logic clock, clear
);

    `DECODE_FUNCTIONS
    `STATE_FUNCTIONS

    ExecuteState_t executeState;
    Word_t aluOperand1, aluOperand2;

    aluSelecValidInstruction1: assert property (@(posedge clock) disable iff(clear)
        decode_EX.decode.aluOperand1 != ALU_SELECT_ZERO |-> decode_EX.fetch.valid
    );

    aluSelectRegister1Valid: assert property (@(posedge clock) disable iff(clear)
        decode_EX.decode.aluOperand1 == ALU_SELECT_REGISTER |-> decode_EX.decode.rs1.resultValid
    );

    ArithmeticLogicUnitOperandSelector aluOperandSelector1(
        .decode(decode_EX),
        .registerValue(decode_EX.decode.rs1.result),
        .operandSelect(decode_EX.decode.aluOperand1),
        .aluOperand(aluOperand1)
    );

    aluSelecValidInstruction2: assert property (@(posedge clock) disable iff(clear)
        decode_EX.decode.aluOperand2 != ALU_SELECT_ZERO |-> decode_EX.fetch.valid
    );

    aluSelectRegister2Valid: assert property (@(posedge clock) disable iff(clear)
        decode_EX.decode.aluOperand2 == ALU_SELECT_REGISTER |-> decode_EX.decode.rs2.resultValid
    );

    ArithmeticLogicUnitOperandSelector aluOperandSelector2(
        .decode(decode_EX),
        .registerValue(decode_EX.decode.rs2.result),
        .operandSelect(decode_EX.decode.aluOperand2),
        .aluOperand(aluOperand2)
    );

    ArithmeticLogicUnit alu(
        .aluOperand1,
        .aluOperand2,
        .operation(decode_EX.decode.aluOperation),
        .aluResult(executeState.aluResult)
    );

    Word_t jumpBase;
    always_comb begin
        jumpBase = 32'b0;
        unique case(decode_EX.decode.jumpBaseSelect)
            JUMP_SELECT_ZERO: jumpBase = 32'b0;
            JUMP_SELECT_PC: jumpBase = decode_EX.fetch.pc;
            JUMP_SELECT_RS1: jumpBase = decode_EX.decode.rs1.result;
        endcase
    end

    assign executeState.jumpTarget = jumpBase + decode_EX.decode.immediate.value;

    always_comb begin
        executeState.rd_EX = '{
            register: decode_EX.decode.rd,
            result: `WORD_POISON,
            resultValid: 1'b0
        };
        case(decode_EX.decode.writeBackSelect)
            WRITE_BACK_SELECT_ALU: begin
                executeState.rd_EX.result = executeState.aluResult;
                executeState.rd_EX.resultValid = 1'b1;
            end
            WRITE_BACK_SELECT_PC_PLUS_4: begin
                executeState.rd_EX.result = decode_EX.fetch.pc + 32'd4;
                executeState.rd_EX.resultValid = 1'b1;
            end
            WRITE_BACK_SELECT_CSR: begin
                executeState.rd_EX.result = decode_EX.decode.csrValue;
                executeState.rd_EX.resultValid = 1'b1;
            end
        endcase
    end

    assign rd_EX.response = executeState.rd_EX;

    always_comb begin
       controlFlow.fencing = 1'b0;
       if(decode_EX.fetch.valid) begin
            if(hasPendingException_IW(decode_EX.instructionWait) ||
                decodeGenerateFence(decode_EX.decode)
            ) begin
                controlFlow.fencing = 1'b1;
            end
       end
    end

    assign controlFlow.ready = 1'b1; // EX always ready
    assign controlFlow.idle = !decode_EX.fetch.valid;
    assign controlFlow.decode = decode_EX.decode;


    ExecuteExport_t executeExport_EX_END;

    assign executeExport_EX_END.fetch = decode_EX.fetch;
    assign executeExport_EX_END.instructionWait = decode_EX.instructionWait;
    assign executeExport_EX_END.decode = decode_EX.decode;
    assign executeExport_EX_END.execute = executeState;

    Register #(.WIDTH($bits(execute_BR))) executePipelineRegister(
        .d(executeExport_EX_END),
        .q(execute_BR),
        .enable(!controlFlow.stall),
        .clock,
        .clear(clear || controlFlow.flush)
    );

endmodule // ExecuteStage

module ArithmeticLogicUnitOperandSelector(
    input InstructionDecodeExport_t decode,
    input Word_t registerValue,
    input ArithmeticOperandSelect_t operandSelect,
    output Word_t aluOperand
);
    always_comb begin
        aluOperand = `WORD_POISON;
        unique case(operandSelect)
            ALU_SELECT_ZERO: aluOperand = 32'b0;
            ALU_SELECT_REGISTER: aluOperand = registerValue;
            ALU_SELECT_PC: aluOperand = decode.fetch.pc;
            ALU_SELECT_IMMEDIATE: aluOperand = decode.decode.immediate.value;
            ALU_SELECT_CSR: aluOperand = decode.decode.csrValue;
        endcase
    end
endmodule // ArithmeticLogicUnitOperandSelector

module ArithmeticLogicUnit(
    input Word_t aluOperand1, aluOperand2,
    input ArithmeticOperation_t operation,

    output Word_t aluResult
);

    always_comb begin
        aluResult = `WORD_POISON;
        unique case (operation)
            ALU_ADD: aluResult = aluOperand1 + aluOperand2;
            ALU_SUBTRACT: aluResult = aluOperand1 - aluOperand2;
            ALU_OR: aluResult = aluOperand1 | aluOperand2;
            ALU_XOR: aluResult = aluOperand1 ^ aluOperand2;
            ALU_AND: aluResult = aluOperand1 & aluOperand2;
            ALU_CLEAR_MASK: aluResult = ~aluOperand1 & aluOperand2;
            ALU_SHIFT_LEFT: aluResult = aluOperand1 << aluOperand2[4:0];
            ALU_SHIFT_RIGHT_LOGICAL: aluResult = $unsigned(aluOperand1) >> aluOperand2[4:0];
            ALU_SHIFT_RIGHT_ARITHMETIC: aluResult = $signed(aluOperand1) >>> aluOperand2[4:0];
            ALU_EQUAL: aluResult = (aluOperand1 == aluOperand2);
            ALU_NOT_EQUAL: aluResult = (aluOperand1 != aluOperand2);
            ALU_LESS_THAN_SIGNED: aluResult = ($signed(aluOperand1) < $signed(aluOperand2));
            ALU_LESS_THAN_UNSIGNED: aluResult = ($unsigned(aluOperand1) < $unsigned(aluOperand2));
            ALU_GREATER_THAN_OR_EQUAL_SIGNED: aluResult = ($signed(aluOperand1) >= $signed(aluOperand2));
            ALU_GREATER_THAN_OR_EQUAL_UNSIGNED: aluResult = ($unsigned(aluOperand1) >= $unsigned(aluOperand2));
        endcase
    end

endmodule