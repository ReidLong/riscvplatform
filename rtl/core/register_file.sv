//# PRINT AFTER include/core/register_file.vh
`default_nettype none

`include "register_file.vh"
`include "riscv_isa.vh"

module RegisterFile(
    RegisterFileRequest.RegisterFile rs1_ID, rs2_ID, // Forwarding
    RegisterFileRequest.RegisterFile remote, // Does not forward

    RegisterReport.RegisterFile rd_EX,
    RegisterReport.RegisterFile rd_BR,
    RegisterReport.RegisterFile rd_MR,
    RegisterReport.RegisterFile rd_MW,
    RegisterReport.RegisterFile rd_WB,

    `ifdef SIMULATION_18447
    input logic dump,
    `endif

    input logic clock, clear
);

    reportValidCheck_WB: assert property (@(posedge clock) disable iff (clear)
        rd_WB.response.resultValid |-> rd_WB.response.register.isValid
    );

    reportValidCheck_MW: assert property (@(posedge clock) disable iff(clear)
        rd_MW.response.resultValid |-> rd_MW.response.register.isValid
    );

    reportValidCheck_MR: assert property (@(posedge clock) disable iff(clear)
        rd_MR.response.resultValid |-> rd_MR.response.register.isValid
    );

    reportValidCheck_BR: assert property (@(posedge clock) disable iff(clear)
        rd_BR.response.resultValid |-> rd_BR.response.register.isValid
    );

    reportValidCheck_EX: assert property (@(posedge clock) disable iff(clear)
        rd_EX.response.resultValid |-> rd_EX.response.register.isValid
    );

    writeBackValid: assert property (@(posedge clock) disable iff(clear)
        rd_WB.response.register.isValid |-> rd_WB.response.resultValid
    );

    RegisterResponse_t rs1_RF, rs2_RF;

    SimpleRegisterFile #(.NUM_REGS(`NUM_REGS), .WIDTH(`XLEN)) registerFile(
        .rs1(rs1_ID.request.number),
        .rs1_data(rs1_RF.result),

        .rs2(rs2_ID.request.number),
        .rs2_data(rs2_RF.result),

        .rs3(remote.request.number),
        .rs3_data(remote.response.result),

        .rd(rd_WB.response.register.number),
        .rd_data(rd_WB.response.result),
        .rd_write(rd_WB.response.resultValid),

`ifdef SIMULATION_18447
        .dump,
`endif
        .clock,
        .clear
    );

    always_comb begin
        // Since we aren't forwarding, the result is always valid
        remote.response.resultValid = 1'b1;
        remote.response.register = remote.request;
    end

    always_comb begin
        rs1_RF.register = rs1_ID.request;
        rs2_RF.register = rs2_ID.request;
        rs1_RF.resultValid = rs1_ID.request.isValid;
        rs2_RF.resultValid = rs2_ID.request.isValid;
    end

    ForwardingRegister rs1Forwarding(
        .register(rs1_ID),
        .rd_RF(rs1_RF),
        .rd_EX(rd_EX.response),
        .rd_BR(rd_BR.response),
        .rd_MR(rd_MR.response),
        .rd_MW(rd_MW.response),
        .rd_WB(rd_WB.response),

        .clock,
        .clear
    );

    ForwardingRegister rs2Forwarding(
        .register(rs2_ID),
        .rd_RF(rs2_RF),
        .rd_EX(rd_EX.response),
        .rd_BR(rd_BR.response),
        .rd_MR(rd_MR.response),
        .rd_MW(rd_MW.response),
        .rd_WB(rd_WB.response),

        .clock,
        .clear
    );

endmodule


module ForwardingRegister(
    RegisterFileRequest.RegisterFile register,

    input RegisterResponse_t rd_EX,
    input RegisterResponse_t rd_BR,
    input RegisterResponse_t rd_MR,
    input RegisterResponse_t rd_MW,
    input RegisterResponse_t rd_WB,
    input RegisterResponse_t rd_RF,

    input logic clock, clear
);

    function automatic logic registerMatch(Register_t a, Register_t b);
        return a.isValid && b.isValid && (a.number == b.number);
    endfunction

    registerFileMatches: assert property (@(posedge clock) disable iff(clear)
        register.request.isValid |-> registerMatch(register.request, rd_RF.register)
    );

    writeBackValid: assert property (@(posedge clock) disable iff(clear)
        register.request.isValid &&
        register.request.number != 5'd0 &&
        registerMatch(register.request, rd_WB.register) |->
        rd_WB.resultValid
    );

    always_comb begin
        register.response = '{
            register: register.request,
            result: `WORD_POISON,
            resultValid: 1'b0
        };
        if(register.request.number == 5'd0 || !register.request.isValid) begin
            register.response.result = 32'b0;
            register.response.resultValid = register.request.isValid;
        end else begin
            if(registerMatch(register.request, rd_EX.register)) begin
                if(rd_EX.resultValid) begin
                    register.response.result = rd_EX.result;
                    register.response.resultValid = 1'b1; // Forward Successful
                end
            end else if(registerMatch(register.request, rd_BR.register)) begin
                if(rd_BR.resultValid) begin
                    register.response.result = rd_BR.result;
                    register.response.resultValid = 1'b1; // Forward Successful
                end
            end else if(registerMatch(register.request, rd_MR.register)) begin
                if(rd_MR.resultValid) begin
                    register.response.result = rd_MR.result;
                    register.response.resultValid = 1'b1; // Forward Successful
                end
            end else if(registerMatch(register.request, rd_MW.register)) begin
                if(rd_MW.resultValid) begin
                    register.response.result = rd_MW.result;
                    register.response.resultValid = 1'b1; // Forward Successful
                end
            end else if(registerMatch(register.request, rd_WB.register)) begin
                // Write back should always be valid
                register.response.result = rd_WB.result;
                register.response.resultValid = 1'b1;
            end else begin
                register.response.result = rd_RF.result;
                register.response.resultValid = 1'b1;
            end
        end
    end

endmodule