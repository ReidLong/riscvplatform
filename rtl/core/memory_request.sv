//# PRINT AFTER include/core/memory_stage.vh
`default_nettype none

`include "control_flow.vh"
`include "pipeline.vh"
`include "memory.vh"
`include "memory_stage.vh"
`include "register_file.vh"
`include "branch.vh"
`include "decode.vh"
`include "riscv_types.vh"

typedef struct packed {
    logic valid;
    VirtualAddress_t address;
} Reservation_t;

module MemoryRequestStage(
    PipelineControl.Stage controlFlow,

    input BranchResolveExport_t branch_MR,

    output MemoryRequest_t memoryRequest,
    input logic memoryReady,
    RegisterReport.Reporter rd_MR,

    output MemoryRequestExport_t memoryRequest_MW,

    input logic clock, clear
);

    `DECODE_FUNCTIONS
    `STATE_FUNCTIONS

    noMemoryOperationUnlessValid: assert property (@(posedge clock) disable iff(clear)
        branch_MR.decode.memoryOperation != NO_MEMORY_OPERATION |-> branch_MR.fetch.valid
    );

    MemoryRequestState_t requestState;

    Word_t address;
    assign address = branch_MR.execute.aluResult;

    Word_t writeData_aligned;
    always_comb begin
        unique case(address[1:0])
            2'd0: writeData_aligned = branch_MR.decode.rs2.result;
            2'd1: writeData_aligned = branch_MR.decode.rs2.result << 5'd8;
            2'd2: writeData_aligned = branch_MR.decode.rs2.result << 5'd16;
            2'd3: writeData_aligned = branch_MR.decode.rs2.result << 5'd24;
        endcase
    end

    Reservation_t reservation, reservation_next;
    logic reservation_clear, reservation_load;

    assign reservation_next = '{
        valid: reservation_load,
        address: address
    };

    alignedReservation: assert property (@(posedge clock) disable iff(clear)
        reservation.valid |-> reservation.address[1:0] == 2'd0
    );

    alignedHalfWord: assert property (@(posedge clock) disable iff(clear)
        branch_MR.decode.memoryOperation == STORE_HALF_WORD ||
        branch_MR.decode.memoryOperation == LOAD_HALF_WORD ||
        branch_MR.decode.memoryOperation == LOAD_HALF_WORD_UNSIGNED |->
        address[0] == 1'b0
    );

    alignedWord: assert property (@(posedge clock) disable iff(clear)
        branch_MR.decode.memoryOperation == STORE_WORD ||
        branch_MR.decode.memoryOperation == LOAD_WORD ||
        branch_MR.decode.memoryOperation == LOAD_RESERVED ||
        branch_MR.decode.memoryOperation == STORE_CONDITIONAL |->
        address[1:0] == 2'b0
    );

    alignedFlush: assert property (@(posedge clock) disable iff(clear)
        branch_MR.decode.memoryOperation == FLUSH_TRANSLATION |->
        address[1:0] == 2'b0
    );

    Register #(.WIDTH($bits(reservation))) reservationRegister(
        .d(reservation_next),
        .q(reservation),
        .enable(reservation_load),
        // If we stall of memoryWait stall's, we don't want to clear the reservation bit yet
        .clear(clear || (reservation_clear && !controlFlow.stall && !controlFlow.flush)),
        .clock
    );

    // TODO: This seems like it needs to be predicated on being ready/not
    // stalling so we don't clear if we are going to stall.
    always_comb begin : requestGeneration
        memoryRequest = '{
            readEnable: 1'b0,
            writeEnable: 4'b0,
            virtualAddress: {address[31:2], 2'b0},
            writeData: writeData_aligned,
            flushMode: KEEP
        };
        reservation_clear = 1'b0;
        reservation_load = 1'b0;
        requestState.storeConditionalResult = NO_CONDITIONAL;

        unique case(branch_MR.decode.memoryOperation)
            NO_MEMORY_OPERATION: begin
                // We should invalidate the LR/SC reservation if we change privilege level
                unique case(branch_MR.decode.instructionType)
                    INVALID_INSTRUCTION: reservation_clear = 1'b0;
                    ILLEGAL_INSTRUCTION_FAULT: begin
                        // This is a fault
                        reservation_clear = 1'b1;
                    end
                    NORMAL: reservation_clear = 1'b0;
                    JUMP: reservation_clear = 1'b0;
                    BRANCH: reservation_clear = 1'b0;
                    CSR: begin
                        // SATP doesn't get any special behavior
                        reservation_clear = 1'b0;
                    end
                    FENCE: begin
                        // Generic fences don't clear, but SFENCE.VMA is a
                        // FLUSH_TRANSLATION memory op so we cover it below
                        reservation_clear = 1'b0;
                    end
                    PRIVILEGE_RAISE: reservation_clear = 1'b1;
                    PRIVILEGE_LOWER_SRET: reservation_clear = 1'b1;
                    PRIVILEGE_LOWER_MRET: reservation_clear = 1'b1;
                endcase // branch_MR.decode.instructionType
            end
            LOAD_BYTE: begin
                reservation_clear = 1'b1;
                memoryRequest.readEnable = 1'b1;
            end
            LOAD_HALF_WORD: begin
                reservation_clear = 1'b1;
                memoryRequest.readEnable = 1'b1;
            end
            LOAD_WORD: begin
                reservation_clear = 1'b1;
                memoryRequest.readEnable = 1'b1;
            end
            LOAD_BYTE_UNSIGNED: begin
                reservation_clear = 1'b1;
                memoryRequest.readEnable = 1'b1;
            end
            LOAD_HALF_WORD_UNSIGNED: begin
                reservation_clear = 1'b1;
                memoryRequest.readEnable = 1'b1;
            end
            STORE_BYTE: begin
                reservation_clear = 1'b1;
                memoryRequest.writeEnable = 4'b0001 << address[1:0];
            end
            STORE_HALF_WORD: begin
                reservation_clear = 1'b1;
                memoryRequest.writeEnable = 4'b0011 << address[1:0];
            end
            STORE_WORD: begin
                reservation_clear = 1'b1;
                memoryRequest.writeEnable = 4'b1111;
            end
            LOAD_RESERVED: begin
                reservation_load = 1'b1;
                memoryRequest.readEnable = 1'b1;
            end
            STORE_CONDITIONAL: begin
                if(reservation.valid && (reservation.address == address)) begin
                    memoryRequest.writeEnable = 4'b1111;
                    requestState.storeConditionalResult = STORE_SUCCESS;
                end else begin
                    requestState.storeConditionalResult = STORE_REJECTED;
                end
                reservation_clear = 1'b1;
            end
            FLUSH_TRANSLATION: begin
                if(branch_MR.decode.rs1.register.number == X0) begin
                    memoryRequest.flushMode = INVALIDATE_ALL;
                end else begin
                    memoryRequest.flushMode = INVALIDATE_PAGE;
                end
                reservation_clear = 1'b1;
            end
        endcase // branch_MR.decode.instructionType
    end // requestGeneration

    `ifdef SIMULATION_18447
    always @(posedge clock) begin
        if(branch_MR.decode.memoryOperation == STORE_CONDITIONAL && requestState.storeConditionalResult == STORE_REJECTED) begin
            $display("%p %p", reservation, branch_MR);
            $display("SC Rejected: %d %x", $time, address);
        end
    end
    `endif

    // New rd isn't computed during MR (even the SC return value isn't confirmed
    // because we might page fault)
    assign rd_MR.response = branch_MR.execute.rd_EX;

    always_comb begin
        controlFlow.fencing = 1'b0;
        controlFlow.ready = 1'b1;
        if(branch_MR.fetch.valid) begin
            controlFlow.fencing = decodeGenerateFence(memoryRequest_MW.decode);
            if(hasPendingException_IW(branch_MR.instructionWait)) begin
                controlFlow.fencing = 1'b1; // Force a fence
                controlFlow.ready = 1'b1;
            end else if(branch_MR.decode.memoryOperation == NO_MEMORY_OPERATION) begin
                controlFlow.ready = 1'b1; // No memory operation, nothing to do
            end else begin
                // We have a memory operation here, so ready is based on memoryReady
                controlFlow.ready = memoryReady;
            end
        end
    end

    assign controlFlow.idle = !branch_MR.fetch.valid;
    assign controlFlow.decode = branch_MR.decode;

    MemoryRequestExport_t memoryRequest_MR_END;

    assign memoryRequest_MR_END.fetch = branch_MR.fetch;
    assign memoryRequest_MR_END.instructionWait = branch_MR.instructionWait;
    assign memoryRequest_MR_END.decode = branch_MR.decode;
    assign memoryRequest_MR_END.execute = branch_MR.execute;
    assign memoryRequest_MR_END.branch = branch_MR.branch;
    assign memoryRequest_MR_END.memoryRequest = requestState;

    Register #(.WIDTH($bits(memoryRequest_MW))) memoryRequestPipelineRegister(
        .d(memoryRequest_MR_END),
        .q(memoryRequest_MW),
        .enable(!controlFlow.stall),
        .clock,
        .clear(clear || controlFlow.flush)
    );


endmodule