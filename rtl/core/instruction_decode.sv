//# PRINT AFTER core/riscv_decoder.sv
`default_nettype none

`include "control_flow.vh"
`include "pipeline.vh"
`include "decode.vh"
`include "control_status_registers.vh"
`include "register_file.vh"

module InstructionDecodeStage(
    PipelineControl.Stage controlFlow,

    input InstructionWaitExport_t instructionWait_ID,

    CSRReadInterface.Requester csr,
    RegisterFileRequest.Requester rs1, rs2,
    CSRCoreObserver.Observer csrObserver,

    output InstructionDecodeExport_t decode_EX,

    input logic clock, clear
);

    `STATE_FUNCTIONS

    InstructionDecodeState_t decodeState_decoder, decodeState_selected;

    RISCVDecoder decoder(
        .instruction(instructionWait_ID.instructionWait.instruction.readData),
        .decodeState(decodeState_decoder),

        .csr,
        .rs1,
        .rs2,
        .csrObserver,

        .clock,
        .clear
    );

    always_comb begin
        controlFlow.ready = 1'b1; // Default to ready
        controlFlow.fencing = 1'b0;
        decodeState_selected = '{default:0};
        if(instructionWait_ID.fetch.valid) begin
            if(hasPendingException_IW(instructionWait_ID.instructionWait)) begin
                controlFlow.ready = 1'b1;
                controlFlow.fencing = 1'b1;
                // TODO: This is not strictly true, but close enough since we
                // have a valid instruction, but it happens to be a fault
                // already.
                decodeState_selected.instructionType = ILLEGAL_INSTRUCTION_FAULT;
            end else if(decodeState_decoder.instructionType == ILLEGAL_INSTRUCTION_FAULT) begin
                controlFlow.ready = 1'b1;
                controlFlow.fencing = 1'b1;
                decodeState_selected = decodeState_decoder;
            end else if((decodeState_decoder.rs1.register.isValid && !rs1.response.resultValid) ||
                (decodeState_decoder.rs2.register.isValid && !rs2.response.resultValid)
            ) begin
                // Stall if the registers aren't ready
                controlFlow.ready = 1'b0;
                decodeState_selected = decodeState_decoder;
            end else begin
                // Good to go
                controlFlow.ready = 1'b1;
                decodeState_selected = decodeState_decoder;
            end
        end
    end

    assign controlFlow.idle = !instructionWait_ID.fetch.valid;
    assign controlFlow.decode = decodeState_selected;

    InstructionDecodeExport_t decodeExport_ID_END;

    assign decodeExport_ID_END.fetch = instructionWait_ID.fetch;
    assign decodeExport_ID_END.instructionWait = instructionWait_ID.instructionWait;
    assign decodeExport_ID_END.decode = decodeState_selected;

    Register #(.WIDTH($bits(decode_EX))) decodePipelineRegister(
        .d(decodeExport_ID_END),
        .q(decode_EX),
        .enable(!controlFlow.stall),
        .clock,
        .clear(clear || controlFlow.flush)
    );

    validTypeValidFetch: assert property (@(posedge clock) disable iff(clear)
        decode_EX.decode.instructionType != INVALID_INSTRUCTION |-> decode_EX.fetch.valid
    );

    // Either we have a valid instruction or there was a fault on the instruction stream
    validFetchValidType: assert property (@(posedge clock) disable iff(clear)
        decode_EX.fetch.valid |-> (decode_EX.decode.instructionType != INVALID_INSTRUCTION)
    );

    writeBackSelectValid: assert property (@(posedge clock) disable iff(clear)
        decode_EX.decode.writeBackSelect == NO_WRITE_BACK |-> !decode_EX.decode.rd.isValid
    );

    writeBackValidSelect: assert property (@(posedge clock) disable iff(clear)
        decode_EX.decode.rd.isValid |-> decode_EX.decode.writeBackSelect != NO_WRITE_BACK
    );

endmodule

