//# PRINT AFTER include/core/branch_predictor.vh
`default_nettype none

`include "riscv_types.vh"
`include "branch_predictor.vh"
`include "config.vh"
`include "decode.vh"

typedef struct packed {
    Word_t tagPC;
    Word_t nextPC;
    CounterState_t counter;
} BTBEntry_t;

module BranchPredictor(
    input Word_t pc_IF,
    output Prediction_t prediction_IF,

    input Prediction_t prediction_BR,
    input Word_t actualPC_BR,
    input InstructionDecodeState_t decode_BR,

    input logic clock, clear
);

    localparam ADDRESS_WIDTH = $clog2(`BTB_ENTRIES);
    logic [ADDRESS_WIDTH-1:0] readAddress, writeAddress;

    // Since the lowest two bits are 0 on a 4-byte aligned PC value, we skip them
    assign readAddress = pc_IF[ADDRESS_WIDTH+1:2];
    assign writeAddress = prediction_BR.tagPC[ADDRESS_WIDTH+1:2];

    pcWordAligned_IF: assert property (@(posedge clock) disable iff(clear)
        pc_IF[1:0] == 2'b0
    );

    pcWordAligned_BR: assert property (@(posedge clock) disable iff(clear)
        decode_BR.instructionType != INVALID_INSTRUCTION && decode_BR.instructionType != ILLEGAL_INSTRUCTION_FAULT |->
        prediction_BR.nextPC[1:0] == 2'b0
    );

    BTBEntry_t btbPrediction_IF, correctedPrediction_BR;
    logic writeEnable_BR;

    xilinx_bram_single #(.RAM_WIDTH($bits(btbPrediction_IF)), .RAM_DEPTH(`BTB_ENTRIES)) btb(
        .address_read(readAddress),
        .dataOut(btbPrediction_IF),

        .address_write(writeAddress),
        .dataIn(correctedPrediction_BR),
        .writeEnable(writeEnable_BR),

        .clock
    );

    Word_t pc_saved;
    Register #(.WIDTH($bits(pc_saved))) pcSavedRegister(
        .d(pc_IF),
        .q(pc_saved),
        .clock,
        .clear,
        .enable(1'b1)
    );

    Word_t returnAddress_push_BR, returnAddress_pop_IF;
    assign returnAddress_push_BR = prediction_BR.tagPC + 32'd4;

    logic push_valid_BR, pop_valid_BR;

    Stack #(
        .SIZE(`BTB_RA_STACK_SIZE),
        .WIDTH($bits(Word_t))
    ) returnAddressStack(
        .push_data(returnAddress_push_BR),
        .push_valid(push_valid_BR),
        .pop_valid(pop_valid_BR),
        .peak_data(returnAddress_pop_IF),
        .clock,
        .clear
    );

    // There are two predictors implemented. The first is a standard 2-bit
    // hysteresis counter which determines the prediction by default and for
    // branches. The second is a return stack predictor which operates on JAL
    // instructions. We don't worry about stack overflows since the branch
    // predictor is just an optimization and will still be evaluated in BR to
    // verify the result.
    always_comb begin
        if(btbPrediction_IF.tagPC == pc_saved) begin
            // Hit
            prediction_IF = '{
                tagPC: btbPrediction_IF.tagPC,
                nextPC: btbPrediction_IF.nextPC,
                counter: btbPrediction_IF.counter,
                isHit: 1'b1
            };
            if(btbPrediction_IF.counter == FUNCTION_RETURN) begin
                prediction_IF.nextPC = returnAddress_pop_IF;
            end
        end else begin
            // Miss, assume PC + 4
            prediction_IF = '{
                tagPC: pc_saved,
                nextPC: pc_saved + 32'd4,
                counter: WEAKLY_NOT_TAKEN,
                isHit: 1'b0
            };
        end
    end


    // It would be nice if all jumps that are taken don't go to the next
    // instruction, but unfortunately GCC generates switch statements that have
    // this propery.

    // predictionTakenConsistent: assert property (@(posedge clock) disable iff(clear)
    //     (btbPrediction_IF.tagPC == pc_saved) &&
    //     (btbPrediction_IF.counter == WEAKLY_TAKEN || btbPrediction_IF.counter == STRONGLY_TAKEN) |->
    //     btbPrediction_IF.nextPC != (pc_saved + 32'd4)
    // );

    // We need the extra zero check because the BRAM's are initialized to zero at boot
    predictionNotTakenConsistent: assert property (@(posedge clock) disable iff(clear)
        (btbPrediction_IF.tagPC == pc_saved) && (pc_saved != 32'b0) &&
        (btbPrediction_IF.counter == WEAKLY_NOT_TAKEN || btbPrediction_IF.counter == STRONGLY_NOT_TAKEN) |->
        btbPrediction_IF.nextPC == (pc_saved + 32'd4)
    );

    function automatic CounterState_t boostConfidence(CounterState_t original);
        unique case(original)
            STRONGLY_NOT_TAKEN: return STRONGLY_NOT_TAKEN;
            WEAKLY_NOT_TAKEN: return STRONGLY_NOT_TAKEN;
            WEAKLY_TAKEN: return STRONGLY_TAKEN;
            STRONGLY_TAKEN: return STRONGLY_TAKEN;
        endcase
    endfunction

    // writePredictionTakenConsistent: assert property (@(posedge clock) disable iff(clear)
    //     writeEnable_BR &&
    //     (correctedPrediction_BR.counter == WEAKLY_TAKEN || correctedPrediction_BR.counter == STRONGLY_TAKEN) |->
    //     correctedPrediction_BR.nextPC != (correctedPrediction_BR.tagPC + 32'd4)
    // ) else $error("%d correctedPrediction: %x %x", $time, correctedPrediction_BR.nextPC, correctedPrediction_BR.tagPC);

    writePredictionNotTakenConsistent: assert property (@(posedge clock) disable iff(clear)
        writeEnable_BR &&
        (correctedPrediction_BR.counter == WEAKLY_NOT_TAKEN || correctedPrediction_BR.counter == STRONGLY_NOT_TAKEN) |->
        correctedPrediction_BR.nextPC == (correctedPrediction_BR.tagPC + 32'd4)
    );

    function automatic logic isJumpLink(Register_t register);
        return register.isValid && (register.number == X1 || register.number == X5);
    endfunction

    function automatic logic isSameRegister(Register_t r1, Register_t r2);
        return r1.isValid && r2.isValid && r1.number == r2.number;
    endfunction

    logic rdLink_BR, rs1Link_BR;
    assign rdLink_BR = isJumpLink(decode_BR.rd);
    assign rs1Link_BR = isJumpLink(decode_BR.rs1.register);

    always_comb begin
        correctedPrediction_BR = '{
            tagPC: prediction_BR.tagPC,
            nextPC: prediction_BR.nextPC,
            counter: prediction_BR.counter
        };
        writeEnable_BR = 1'b0;
        push_valid_BR = 1'b0;
        pop_valid_BR = 1'b0;
        case(decode_BR.instructionType)
            JUMP: begin
                // Jumps are always taken
                writeEnable_BR = 1'b1;
                correctedPrediction_BR.nextPC = actualPC_BR;

                // This if/else explosion is based on the Table 2.1 on pg. 17 in
                // the RISC-V User-Level ISA v2.2
                unique if (!rdLink_BR && rs1Link_BR) begin
                    // Simple function return
                    correctedPrediction_BR.counter = FUNCTION_RETURN;
                    pop_valid_BR = 1'b1;
                end else if(rdLink_BR && !rs1Link_BR) begin
                    // Simple function call
                    push_valid_BR = 1'b1;
                    correctedPrediction_BR.counter = STRONGLY_TAKEN;
                end else if(rdLink_BR && rs1Link_BR) begin
                    push_valid_BR = 1'b1;
                    if(isSameRegister(decode_BR.rd, decode_BR.rs1)) begin
                        // Macro-op fusion
                        correctedPrediction_BR.counter = STRONGLY_TAKEN;
                    end else begin
                        // Co-routine push/pop
                        pop_valid_BR = 1'b1;
                        correctedPrediction_BR.counter = FUNCTION_RETURN;
                    end
                end else begin
                    // Non-function jump
                    correctedPrediction_BR.counter = STRONGLY_TAKEN;
                end

            end
            BRANCH: begin
                if(prediction_BR.nextPC == actualPC_BR) begin
                    // This means we predicted correctly, boost the counter
                    correctedPrediction_BR.counter = boostConfidence(prediction_BR.counter);
                    writeEnable_BR = 1'b1;
                end else begin
                    // This means we predicted incorrectly, decrement the
                    // counter and potentially correct the instruction if we
                    // flip.
                    writeEnable_BR = 1'b1;
                    unique case(prediction_BR.counter)
                        STRONGLY_NOT_TAKEN: correctedPrediction_BR.counter = WEAKLY_NOT_TAKEN;
                        WEAKLY_NOT_TAKEN: begin
                            // We predicted PC + 4, but it was actually taken
                            correctedPrediction_BR.counter = WEAKLY_TAKEN;
                            correctedPrediction_BR.nextPC = actualPC_BR;
                        end
                        WEAKLY_TAKEN: begin
                            // We predicted a jump target, but it was actually PC + 4
                            correctedPrediction_BR.counter = WEAKLY_NOT_TAKEN;
                            correctedPrediction_BR.nextPC = actualPC_BR;
                        end
                        STRONGLY_TAKEN: correctedPrediction_BR.counter = WEAKLY_TAKEN;
                    endcase
                end
            end
            // Everything else isn't worth updating the predictor for
        endcase
    end


endmodule