//# PRINT AFTER core/register_file.sv
`default_nettype none

`include "register_file.vh"


module SimpleRegisterFile #(
    parameter NUM_REGS=32, WIDTH=32
    )(
    input logic[$clog2(NUM_REGS)-1:0] rs1, rs2, rs3, rd,

    input logic rd_write,
    input logic [WIDTH-1:0] rd_data,
    output logic [WIDTH-1:0] rs1_data, rs2_data, rs3_data,

`ifdef SIMULATION_18447
    input logic dump,
`endif

    input logic clock, clear
);

    logic [NUM_REGS-1:0][WIDTH-1:0] registers;

    always_ff @(posedge clock) begin
        if(clear) begin
            registers <= '{default:0};
        end else begin
            if(rd_write && (rd != 'd0)) begin
                registers[rd] <= rd_data;
            end
        end
    end

    always_comb begin
        rs1_data = registers[rs1];
        rs2_data = registers[rs2];
        rs3_data = registers[rs3];
    end

    `ifdef SIMULATION_18447

    localparam STDOUT = 32'h8000_0002;

    int fd;
    always @(posedge clock) begin
        if(!clear && dump) begin
            $display("\n18-447 Register File Dump at Cycle %0d", $time);
            $display("---------------------------------------------\n");
            print_cpu_state(STDOUT, registers);

            fd = $fopen("simulation.reg");
            print_cpu_state(fd, registers);
            $display();
            $fclose(fd);
        end
    end

        // Structure representing the naming information about a register
    typedef struct {
        string isa_name;        // The ISA name for a register (x0..x31)
        string abi_name;        // The ABI name for a register (sp, t0, etc.)
    } register_name_t;

    // An array of all the naming information for a register
    register_name_t REGISTER_NAMES[0:NUM_REGS-1] = '{
        '{isa_name: "x0",  abi_name: "zero"},
        '{isa_name: "x1",  abi_name: "ra"},
        '{isa_name: "x2",  abi_name: "sp"},
        '{isa_name: "x3",  abi_name: "gp"},
        '{isa_name: "x4",  abi_name: "tp"},
        '{isa_name: "x5",  abi_name: "t0"},
        '{isa_name: "x6",  abi_name: "t1"},
        '{isa_name: "x7",  abi_name: "t2"},
        '{isa_name: "x8",  abi_name: "s0/fp"},
        '{isa_name: "x9",  abi_name: "s1"},
        '{isa_name: "x10", abi_name: "a0"},
        '{isa_name: "x11", abi_name: "a1"},
        '{isa_name: "x12", abi_name: "a2"},
        '{isa_name: "x13", abi_name: "a3"},
        '{isa_name: "x14", abi_name: "a4"},
        '{isa_name: "x15", abi_name: "a5"},
        '{isa_name: "x16", abi_name: "a6"},
        '{isa_name: "x17", abi_name: "a7"},
        '{isa_name: "x18", abi_name: "s2"},
        '{isa_name: "x19", abi_name: "s3"},
        '{isa_name: "x20", abi_name: "s4"},
        '{isa_name: "x21", abi_name: "s5"},
        '{isa_name: "x22", abi_name: "s6"},
        '{isa_name: "x23", abi_name: "s7"},
        '{isa_name: "x24", abi_name: "s8"},
        '{isa_name: "x25", abi_name: "s9"},
        '{isa_name: "x26", abi_name: "s10"},
        '{isa_name: "x27", abi_name: "s11"},
        '{isa_name: "x28", abi_name: "t3"},
        '{isa_name: "x29", abi_name: "t4"},
        '{isa_name: "x30", abi_name: "t5"},
        '{isa_name: "x31", abi_name: "t6"}
    };

        // Prints out the information for a single register to the given file.
    function void print_register(int fd, int i, register_name_t reg_name,
            const ref logic [NUM_REGS-1:0][WIDTH-1:0] registers);

        // Format the ABI alias name for the register
        string abi_name, reg_uint_value, reg_int_value;
        abi_name = {"(", reg_name.abi_name, ")"};

        // Format the signed and unsigned views of the register
        $sformat(reg_uint_value, "(%0d)", registers[i]);
        $sformat(reg_int_value, "(%0d)", signed'(registers[i]));

        // Print out the register's names and values
        $fdisplay(fd, "%-8s %-8s = 0x%08x %-12s %-13s", reg_name.isa_name,
                abi_name, registers[i], reg_uint_value, reg_int_value);
    endfunction: print_register

    // Prints the CPU state to the given file.
    function void print_cpu_state(int fd,
            const ref logic [NUM_REGS-1:0][WIDTH-1:0] registers);

        /* Print out the instructions fetched and the current pc value. Don't
         * print this to the register dump file. */
        if (fd == STDOUT) begin
            $fdisplay(fd, "Current CPU State and Register Values:");
            $fdisplay(fd, "--------------------------------------");
            $fdisplay(fd, "%-20s = %0d", "Cycle Count",
                    $root.top.chip.timer.currentTime);
            // $fdisplay(fd, "%-20s = 0x%08x\n", "Program Counter (PC)",
            //         $root.top.chip.core.memoryExport_WB_START.fetch.fetchState.pc);
        end

        // Display the header for the table of register values
        $fdisplay(fd, "%-8s %-8s   %-10s %-12s %-13s", "ISA Name", "ABI Name",
                "Hex Value", "Uint Value", "Int Value");
        $fdisplay(fd, {(8+1+8+3+10+1+12+1+13){"-"}});

        // Display the register and its values for each register
        foreach (REGISTER_NAMES[i]) begin
            print_register(fd, i, REGISTER_NAMES[i], registers);
        end
    endfunction: print_cpu_state

    `endif

endmodule