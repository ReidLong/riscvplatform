//# PRINT AFTER include/core/branch.vh
`default_nettype none

`include "pipeline.vh"
`include "control_flow.vh"
`include "execute.vh"
`include "branch.vh"
`include "register_file.vh"
`include "decode.vh"

module BranchResolveStage(
    PipelineControl.Stage controlFlow,

    input ExecuteExport_t execute_BR,

    RegisterReport.Reporter rd_BR,
    BranchResolve.Branch predictionResolution,

    output BranchResolveExport_t branch_MR,

    input logic clock, clear
);

    `DECODE_FUNCTIONS
    `STATE_FUNCTIONS

    BranchResolveState_t branchState;

    assign branchState.simpleDirection = (branchState.actualNextPC >= execute_BR.fetch.pc) ? FORWARD : BACKWARD;

    validIfPrediction: assert property (@(posedge clock) disable iff(clear)
        execute_BR.decode.instructionType != INVALID_INSTRUCTION && execute_BR.decode.instructionType != ILLEGAL_INSTRUCTION_FAULT |-> execute_BR.fetch.valid
    );

    always_comb begin
        branchState.predictionResult = UNPREDICTABLE;
        if (execute_BR.decode.instructionType != INVALID_INSTRUCTION &&
            execute_BR.decode.instructionType != ILLEGAL_INSTRUCTION_FAULT
        ) begin
            if(branchState.actualNextPC == execute_BR.fetch.prediction.nextPC) begin
                branchState.predictionResult = CORRECT_PREDICTION;
            end else begin
                branchState.predictionResult = INCORRECT_PREDICTION;
            end

        end
    end

    branchALUZero: assert property (@(posedge clock) disable iff(clear)
        execute_BR.decode.instructionType == BRANCH |-> execute_BR.execute.aluResult[31:1] == 31'd0
    );

    always_comb begin
        branchState.actualNextPC = execute_BR.fetch.pc + 32'd4;
        unique case (execute_BR.decode.instructionType)
            INVALID_INSTRUCTION: branchState.actualNextPC = `VIRTUAL_ADDRESS_POISON;
            ILLEGAL_INSTRUCTION_FAULT: branchState.actualNextPC = `VIRTUAL_ADDRESS_POISON;
            NORMAL: branchState.actualNextPC = execute_BR.fetch.pc + 32'd4;
            JUMP: branchState.actualNextPC = execute_BR.execute.jumpTarget;
            BRANCH: begin
                if(execute_BR.execute.aluResult[0]) begin
                    branchState.actualNextPC = execute_BR.execute.jumpTarget;
                end else begin
                    branchState.actualNextPC = execute_BR.fetch.pc + 32'd4;
                end
            end
            CSR: branchState.actualNextPC = execute_BR.fetch.pc + 32'd4;
            FENCE: branchState.actualNextPC = execute_BR.fetch.pc + 32'd4;
            PRIVILEGE_RAISE: branchState.actualNextPC = execute_BR.fetch.pc + 32'd4;
            PRIVILEGE_LOWER_SRET: begin
                branchState.actualNextPC = execute_BR.fetch.pc + 32'd4; // epc
            end
            PRIVILEGE_LOWER_MRET: begin
                branchState.actualNextPC = execute_BR.fetch.pc + 32'd4; // epc
            end
        endcase
    end

    assign predictionResolution.fetch_BR = execute_BR.fetch;
    assign predictionResolution.branch_BR = branchState;

    // New register values aren't computed during BR
    assign rd_BR.response = execute_BR.execute.rd_EX;

    always_comb begin
        controlFlow.fencing = 1'b0;
        if(execute_BR.fetch.valid) begin
            if(hasPendingException_IW(execute_BR.instructionWait) ||
                decodeGenerateFence(execute_BR.decode)
            ) begin
                controlFlow.fencing = 1'b1;
            end
        end
    end

    assign controlFlow.ready = 1'b1; // BR always ready
    assign controlFlow.idle = !execute_BR.fetch.valid;
    assign controlFlow.decode = execute_BR.decode;

    BranchResolveExport_t branchExport_BR_END;

    assign branchExport_BR_END.fetch = execute_BR.fetch;
    assign branchExport_BR_END.instructionWait = execute_BR.instructionWait;
    assign branchExport_BR_END.decode = execute_BR.decode;
    assign branchExport_BR_END.execute = execute_BR.execute;
    assign branchExport_BR_END.branch = branchState;

    Register #(.WIDTH($bits(branch_MR))) branchPipelineRegister(
        .d(branchExport_BR_END),
        .q(branch_MR),
        .enable(!controlFlow.stall),
        .clock,
        .clear(clear || controlFlow.flush)
    );

endmodule