//# PRINT AFTER core/memory_request.sv
`default_nettype none

`include "control_flow.vh"
`include "pipeline.vh"
`include "register_file.vh"
`include "memory.vh"
`include "decode.vh"
`include "riscv_types.vh"

module MemoryWaitStage(
    PipelineControl.Stage controlFlow,

    input MemoryRequestExport_t memoryRequest_MW,

    input MemoryResponse_t memoryResponse,
    RegisterReport.Reporter rd_MW,

    output MemoryWaitExport_t memoryWait_WB,

    input logic clock, clear
);

    `DECODE_FUNCTIONS
    `STATE_FUNCTIONS

    // It's too late to abort this operation, but it's okay to flush if we are stalling
    noCancelMemory: assert property (@(posedge clock) disable iff(clear)
        controlFlow.flush |-> !memoryRequest_MW.fetch.valid || !controlFlow.ready
    );

    memoryResponseOnlyValid: assert property (@(posedge clock) disable iff(clear)
        memoryResponse.responseType != INVALID_MEMORY_RESPONSE |->
        memoryRequest_MW.fetch.valid && memoryRequest_MW.decode.memoryOperation != NO_MEMORY_OPERATION
    );

    rejectedStoreConditionalNoMemoryResponse: assert property (@(posedge clock) disable iff(clear)
        memoryRequest_MW.fetch.valid &&
        memoryRequest_MW.memoryRequest.storeConditionalResult == STORE_REJECTED |->
        memoryResponse.responseType == INVALID_MEMORY_RESPONSE
    );

    // If there is a page fault, we should force the store conditional result to
    // be false, since the store didn't actually succeed
    MemoryRequestState_t requestState;
    always_comb begin
        requestState = memoryRequest_MW.memoryRequest;
        if(requestState.storeConditionalResult == STORE_SUCCESS &&
            memoryResponse.responseType == PAGE_FAULT
        ) begin
            `ifdef SIMULATION_18447
            $display("Store Conditional Page Fault: %p %p", requestState, memoryResponse);
            `endif
            requestState.storeConditionalResult = STORE_REJECTED;
        end
    end

    storeConditionalOperationHasResponse: assert property (@(posedge clock) disable iff(clear)
        memoryRequest_MW.decode.memoryOperation == STORE_CONDITIONAL |->
        requestState.storeConditionalResult == STORE_REJECTED ||
        requestState.storeConditionalResult == STORE_SUCCESS
    );

    MemoryWaitState_t waitState;

    assign waitState.response = memoryResponse;

    memoryWriteBackRDNotReady: assert property (@(posedge clock) disable iff(clear)
        memoryRequest_MW.decode.writeBackSelect == WRITE_BACK_SELECT_MEMORY |->
        !memoryRequest_MW.execute.rd_EX.resultValid
    );

    memoryWriteBackRDValid: assert property (@(posedge clock) disable iff(clear)
        memoryRequest_MW.decode.writeBackSelect == WRITE_BACK_SELECT_MEMORY |->
        memoryRequest_MW.decode.rd.isValid
    );

    memoryWriteBackRDEXValid: assert property (@(posedge clock) disable iff(clear)
        memoryRequest_MW.decode.writeBackSelect == WRITE_BACK_SELECT_MEMORY |->
        memoryRequest_MW.execute.rd_EX.register.isValid
    );

    memoryWriteBackInstructionValid: assert property (@(posedge clock) disable iff(clear)
        memoryRequest_MW.decode.writeBackSelect == WRITE_BACK_SELECT_MEMORY |->
        memoryRequest_MW.fetch.valid
    );

    memoryWriteBackNotTranslationInvalidation: assert property (@(posedge clock) disable iff(clear)
        memoryRequest_MW.decode.writeBackSelect == WRITE_BACK_SELECT_MEMORY |->
        memoryResponse.responseType != TRANSLATION_INVALIDATE
    );

    memoryWriteBackNotStore: assert property (@(posedge clock) disable iff(clear)
        memoryRequest_MW.decode.writeBackSelect == WRITE_BACK_SELECT_MEMORY |->
        memoryRequest_MW.decode.memoryOperation != STORE_BYTE ||
        memoryRequest_MW.decode.memoryOperation != STORE_HALF_WORD ||
        memoryRequest_MW.decode.memoryOperation != STORE_WORD ||
        memoryRequest_MW.decode.memoryOperation != NO_MEMORY_OPERATION
    );

    Word_t address;
    assign address = memoryRequest_MW.execute.aluResult;

    Word_t readData_aligned;
    always_comb begin
        unique case(address[1:0])
            2'd0: readData_aligned = memoryResponse.readData;
            2'd1: readData_aligned = memoryResponse.readData >> 5'd8;
            2'd2: readData_aligned = memoryResponse.readData >> 5'd16;
            2'd3: readData_aligned = memoryResponse.readData >> 5'd24;
        endcase
    end

    pageFaultFences: assert property (@(posedge clock) disable iff(clear)
        memoryResponse.responseType == PAGE_FAULT |-> controlFlow.fencing
    );

    pageFaultNoRD: assert property (@(posedge clock) disable iff(clear)
        memoryResponse.responseType == PAGE_FAULT |-> !waitState.rd_MEM.resultValid
    );

    pageFaultMemoryOperation: assert property (@(posedge clock) disable iff(clear)
        memoryResponse.responseType == PAGE_FAULT |-> memoryRequest_MW.decode.memoryOperation != NO_MEMORY_OPERATION
    );

    logic memoryHasResponse;

    always_comb begin
        memoryHasResponse = 1'b0;
        unique case(memoryResponse.responseType)
            INVALID_MEMORY_RESPONSE: memoryHasResponse = 1'b0;
            CACHE_HIT: memoryHasResponse = 1'b1;
            CACHE_MISS: memoryHasResponse = 1'b1;
            VRAM_IO: memoryHasResponse = 1'b1;
            REMOTE_IO: memoryHasResponse = 1'b1;
            PAGE_FAULT: memoryHasResponse = 1'b0;
            TRANSLATION_INVALIDATE: memoryHasResponse = 1'b0;
        endcase
    end

    always_comb begin
        waitState.rd_MEM = memoryRequest_MW.execute.rd_EX;

        case(memoryRequest_MW.decode.memoryOperation)
            LOAD_BYTE: begin
                waitState.rd_MEM.result = {{24{readData_aligned[7]}}, readData_aligned[7:0]};
                waitState.rd_MEM.resultValid = memoryHasResponse;
            end
            LOAD_HALF_WORD: begin
                waitState.rd_MEM.result = {{16{readData_aligned[15]}}, readData_aligned[15:0]};
                waitState.rd_MEM.resultValid = memoryHasResponse;
            end
            LOAD_WORD: begin
                waitState.rd_MEM.result = memoryResponse.readData;
                waitState.rd_MEM.resultValid = memoryHasResponse;
            end
            LOAD_BYTE_UNSIGNED: begin
                waitState.rd_MEM.result = {24'h0, readData_aligned[7:0]};;
                waitState.rd_MEM.resultValid = memoryHasResponse;
            end
            LOAD_HALF_WORD_UNSIGNED: begin
                waitState.rd_MEM.result = {16'h0, readData_aligned[15:0]};
                waitState.rd_MEM.resultValid = memoryHasResponse;
            end
            LOAD_RESERVED: begin
                waitState.rd_MEM.result = memoryResponse.readData;
                waitState.rd_MEM.resultValid = memoryHasResponse;
            end
            STORE_CONDITIONAL: begin
                if(requestState.storeConditionalResult == STORE_SUCCESS) begin
                    waitState.rd_MEM.result = 32'b0;
                    waitState.rd_MEM.resultValid = memoryHasResponse;
                end else begin
                    waitState.rd_MEM.resultValid = 1'b1;
                    waitState.rd_MEM.result = 32'b1;
                end
            end
        endcase
    end

    assign rd_MW.response = waitState.rd_MEM;

    always_comb begin
        controlFlow.fencing = 1'b0;
        controlFlow.ready = 1'b1;
        if(memoryRequest_MW.fetch.valid) begin
            controlFlow.fencing = decodeGenerateFence(memoryRequest_MW.decode);
            if(hasPendingException_IW(memoryRequest_MW.instructionWait)) begin
                controlFlow.fencing = 1'b1; // Force a fence
                controlFlow.ready = 1'b1;
            end else if(memoryResponse.responseType == PAGE_FAULT) begin
                controlFlow.fencing = 1'b1; // Force a fence
                controlFlow.ready = 1'b1;
            end else if(memoryRequest_MW.decode.memoryOperation == NO_MEMORY_OPERATION) begin
                // If there isn't a memory operation, then we are ready by default
                controlFlow.ready = 1'b1;
            end else if(memoryResponse.responseType == INVALID_MEMORY_RESPONSE &&
                memoryRequest_MW.memoryRequest.storeConditionalResult != STORE_REJECTED
            ) begin
                // We don't have a valid memory response and the memory
                // operation didn't result in a store conditional rejection
                // (meaning we are waiting on something), so we aren't ready to
                // continue yet
                controlFlow.ready = 1'b0;
            end
        end
    end

    assign controlFlow.idle = !memoryRequest_MW.fetch.valid;
    assign controlFlow.decode = memoryRequest_MW.decode;

    MemoryWaitExport_t memoryWait_MW_END;
    assign memoryWait_MW_END.fetch = memoryRequest_MW.fetch;
    assign memoryWait_MW_END.instructionWait = memoryRequest_MW.instructionWait;
    assign memoryWait_MW_END.decode = memoryRequest_MW.decode;
    assign memoryWait_MW_END.execute = memoryRequest_MW.execute;
    assign memoryWait_MW_END.branch = memoryRequest_MW.branch;
    assign memoryWait_MW_END.memoryRequest = requestState;
    assign memoryWait_MW_END.memoryWait = waitState;

    Register #(.WIDTH($bits(memoryWait_WB))) memoryWaitPipelineRegister(
        .d(memoryWait_MW_END),
        .q(memoryWait_WB),
        .enable(!controlFlow.stall),
        .clock,
        .clear(clear || controlFlow.flush)
    );



endmodule