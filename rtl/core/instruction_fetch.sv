//# PRINT AFTER include/core/fetch.vh
`default_nettype none

`include "control_flow.vh"
`include "fetch.vh"
`include "config.vh"
`include "memory.vh"
`include "pipeline.vh"
`include "riscv_types.vh"

module InstructionFetchStage(
    PipelineControl.Stage controlFlow,
    FetchPrediction.Fetch prediction,

    output MemoryRequest_t memoryRequest,
    input logic memoryReady,

    output InstructionFetchExport_t fetch_IW,

    input logic clock, clear, halted
);

    `MEMORY_FUNCTIONS

    Word_t pc, pc_saved, pc_new;
    logic pc_enable, pc_enable_saved;

    assign pc_new = prediction.prediction.nextPC;

    assign pc_enable = (!controlFlow.stall && !controlFlow.flush) || prediction.forcePC;

    // pc_enable is combinational, but the next pc value is sequential, so we
    // need to buffer the pc enable bits and then redirect the next pc value
    // with the sequential result if we would have loaded the pc register in a
    // combinational world, otherwise, just use the prior saved value for pc
    Register #(.WIDTH($bits(pc_enable))) pcEnableRegister(
        .q(pc_enable_saved),
        .d(pc_enable),
        .enable(1'b1),
        .clock,
        .clear
    );

    Register #(.WIDTH($bits(pc)), .CLEAR_VALUE(`TEXT_START)) pcRegister(
        .q(pc_saved),
        .d(pc_new),
        .enable(pc_enable_saved),
        .clock,
        .clear
    );

    always_comb begin
        if(pc_enable_saved) begin
            // If we wanted to load a new pc then use the new pc value
            pc = pc_new;
        end else begin
            pc = pc_saved;
        end
    end

    assign controlFlow.ready = memoryReady;
    assign controlFlow.idle = controlFlow.flush;
    assign controlFlow.fencing = 1'b0;
    assign controlFlow.decode = '{default:0};

    assign prediction.pc = pc;

    // This was swapped out due to a timing constraint, but might be able to be
    // optimized back in at some point.
    //
    //assign memoryRequest = memoryReadRequest(pc, !controlFlow.idle);
    assign memoryRequest = memoryReadRequest(pc, !halted);

    InstructionFetchState_t fetchState_IF_END;

    assign fetchState_IF_END = '{
        valid: memoryRequest.readEnable && controlFlow.ready, // If we issue a read then the fetch state should be valid
        pc: pc,
        prediction: prediction.prediction
    };

    InstructionFetchExport_t fetchExport_IF_END, fetchExport_IW_START;

    assign fetchExport_IF_END.fetch = fetchState_IF_END;

    Register #(.WIDTH($bits(fetch_IW))) fetchPipelineRegister(
        .d(fetchExport_IF_END),
        .q(fetchExport_IW_START),
        .enable(!controlFlow.stall),
        .clock,
        .clear(clear || controlFlow.flush)
    );

    logic didStall;
    Register #(.WIDTH($bits(didStall))) didStallRegister(
        .d(controlFlow.stall),
        .q(didStall),
        .clock,
        .clear,
        .enable(1'b1)
    );

    Prediction_t prediction_last;

    Register #(.WIDTH($bits(prediction_last))) predictionLastRegister(
        .d(fetch_IW.fetch.prediction),
        .q(prediction_last),
        .clock,
        .clear,
        .enable(1'b1)
    );

    always_comb begin
        fetch_IW = fetchExport_IW_START;
        // Mix in the correct prediction
        fetch_IW.fetch.prediction = prediction.prediction;
        if (didStall) begin
            // If we stalled on the last cycle, then we should use the last
            // prediction since this simulates a stall.
            fetch_IW.fetch.prediction = prediction_last;
        end
    end

    matchingTag: assert property (@(posedge clock) disable iff(clear)
        fetch_IW.fetch.valid |-> fetch_IW.fetch.prediction.tagPC == fetch_IW.fetch.pc
    );

endmodule