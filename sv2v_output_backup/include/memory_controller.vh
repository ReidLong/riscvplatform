//# PRINT AFTER remote/remote_memory.sv
`ifndef __MEMORY_CONTROLLER_VH
`define __MEMORY_CONTROLLER_VH

typedef enum {NO_MODE, READ_MODE, WRITE_MODE, INVALIDATE_MODE} MemoryMode_t;

`endif