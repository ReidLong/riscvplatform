//# PRINT AFTER include/riscv/riscv_isa.vh
`ifndef __RISCV_CSR_VH
`define __RISCV_CSR_VH

`include "riscv_types.vh"

`define CSR_BITS 12

typedef enum logic[`CSR_BITS-1:0] {
    NONE = 'h0,
    SSTATUS  = 'h100,  // Subset of the MSTATUS register
    STVEC    = 'h105,  // Holds the address of the interrupt handler
    SSCRATCH = 'h140,  // R/W scratch register (holds kernel SP)
    SEPC     = 'h141,  // Faulting instruction address
    SCAUSE   = 'h142,  // Holds the cause of the exception
    STVAL    = 'h143,  // Faulting address (for page faults)
    SATP     = 'h180,  // Virtual Memory Page Directory
    MSTATUS  = 'h300,  // Enables/Disables interrupts
    MEDELEG  = 'h302,  // Delegates Exceptions to Supervisor mode
    MIDELEG  = 'h303,  // Delegates Interrupts to Supervisor mode
    MTVEC    = 'h305,  // Holds the address of the interrupt handler
    MSCRATCH = 'h340,  // R/W scratch register (holds kernel SP)
    MEPC     = 'h341,  // Faulting instruction address
    MCAUSE   = 'h342,  // Holds the cause of the exception
    MTVAL    = 'h343,  // Faulting address (for page faults)
    CYCLE        = 'hC00,
    INSTRET      = 'hC02,
    HPMCOUNTER3  = 'hC03,
    HPMCOUNTER4  = 'hC04,
    HPMCOUNTER5  = 'hC05,
    HPMCOUNTER6  = 'hC06,
    HPMCOUNTER7  = 'hC07,
    HPMCOUNTER8  = 'hC08,
    HPMCOUNTER9  = 'hC09,
    HPMCOUNTER10 = 'hC0A,
    HPMCOUNTER11 = 'hC0B,
    HPMCOUNTER12 = 'hC0C,
    HPMCOUNTER13 = 'hC0D,
    HPMCOUNTER14 = 'hC0E,
    HPMCOUNTER15 = 'hC0F,
    HPMCOUNTER16 = 'hC10,
    HPMCOUNTER17 = 'hC11,
    HPMCOUNTER18 = 'hC12,
    HPMCOUNTER19 = 'hC13,
    HPMCOUNTER20 = 'hC14,
    HPMCOUNTER21 = 'hC15,
    HPMCOUNTER22 = 'hC16,
    HPMCOUNTER23 = 'hC17,
    HPMCOUNTER24 = 'hC18,
    HPMCOUNTER25 = 'hC19,
    HPMCOUNTER26 = 'hC1A,
    HPMCOUNTER27 = 'hC1B,
    HPMCOUNTER28 = 'hC1C,
    HPMCOUNTER29 = 'hC1D,
    HPMCOUNTER30 = 'hC1E,
    HPMCOUNTER31 = 'hC1F,

    CYCLEH        = 'hC80,
    INSTRETH      = 'hC82,
    HPMCOUNTER3H  = 'hC83,
    HPMCOUNTER4H  = 'hC84,
    HPMCOUNTER5H  = 'hC85,
    HPMCOUNTER6H  = 'hC86,
    HPMCOUNTER7H  = 'hC87,
    HPMCOUNTER8H  = 'hC88,
    HPMCOUNTER9H  = 'hC89,
    HPMCOUNTER10H = 'hC8A,
    HPMCOUNTER11H = 'hC8B,
    HPMCOUNTER12H = 'hC8C,
    HPMCOUNTER13H = 'hC8D,
    HPMCOUNTER14H = 'hC8E,
    HPMCOUNTER15H = 'hC8F,
    HPMCOUNTER16H = 'hC90,
    HPMCOUNTER17H = 'hC91,
    HPMCOUNTER18H = 'hC92,
    HPMCOUNTER19H = 'hC93,
    HPMCOUNTER20H = 'hC94,
    HPMCOUNTER21H = 'hC95,
    HPMCOUNTER22H = 'hC96,
    HPMCOUNTER23H = 'hC97,
    HPMCOUNTER24H = 'hC98,
    HPMCOUNTER25H = 'hC99,
    HPMCOUNTER26H = 'hC9A,
    HPMCOUNTER27H = 'hC9B,
    HPMCOUNTER28H = 'hC9C,
    HPMCOUNTER29H = 'hC9D,
    HPMCOUNTER30H = 'hC9E,
    HPMCOUNTER31H = 'hC9F
} ControlStatusRegisterName_t;

typedef enum logic [`CSR_BITS-1:0] {
    CYCLE_COUNT = CYCLE,
    INSTRUCTIONS_RETIRED = INSTRET,
    INSTRUCTIONS_FETCHED                = HPMCOUNTER3,
    BRANCH_RETIRED              = HPMCOUNTER4,
    DATA_TLB_HIT             = HPMCOUNTER5,
    FORWARD_BRANCH_PREDICTED_CORRECT    = HPMCOUNTER6,
    BACKWARD_BRANCH_PREDICTED_CORRECT   = HPMCOUNTER7,
    FORWARD_BRANCH_PREDICTED_INCORRECT  = HPMCOUNTER8,
    BACKWARD_BRANCH_PREDICTED_INCORRECT = HPMCOUNTER9,
    INSTRUCTION_CACHE_HIT               = HPMCOUNTER10,
    INSTRUCTION_CACHE_MISS              = HPMCOUNTER11,
    DATA_CACHE_HIT                      = HPMCOUNTER12,
    DATA_CACHE_MISS                     = HPMCOUNTER13,
    DATA_VRAM_HIT                       = HPMCOUNTER14,
    STORE_CONDITIONAL_SUCCESS           = HPMCOUNTER15,
    STORE_CONDITIONAL_REJECT            = HPMCOUNTER16,
    KEYBOARD_INTERRUPT_USER             = HPMCOUNTER17,
    BTB_HIT          = HPMCOUNTER18,
    EXCEPTION_USER                     = HPMCOUNTER19,
    EXCEPTION_SUPERVISOR                   = HPMCOUNTER20,
    TIMER_INTERRUPT_USER                = HPMCOUNTER21,
    TIMER_INTERRUPT_SUPERVISOR             = HPMCOUNTER22,
    JUMP_RETIRED                = HPMCOUNTER23,
    DATA_TLB_MISS               = HPMCOUNTER24,
    JUMP_PREDICTED_CORRECT      = HPMCOUNTER25,
    RETURN_PREDICTED_CORRECT     = HPMCOUNTER26,
    JUMP_PREDICTED_INCORRECT    = HPMCOUNTER27,
    RETURN_PREDICTED_INCORRECT   = HPMCOUNTER28,
    INSTRUCTION_TLB_HIT = HPMCOUNTER29,
    INSTRUCTION_TLB_MISS = HPMCOUNTER30,
    DATA_REMOTE_HIT = HPMCOUNTER31,

    CYCLE_COUNTH = CYCLEH,
    INSTRUCTIONS_RETIREDH = INSTRETH,
    INSTRUCTIONS_FETCHEDH                = HPMCOUNTER3H,
    BRANCH_RETIREDH              = HPMCOUNTER4H,
    DATA_TLB_HITH             = HPMCOUNTER5H,
    FORWARD_BRANCH_PREDICTED_CORRECTH    = HPMCOUNTER6H,
    BACKWARD_BRANCH_PREDICTED_CORRECTH   = HPMCOUNTER7H,
    FORWARD_BRANCH_PREDICTED_INCORRECTH  = HPMCOUNTER8H,
    BACKWARD_BRANCH_PREDICTED_INCORRECTH = HPMCOUNTER9H,
    INSTRUCTION_CACHE_HITH               = HPMCOUNTER10H,
    INSTRUCTION_CACHE_MISSH              = HPMCOUNTER11H,
    DATA_CACHE_HITH                      = HPMCOUNTER12H,
    DATA_CACHE_MISSH                     = HPMCOUNTER13H,
    DATA_VRAM_HITH                       = HPMCOUNTER14H,
    STORE_CONDITIONAL_SUCCESSH           = HPMCOUNTER15H,
    STORE_CONDITIONAL_REJECTH            = HPMCOUNTER16H,
    KEYBOARD_INTERRUPT_SUPERVISOR             = HPMCOUNTER17H,
    BTB_HITH          = HPMCOUNTER18H,
    EXCEPTION_USERH                     = HPMCOUNTER19H,
    EXCEPTION_SUPERVISORH                   = HPMCOUNTER20H,
    TIMER_INTERRUPT_USERH                = HPMCOUNTER21H,
    TIMER_INTERRUPT_SUPERVISORH             = HPMCOUNTER22H,
    JUMP_RETIREDH                = HPMCOUNTER23H,
    DATA_TLB_MISSH               = HPMCOUNTER24H,
    JUMP_PREDICTED_CORRECTH      = HPMCOUNTER25H,
    RETURN_PREDICTED_CORRECTH     = HPMCOUNTER26H,
    JUMP_PREDICTED_INCORRECTH    = HPMCOUNTER27H,
    RETURN_PREDICTED_INCORRECTH   = HPMCOUNTER28H,
    INSTRUCTION_TLB_HITH = HPMCOUNTER29H,
    INSTRUCTION_TLB_MISSH = HPMCOUNTER30H,
    DATA_REMOTE_HITH = HPMCOUNTER31H
} PerformanceCounter_t;

typedef enum logic [1:0] {
    USER_MODE = 'd0,
    SUPERVISOR_MODE = 'd1,
    MACHINE_MODE = 'd3
} PrivilegeLevel_t;

// System Verilog is crazy. Structs have opposite layout compared to C

// This mask is actually over permissive. This core doesn't fully implement many
// of the bits in mstatus that the user is allowed to write to.

// 1000_0000_0111_1111_1111_1001_1011_1011
`define MSTATUS_WRITE_MASK 32'h807FF9BB
`define SSTATUS_WRITE_MASK 32'h800DE133

typedef enum logic {
    INTERRUPTS_DISABLED = 1'b0,
    INTERRUPTS_ENABLED = 1'b1
} InterruptStatus_t;

typedef struct packed {
    // MSB
    logic  sd;  // Some Dirty (Indicates if FS/XS are non-zero)
    logic [7:0]  zero4;
    logic  tsr;  // TrapSRet (Makes SRET cause an illegal instruction exception)
    // Timeout Wait (Makes WFI cause an illegal instruction exception)
    logic  timeoutWait;
    // Trap Virtual Memory (Makes SFENCE.VMA and modifying satp cause illegal
    // instruction)
    logic  tvm;
    // Make eXecutable Readable (Makes executable-only pages readable)
    logic  mxr;
    // Supervisor User Memory access (Allows the supervisor mode to access pages
    // marked user accessible)
    logic  sum;
    // Modify PRiViledge (Makes machine mode operations translate as if the
    // privilege mode is MPP for emulation)
    logic  mprv;
    logic [1:0]  xs;            // eXtension Status
    logic [1:0]  fs;        // Floating point Status

    PrivilegeLevel_t  mpp;  // Machine mode Previous Privilege
    logic [1:0] zero3;
    logic  spp;  // Supervisor Previous Privilege

    // Machine mode Prior Interrupts Enabled (Holds MIE prior to interrupt)
    InterruptStatus_t  mpie;
    logic  zero2;
    InterruptStatus_t  spie;  // Supervisor Previous Interrupts Enabled
    InterruptStatus_t  upie;        // User Previous Interrupts Enabled

    InterruptStatus_t  mie;             // Machine Interrupts Enabled
    logic  zero1;
    InterruptStatus_t  sie;  // Supervisor Interrupts Enabled
    InterruptStatus_t  uie;        // User Interrupts Enabled
    // LSB
} Status_t;

typedef struct packed {
    // MSB
    logic virtualMemoryEnabled;
    logic [8:0] asid;
    logic [21:0] rootPageTablePageNumber;
    // LSB
} AddressTranslationProtection_t;

`define CAUSE_INDEX_MASK 32'hf

typedef enum Word_t {
    INSTRUCTION_MISALIGNED = 'h0,
    INSTRUCTION_ILLEGAL = 'h2,
    LOAD_MISALIGNED = 'h4,
    STORE_MISALIGNED = 'h6,
    USER_ECALL = 'h8,
    SUPERVISOR_ECALL = 'h9,
    INSTRUCTION_PAGE_FAULT = 'hC,
    LOAD_PAGE_FAULT = 'hD,
    STORE_PAGE_FAULT = 'hF,
    RESERVED_INVALID_CAUSE = 'h10,
    TIMER_INTERRUPT = 'h80000007,
    EXTERNAL_INTERRUPT = 'h80000009
} ExceptionCause_t;

`define CSR_FUNCTIONS\
    function automatic logic hasSuffientPermissions(\
        ControlStatusRegisterName_t name, \
        PrivilegeLevel_t privilegeLevel\
    );\
        unique case (privilegeLevel)\
            MACHINE_MODE: return 1'b1;\
            SUPERVISOR_MODE: return name[9:8] <= SUPERVISOR_MODE;\
            USER_MODE: return name[9:8] == USER_MODE;\
        endcase\
    endfunction\
\
    function automatic logic isDelegated(Word_t deleg, Word_t cause);\
        return (deleg & (1 << (cause & `CAUSE_INDEX_MASK))) != 32'd0;\
    endfunction\
\
    function automatic logic isInterruptCause(Word_t cause);\
        return cause[31];\
    endfunction


`endif