//# PRINT AFTER include/memory/cache.vh
`default_nettype none

`include "cache.vh"
`include "compiler.vh"
`include "config.vh"

module Cache (
    CacheInterface.Cache instruction, data,

    input  logic           clock, clear
);

    `CACHE_FUNCTIONS;

    `STATIC_ASSERT($bits(CacheTagEntry_t) <= 36); // We use more than one bram if this is true
    `STATIC_ASSERT($bits(CacheIndex_t) + $bits(CacheBlockOffset_t) == 10); // Assumption about address
    `STATIC_ASSERT($bits(CacheBlockOffset_t) == $clog2(`CACHE_BLOCK_SIZE));

    CacheResponse_t [`CACHE_SETS-1:0] dataResponses;
    CacheResponse_t [`CACHE_SETS-1:0] instructionResponses;

    function automatic CacheResponse_t sanitizeResponse(CacheResponse_t rawResponse, PhysicalAddress_t address);
        if(isCacheTagHit(rawResponse, cachePhysicalTag(address))) begin
            return rawResponse;
        end else begin
            return '{default:0};
        end
    endfunction

    function automatic CacheRequest_t sanitizeRequest(
        CacheRequest_t request,
        CacheSetNumber_t setNumber
    );
        if(request.requestType == CACHE_READ) begin
            // Reads are non-destructive, so don't worry about sanitizing
            return request;
        end else begin
            if(request.writeSet == setNumber) begin
                return request;
            end else begin
                return '{default:0};
            end
        end
    endfunction

    generate
        genvar setNumber;
        for (setNumber = 0;
            setNumber < `CACHE_SETS;
            setNumber++)
        begin : cacheSet
            CacheResponse_t rawDataResponse, rawInstructionResponse;
            CacheRequest_t rawDataRequest, rawInstructionRequest;

            CacheSetInterface dataInterface(
                .request(rawDataRequest),
                .response(rawDataResponse)
            );
            CacheSetInterface instructionInterface(
                .request(rawInstructionRequest),
                .response(rawInstructionResponse)
            );

            assign rawDataRequest = sanitizeRequest(data.request, setNumber);
            assign rawInstructionRequest = sanitizeRequest(instruction.request, setNumber);

            CacheSet #(.SET_NUMBER(setNumber)) cacheSet(
                .instruction(instructionInterface.CacheSet),
                .data(dataInterface.CacheSet),
                .clock,
                .clear
            );

            assign dataResponses[setNumber] = sanitizeResponse(
                rawDataResponse,
                data.physicalAddress
            );
            assign instructionResponses[setNumber] = sanitizeResponse(
                rawInstructionResponse,
                instruction.physicalAddress
            );
            assign data.rawResponses[setNumber] = rawDataResponse;
            assign instruction.rawResponses[setNumber] = rawInstructionResponse;

        end
    endgenerate

    ArrayOrReduction #(.WIDTH($bits(CacheResponse_t)), .SIZE(`CACHE_SETS)) dataReduction(
        .inputs(dataResponses),
        .result(data.response)
    );

    ArrayOrReduction #(.WIDTH($bits(CacheResponse_t)), .SIZE(`CACHE_SETS)) instructionReduction(
        .inputs(instructionResponses),
        .result(instruction.response)
    );


endmodule