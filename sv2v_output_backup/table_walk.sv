//# PRINT AFTER include/memory/table_walk.vh
`default_nettype none

`include "table_walk.vh"
`include "riscv_types.vh"
`include "compiler.vh"
`include "memory_arbiter.vh"
`include "cache.vh"
`include "dram_fill.vh"
`include "translation.vh"
`include "memory.vh"

module TableWalk(
    TableWalkInterface.TableWalk instruction,
    TableWalkInterface.TableWalk data,

    MemoryArbiterInterface.Requester arbiter,
    CacheInterface.Requester cache,
    DRAMFillInterface.Requester dram,

    TranslationFill.TableWalk tlb,

    MemoryControl.Memory control,

    input logic clock, clear
);

    `CACHE_FUNCTIONS

    `STATIC_ASSERT($clog2(`PAGE_SIZE) == $bits(PageOffset_t));
    `STATIC_ASSERT($bits(PageOffset_t) + $bits(VirtualPageNumber_t) == $bits(VirtualAddress_t));
    `STATIC_ASSERT($bits(PageOffset_t) + $bits(PhysicalPageNumber_t) == $bits(PhysicalAddress_t));

    enum {
        WALK_IDLE,
        WALK_LOCK,
        WALK_LEVEL1,
        WALK_LEVEL2,
        WALK_WAIT1,
        WALK_WAIT2,
        WALK_RESTART1,
        WALK_RESTART2,
        WALK_FILL,
        WALK_SUCCESS,
        WALK_FAULT
    } state, nextState;

    enum {NONE, INST, DATA} mode, nextMode;

    always_ff @(posedge clock)
        if(clear) begin
            state <= WALK_IDLE;
            mode <= NONE;
        end else begin
            state <= nextState;
            mode <= nextMode;
        end

    lockWithRequest: assert property (
        @(posedge clock) disable iff(clear)
        state == WALK_LOCK && arbiter.lock == TABLE_WALK_LOCK |->
            data.requestTranslation || instruction.requestTranslation
    );

    validMode: assert property (
        @(posedge clock) disable iff(clear)
        state != WALK_IDLE && state != WALK_LOCK |-> mode != NONE
    );

    function automatic PhysicalAddress_t getEntryAddress(
        PhysicalAddress_t pageTableBase,
        VirtualPageNumber_t vpn,
        logic vpnIndex
    );
        return pageTableBase + {vpn[vpnIndex], 2'b0};
    endfunction

    function automatic VirtualPageNumber_t getVirtualPageNumber(VirtualAddress_t virtualAddress);
        return virtualAddress[31:12];
    endfunction

    VirtualAddress_t virtualaddress_selected;

    always_comb begin
        virtualaddress_selected = `VIRTUAL_ADDRESS_POISON;
        unique case(mode)
            NONE: virtualaddress_selected = `VIRTUAL_ADDRESS_POISON;
            INST: virtualaddress_selected = instruction.virtualAddress;
            DATA: virtualaddress_selected = data.virtualAddress;
        endcase
    end

    PageTableEntry_t pageTableEntry_new, pageTableEntry_saved;
    logic savePageTableEntry;

    `STATIC_ASSERT($bits(pageTableEntry_new) == $bits(cache.response.readData));
    assign pageTableEntry_new = cache.response.readData;

    Register #(.WIDTH($bits(PageTableEntry_t))) pageTableEntryRegister(
        .d(pageTableEntry_new),
        .q(pageTableEntry_saved),
        .enable(savePageTableEntry),
        .clock,
        .clear
    );

    reservedEntryBits: assert property (
        @(posedge clock) disable iff(clear)
        pageTableEntry_saved.valid |->
        !(pageTableEntry_saved.writable && !pageTableEntry_saved.readable)
    );

    PhysicalAddress_t entryAddress_level1, entryAddress_level2;
    always_comb begin
        entryAddress_level1 = getEntryAddress(
            .pageTableBase(control.rootPageTable),
            .vpn(getVirtualPageNumber(virtualaddress_selected)),
            .vpnIndex(1'b1)
        );
        entryAddress_level2 = getEntryAddress(
            .pageTableBase({pageTableEntry_saved.ppn, 12'b0}),
            .vpn(getVirtualPageNumber(virtualaddress_selected)),
            .vpnIndex(1'b0)
        );
    end

    TLBIndex_t evictionIndex;
    logic tlbSelectIndex;

    TLBEvictionSelector tlbEvictionSelector(
        .instructionResponse(tlb.instructionResponse),
        .dataResponse(tlb.dataResponse),
        .tlbSelectIndex,
        .evictionIndex,
        .clock,
        .clear
    );

    CacheVirtualTag_t virtualTag_selected;

    assign virtualTag_selected = cacheVirtualTag(virtualaddress_selected);

    assign dram.cacheResponse = cache.response;

    always_comb begin
        nextState = state;
        nextMode = mode;

        arbiter.isIdle = 1'b0;
        arbiter.requestLock = 1'b0;
        arbiter.handoff = MEMORY_UNLOCKED;
        cache.request = '{default:0};
        cache.physicalAddress = `PHYSICAL_ADDRESS_POISON;
        dram.address = `PHYSICAL_ADDRESS_POISON;
        dram.requestFill = 1'b0;
        savePageTableEntry = 1'b0;
        tlb.newTranslation = '{default:0};

        data.result = INVALID_TABLE_WALK;
        instruction.result = INVALID_TABLE_WALK;
        tlbSelectIndex = 1'b0;

        unique case(state)
            WALK_IDLE: begin
                arbiter.isIdle = 1'b1;
                nextMode = NONE;
                if(data.requestTranslation || instruction.requestTranslation) begin
                    nextState = WALK_LOCK; // T1
                end else begin
                    nextState = WALK_IDLE; // T2
                end
            end
            WALK_LOCK: begin
                arbiter.requestLock = 1'b1;
                if(arbiter.lock != TABLE_WALK_LOCK) begin
                    // Wait here until the lock is acquired
                    arbiter.isIdle = 1'b1;
                    nextState = WALK_LOCK; // T3
                end else begin
                    arbiter.isIdle = 1'b0;
                    // We have the lock, so now select the mode
                    if(data.requestTranslation) begin
                        nextMode = DATA;
                        nextState = WALK_LEVEL1; // T4.1
                        cache.request = readRequest(
                            getEntryAddress(
                                .pageTableBase(control.rootPageTable),
                                .vpn(getVirtualPageNumber(data.virtualAddress)),
                                .vpnIndex(1'b1)
                            )
                        );
                    end else if(instruction.requestTranslation) begin
                        nextMode = INST;
                        nextState = WALK_LEVEL1; // T4.2
                        cache.request = readRequest(
                            getEntryAddress(
                                .pageTableBase(control.rootPageTable),
                                .vpn(getVirtualPageNumber(instruction.virtualAddress)),
                                .vpnIndex(1'b1)
                            )
                        );
                    end else begin
                        `ifdef SIMULATION_18447
                        $error("Missing request?");
                        `endif
                        nextState = WALK_IDLE; // T5
                    end
                end
            end
            WALK_LEVEL1: begin
                arbiter.requestLock = 1'b1;
                cache.physicalAddress = entryAddress_level1;
                dram.address = entryAddress_level1; // Only valid if !cacheHit
                if(!isCacheHit(cache.response)) begin
                    dram.requestFill = 1'b1;
                    arbiter.handoff = DRAM_LOCK;
                    arbiter.isIdle = 1'b1;
                    nextState = WALK_WAIT1; // T6
                end else begin
                    savePageTableEntry = 1'b1;
                    // Check permissions and then go to the next level
                    if(!pageTableEntry_new.valid || (!pageTableEntry_new.readable && pageTableEntry_new.writable)) begin
                        // PAGE FAULT
                        nextState = WALK_FAULT; // T7
                    end else if(pageTableEntry_new.readable || pageTableEntry_new.executable) begin
                        // This is a super page
                        `ifdef SIMULATION_18447
                        $error("Found a super page?");
                        `endif
                        // PAGE FAULT (Not Implemented)
                        nextState = WALK_FAULT; // T8
                    end else begin
                        // This is a non-leaf page table entry, translate the next level
                        cache.request = readRequest(
                            getEntryAddress(
                                .pageTableBase({pageTableEntry_new.ppn, 12'b0}),
                                .vpn(getVirtualPageNumber(virtualaddress_selected)),
                                .vpnIndex(1'b0)
                            )
                        );
                        nextState = WALK_LEVEL2; // T9
                    end
                end
            end
            WALK_WAIT1: begin
                arbiter.requestLock = 1'b1;
                dram.address = entryAddress_level1; // Only valid when not complete
                if(dram.requestComplete) begin
                    arbiter.isIdle = 1'b0;
                    nextState = WALK_RESTART1; // T10
                end else begin
                    arbiter.isIdle = 1'b1;
                    dram.requestFill = 1'b1;
                    nextState = WALK_WAIT1; // T11
                end
            end
            WALK_RESTART1: begin
                arbiter.requestLock = 1'b1;
                arbiter.isIdle = 1'b0;
                cache.request = readRequest(entryAddress_level1);
                nextState = WALK_LEVEL1; // T12
            end
            WALK_LEVEL2: begin
                arbiter.requestLock = 1'b1;
                arbiter.isIdle = 1'b1;
                cache.physicalAddress = entryAddress_level2;
                dram.address = entryAddress_level2; // Only valid when !cacheHit
                if(!isCacheHit(cache.response)) begin
                    dram.requestFill = 1'b1;
                    arbiter.handoff = DRAM_LOCK;
                    nextState = WALK_WAIT2; // T13
                end else begin
                    savePageTableEntry = 1'b1;
                    // Check permissions, this is the last level
                    if(!pageTableEntry_new.valid || (!pageTableEntry_new.readable && !pageTableEntry_new.executable)) begin
                        // PAGE FAULT
                        nextState = WALK_FAULT; // T14
                    end else begin
                        tlbSelectIndex = 1'b1;
                        nextState = WALK_FILL;
                    end
                end
            end
            WALK_WAIT2: begin
                arbiter.requestLock = 1'b1;
                dram.address = entryAddress_level2; // Only valid when !complete
                if(dram.requestComplete) begin
                    arbiter.isIdle = 1'b0;
                    nextState = WALK_RESTART2; // T16
                end else begin
                    arbiter.isIdle = 1'b1;
                    dram.requestFill = 1'b1;
                    nextState = WALK_WAIT2; // T17
                end
            end
            WALK_RESTART2: begin
                arbiter.requestLock = 1'b1;
                arbiter.isIdle = 1'b0;
                cache.request = readRequest(entryAddress_level2);
                nextState = WALK_LEVEL2; // T18
            end
            WALK_FILL: begin
                arbiter.requestLock = 1'b1;
                arbiter.isIdle = 1'b1;
                tlb.newTranslation = '{
                    index: evictionIndex,
                    entry: '{
                        virtualTag: virtualTag_selected,
                        physicalTag: pageTableEntry_saved.ppn,
                        isValid: 1'b1,
                        readable: pageTableEntry_saved.readable,
                        writable: pageTableEntry_saved.writable,
                        executable: pageTableEntry_saved.executable,
                        userAccessable: pageTableEntry_saved.userAccessable,
                        isGlobal: pageTableEntry_saved.isGlobal
                    }
                };
                nextState = WALK_SUCCESS; // T15
            end
            // By moving the output to the instruction/data memory modules as a
            // simple state, we are removing a timing issue where this triggers
            // additional behavior and memory requests through core control
            // flow. Since a TLB fill is not on the speculative path, it is okay
            // to take an extra cycle.
            WALK_SUCCESS: begin
                arbiter.isIdle = 1'b1;
                unique case(mode)
                    INST: instruction.result = TRANSLATION_FILLED;
                    DATA: data.result = TRANSLATION_FILLED;
                endcase
                nextState = WALK_IDLE; // T19
            end
            WALK_FAULT: begin
                arbiter.isIdle = 1'b1;
                nextState = WALK_IDLE; // T20
                unique case(mode)
                    INST: instruction.result = TRANSLATION_FAULT;
                    DATA: data.result = TRANSLATION_FAULT;
                endcase
            end
        endcase
    end

endmodule