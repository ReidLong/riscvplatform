# Auto-Generated Test
# Template for generating random assembly tests

    .data
data:
    .space 4096
data_end:

    .text
    .global main
main:
# Seeding Register Values Randomly
	li x1, 0x5745d4d0
	li x2, 0x8ff87dd1
	li x3, 0xc4d8ec07
	li x4, 0x7a610d0f
	li x5, 0x86eab383
	li x6, 0x6206fece
	li x7, 0x94a29a3e
	li x8, 0x6a751675
	li x9, 0x9293714f
	li x10, 0xf24281cc
	li x11, 0xc2ea78a5
	li x12, 0x81dbc43c
	li x13, 0x8e00dd6b
	li x14, 0x5178dfb1
	li x15, 0x641d76a3
	li x16, 0x68afdeac
	li x17, 0x31dc0cde
	li x18, 0x77598229
	li x19, 0xdbb8dea4
	li x20, 0x3d3e323a
	li x21, 0x369740ca
	li x22, 0xd5c232ea
	li x23, 0xe6d8063b
	li x24, 0xdd6e5a50
	li x25, 0x808842f3
	li x26, 0x653c5f97
	li x27, 0x49e53366
	li x28, 0x36902964
	li x29, 0x3ff040a6
	li x30, 0x88efba71
	li x31, 0x365fc86d

# Seeding Simple Instructions
	sra	x17, x3, x5
	or	x28, x11, x20
	sra	x1, x0, x6
	sll	x27, x3, x12

    li a0, 0xa
    ecall
