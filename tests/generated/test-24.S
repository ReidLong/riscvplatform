# Auto-Generated Test
# Template for generating random assembly tests

    .data
data:
    .space 4096
data_end:

    .text
    .global main
main:
# Seeding Register Values Randomly
	li x1, 0x7f864aab
	li x2, 0xa8f8fe48
	li x3, 0xc2dfe0f
	li x4, 0xf971a77
	li x5, 0x57e10fe4
	li x6, 0xb3298836
	li x7, 0x49872438
	li x8, 0xa865d526
	li x9, 0x9c4e65d6
	li x10, 0x135fd049
	li x11, 0xfabd8c22
	li x12, 0xa3a0f9a9
	li x13, 0x7f802160
	li x14, 0x4e617193
	li x15, 0xab0e6adc
	li x16, 0x97f1badc
	li x17, 0xc218212f
	li x18, 0x6883e8be
	li x19, 0xe4f10fbf
	li x20, 0x70aa95b6
	li x21, 0x7f46cac5
	li x22, 0xe3b14a53
	li x23, 0x52999aa2
	li x24, 0xa8fef369
	li x25, 0x90777ec4
	li x26, 0x9d982b2c
	li x27, 0x9917bf3f
	li x28, 0x43010a4f
	li x29, 0xa314af6f
	li x30, 0x170b4447
	li x31, 0xdd411796

# Seeding Simple Instructions
	sll	x3, x7, x17
	and	x29, x8, x12
	srli	x11,	x29,	0xb
	slti	x2,	x11,	-1240
	srl	x5, x13, x16
	addi	x0,	x2,	901
	slt	x20, x13, x13
	sltiu	x28,	x24,	-1796
	srli	x4,	x20,	0x1d
	srli	x14,	x14,	0x12
	add	x4, x15, x14
	srli	x25,	x20,	0x9
	srai	x13,	x10,	0x2
	slti	x30,	x20,	-1419
	ori	x9,	x15,	-580
	ori	x23,	x21,	1167
	srl	x14, x11, x22
	sll	x23, x3, x15
	xori	x1,	x14,	1317
	sub	x30, x27, x21
	slt	x12, x27, x9
	sub	x22, x23, x18
	srli	x19,	x0,	0x9
	addi	x21,	x29,	-406
	slt	x6, x18, x7
	srai	x6,	x24,	0x7
	slli	x30,	x22,	0x11
	sltu	x18, x9, x30
	sra	x11, x10, x3
	ori	x27,	x6,	-351
	srli	x22,	x1,	0x12
	sll	x8, x30, x21
	srl	x25, x1, x19
	add	x7, x23, x29
	sra	x14, x2, x25
	and	x30, x18, x0
	sra	x11, x4, x7
	srli	x5,	x11,	0xb
	and	x29, x10, x30
	srli	x19,	x18,	0xb
	slti	x14,	x31,	-754
	andi	x0,	x24,	1309
	and	x30, x9, x25
	srli	x3,	x2,	0x1e
	slli	x4,	x22,	0xa
	add	x17, x9, x26
	slti	x14,	x21,	1320
	sub	x27, x26, x13
	srl	x30, x22, x20
	xor	x1, x12, x6
	sltiu	x6,	x1,	-1285
	ori	x0,	x19,	-862
	slti	x0,	x7,	83
	addi	x7,	x17,	-842
	addi	x24,	x28,	-2019
	slli	x21,	x9,	0x19
	srl	x3, x9, x7
	slti	x19,	x11,	-1448
	srai	x3,	x14,	0x7
	slli	x25,	x22,	0x6
	andi	x28,	x28,	-1819
	add	x16, x25, x31
	srl	x25, x24, x11
	add	x6, x26, x6
	ori	x22,	x9,	502
	srai	x3,	x23,	0x13
	slt	x1, x20, x21
	addi	x14,	x23,	1166
	add	x2, x31, x28
	srl	x1, x20, x22
	add	x8, x26, x10
	srai	x20,	x23,	0x6
	slt	x8, x7, x22
	sra	x12, x29, x14
	xori	x3,	x3,	-845

    li a0, 0xa
    ecall
