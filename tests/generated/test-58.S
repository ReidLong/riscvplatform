# Auto-Generated Test
# Template for generating random assembly tests

    .data
mydata:
    .space 4096
data_end:

    .text
    .global main
main:
# Seeding Register Values Randomly
	li x1, 0xaa99e436
	li x2, 0xfdc16962
	li x3, 0xb85b6a7
	li x4, 0x936aabea
	li x5, 0x52dd5d4f
	li x6, 0xd87c99e
	li x7, 0xa597bf8b
	li x8, 0x63f45c53
	li x9, 0xe7343309
	li x10, 0xb3a3bcdd
	li x11, 0xa873bbab
	li x12, 0x52fdd171
	li x13, 0xb757cf10
	li x14, 0x5b49ea80
	li x15, 0xce7f9a9f
	li x16, 0x1fd6519c
	li x17, 0x825d209b
	li x18, 0x3cf3d496
	li x19, 0x9f4f62e3
	li x20, 0x94ae999d
	li x21, 0xa736a935
	li x22, 0xeaa9c7ea
	li x23, 0x1e9b06ad
	li x24, 0x11e6c744
	li x25, 0x2033a5ac
	li x26, 0x69ec5c1d
	li x27, 0xe2eaa425
	li x28, 0xc943a8ef
	li x29, 0x9add437
	li x30, 0x589b9d31
	li x31, 0x7d806db3

        la x1, mydata

# Generating Store Sequence
	li	x27,	1655
	add	x27,	x1,	x27
	sb	x24,	1038(x27)
	li	x11,	4762
	add	x11,	x1,	x11
	sb	x28,	-1038(x11)
	li	x2,	-1385
	add	x2,	x1,	x2
	sh	x14,	1423(x2)
	li	x2,	5331
	add	x2,	x1,	x2
	sw	x5,	-1423(x2)
	li	x21,	-616
	add	x21,	x1,	x21
	sw	x22,	792(x21)
	li	x2,	4150
	add	x2,	x1,	x2
	sb	x14,	-792(x2)
	li	x3,	-1063
	add	x3,	x1,	x3
	sw	x24,	1555(x3)
	li	x31,	4375
	add	x31,	x1,	x31
	sh	x20,	-1555(x31)
	li	x18,	474
	add	x18,	x1,	x18
	sh	x10,	1830(x18)
	li	x19,	3026
	add	x19,	x1,	x19
	sb	x4,	-1830(x19)
	li	x6,	-990
	add	x6,	x1,	x6
	sb	x14,	1443(x6)
	li	x10,	5473
	add	x10,	x1,	x10
	sb	x30,	-1443(x10)
	li	x15,	372
	add	x15,	x1,	x15
	sw	x23,	1976(x15)
	li	x30,	2128
	add	x30,	x1,	x30
	sw	x23,	-1976(x30)
	li	x31,	-389
	add	x31,	x1,	x31
	sh	x28,	1333(x31)
	li	x25,	4173
	add	x25,	x1,	x25
	sh	x31,	-1333(x25)

# Generating Load/Store Sequence
	li	x15,	1360
	add	x15,	x1,	x15
	sh	x3,	-678(x15)
	li	x28,	-59
	add	x28,	x1,	x28
	sb	x25,	447(x28)
	li	x19,	990
	add	x19,	x1,	x19
	lbu	x16,	459(x19)
	li	x22,	4354
	add	x22,	x1,	x22
	lh	x12,	-1746(x22)

# Generating Checksum of Memory
# Checksum Stored in x1
	la	x2,	mydata
	la	x5,	data_end
	li	x1,	0x0

loop:
	lw	x6,	0(x2)
	add	x1,	x1,	x6
	addi	x2,	x2,	0x4
	bne	x2,	x5	,loop

        li a0, 0xa
        ecall
