# Auto-Generated Test
# Template for generating random assembly tests

    .data
mydata:
    .space 4096
data_end:

    .text
    .global main
main:
# Seeding Register Values Randomly
	li x1, 0x6008bf87
	li x2, 0x1155ab54
	li x3, 0xe0befa5c
	li x4, 0x97c0b012
	li x5, 0x5e0b84e4
	li x6, 0x971b4e5f
	li x7, 0x12761289
	li x8, 0x33871ce6
	li x9, 0x28217684
	li x10, 0x33567bfa
	li x11, 0xf1b090fe
	li x12, 0x620b7692
	li x13, 0xaa9bfa3d
	li x14, 0x808cc389
	li x15, 0xf8c17633
	li x16, 0xc17c77ab
	li x17, 0xb3482e31
	li x18, 0xe2e80b9f
	li x19, 0x98e70545
	li x20, 0xe6359b30
	li x21, 0x2f813788
	li x22, 0x455c5f2a
	li x23, 0x45cde09c
	li x24, 0x17de808b
	li x25, 0xbce25c8c
	li x26, 0xa755534b
	li x27, 0x45a7f323
	li x28, 0xb49fb8c3
	li x29, 0xc8049b00
	li x30, 0x5801a789
	li x31, 0x977a4549

        la x1, mydata

# Generating Store Sequence
	li	x31,	-138
	add	x31,	x1,	x31
	sh	x5,	900(x31)
	li	x25,	1814
	add	x25,	x1,	x25
	sh	x25,	-900(x25)
	li	x6,	-235
	add	x6,	x1,	x6
	sw	x3,	1095(x6)
	li	x21,	4311
	add	x21,	x1,	x21
	sw	x4,	-1095(x21)
	li	x26,	664
	add	x26,	x1,	x26
	sw	x28,	1884(x26)
	li	x24,	4881
	add	x24,	x1,	x24
	sb	x22,	-1884(x24)
	li	x26,	2056
	add	x26,	x1,	x26
	sb	x24,	1619(x26)
	li	x21,	2295
	add	x21,	x1,	x21
	sw	x30,	-1619(x21)
	li	x23,	-436
	add	x23,	x1,	x23
	sw	x26,	1056(x23)
	li	x15,	4844
	add	x15,	x1,	x15
	sw	x14,	-1056(x15)
	li	x25,	1682
	add	x25,	x1,	x25
	sb	x20,	549(x25)
	li	x17,	3555
	add	x17,	x1,	x17
	sb	x17,	-549(x17)
	li	x16,	1549
	add	x16,	x1,	x16
	sb	x17,	2044(x16)
	li	x23,	5442
	add	x23,	x1,	x23
	sh	x6,	-2044(x23)
	li	x13,	1757
	add	x13,	x1,	x13
	sh	x2,	689(x13)
	li	x11,	1332
	add	x11,	x1,	x11
	sb	x3,	-689(x11)

# Generating Load/Store Sequence
	li	x19,	1622
	add	x19,	x1,	x19
	lb	x12,	-190(x19)
	li	x21,	1227
	add	x21,	x1,	x21
	lb	x0,	1036(x21)
	li	x17,	1056
	add	x17,	x1,	x17
	sw	x27,	-176(x17)
	li	x27,	3289
	add	x27,	x1,	x27
	lh	x20,	-1239(x27)
	li	x17,	2044
	add	x17,	x1,	x17
	lhu	x28,	-332(x17)
	li	x19,	2234
	add	x19,	x1,	x19
	lh	x10,	-1236(x19)
	li	x13,	3079
	add	x13,	x1,	x13
	lb	x28,	517(x13)
	li	x8,	2495
	add	x8,	x1,	x8
	lw	x9,	-1979(x8)
	li	x16,	-153
	add	x16,	x1,	x16
	sw	x9,	1013(x16)
	li	x13,	1765
	add	x13,	x1,	x13
	lw	x26,	103(x13)
	li	x18,	-182
	add	x18,	x1,	x18
	lb	x3,	1178(x18)
	li	x29,	3973
	add	x29,	x1,	x29
	lhu	x0,	-837(x29)
	li	x2,	2565
	add	x2,	x1,	x2
	lbu	x14,	352(x2)
	li	x18,	1532
	add	x18,	x1,	x18
	lb	x3,	953(x18)
	li	x23,	5930
	add	x23,	x1,	x23
	lw	x25,	-1946(x23)
	li	x2,	-214
	add	x2,	x1,	x2
	lb	x3,	1918(x2)
	li	x31,	2374
	add	x31,	x1,	x31
	sw	x25,	454(x31)
	li	x30,	3179
	add	x30,	x1,	x30
	lhu	x26,	-1281(x30)
	li	x27,	1753
	add	x27,	x1,	x27
	lbu	x23,	1506(x27)
	li	x27,	5575
	add	x27,	x1,	x27
	lbu	x2,	-1510(x27)
	li	x17,	2145
	add	x17,	x1,	x17
	lh	x9,	371(x17)
	li	x5,	2002
	add	x5,	x1,	x5
	sw	x22,	226(x5)
	li	x29,	516
	add	x29,	x1,	x29
	sw	x23,	-96(x29)
	li	x13,	3871
	add	x13,	x1,	x13
	lhu	x7,	-1097(x13)
	li	x2,	-1032
	add	x2,	x1,	x2
	lhu	x30,	1250(x2)
	li	x25,	7
	add	x25,	x1,	x25
	sh	x15,	281(x25)
	li	x20,	1344
	add	x20,	x1,	x20
	lb	x13,	-32(x20)
	li	x10,	2685
	add	x10,	x1,	x10
	sw	x18,	315(x10)
	li	x16,	1967
	add	x16,	x1,	x16
	sb	x22,	-1513(x16)
	li	x30,	3828
	add	x30,	x1,	x30
	sw	x13,	-396(x30)
	li	x2,	-1156
	add	x2,	x1,	x2
	sh	x0,	1762(x2)

# Generating Checksum of Memory
# Checksum Stored in x1
	la	x2,	mydata
	la	x5,	data_end
	li	x1,	0x0

loop:
	lw	x6,	0(x2)
	add	x1,	x1,	x6
	addi	x2,	x2,	0x4
	bne	x2,	x5	,loop

        li a0, 0xa
        ecall
