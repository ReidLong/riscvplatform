# Auto-Generated Test
# Template for generating random assembly tests

    .data
data:
    .space 4096
data_end:

    .text
    .global main
main:
# Seeding Register Values Randomly
	li x1, 0x6d87de4e
	li x2, 0x9a7d8965
	li x3, 0xdc2b76b
	li x4, 0x86d0dc83
	li x5, 0x2de418f3
	li x6, 0x4614dd3c
	li x7, 0x1000fef2
	li x8, 0x7f974b02
	li x9, 0xff46131c
	li x10, 0x40aec012
	li x11, 0x8b4d9f4a
	li x12, 0xe770ad3a
	li x13, 0x543d01cd
	li x14, 0x3d3b6696
	li x15, 0x9ec2ffb4
	li x16, 0x49f2b61f
	li x17, 0x251ed595
	li x18, 0x245590d3
	li x19, 0x5655ac15
	li x20, 0xad46943a
	li x21, 0xaf460996
	li x22, 0x8d6e34ea
	li x23, 0xf4e302b8
	li x24, 0x5765678d
	li x25, 0xdb45ac0d
	li x26, 0xbc862a0
	li x27, 0xa4a84435
	li x28, 0xf7632c1d
	li x29, 0x31447c10
	li x30, 0x498f0526
	li x31, 0xb2ffc12e

# Seeding Simple Instructions
	srl	x27, x16, x23
	sub	x9, x8, x21
	sltu	x6, x18, x7
	add	x6, x16, x2
	or	x31, x6, x22
	and	x20, x7, x16
	or	x5, x28, x25
	sub	x2, x0, x15
	sll	x13, x2, x12
	slt	x31, x12, x20
	srl	x31, x20, x4
	xor	x22, x26, x2
	sll	x15, x2, x28
	add	x9, x19, x22
	sra	x3, x31, x30
	xor	x16, x12, x8
	sltu	x31, x16, x13
	srl	x10, x22, x1
	sub	x12, x15, x14
	xor	x15, x27, x5
	and	x22, x7, x17
	slt	x22, x26, x14
	slt	x31, x10, x19
	or	x24, x8, x19
	slt	x17, x18, x28
	add	x29, x10, x2
	sll	x22, x23, x28
	or	x24, x17, x19
	sra	x27, x15, x17
	sub	x21, x8, x22
	sra	x5, x3, x26
	xor	x15, x13, x18
	sltu	x12, x30, x17
	sra	x17, x29, x3
	and	x23, x6, x16
	sltu	x19, x11, x17
	sra	x25, x15, x23
	srl	x8, x7, x25
	sra	x18, x23, x24
	slt	x15, x25, x18
	add	x26, x31, x27
	xor	x3, x19, x3
	sltu	x18, x20, x6
	and	x21, x21, x22
	add	x19, x7, x18
	sra	x19, x20, x25
	sll	x12, x20, x23
	xor	x28, x20, x15
	sub	x26, x15, x19
	sltu	x2, x22, x5
	slt	x29, x14, x11
	sub	x0, x20, x30
	srl	x21, x10, x7
	sub	x16, x22, x7
	srl	x16, x11, x20
	sll	x26, x5, x23
	sub	x24, x10, x11
	xor	x0, x11, x10
	sltu	x9, x16, x15
	sll	x27, x22, x2
	add	x15, x7, x24
	slt	x2, x3, x0
	or	x26, x19, x21
	xor	x9, x6, x29

    li a0, 0xa
    ecall
