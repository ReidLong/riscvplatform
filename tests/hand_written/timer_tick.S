
.text
.global main
main:


    la a0, interuptHandler
    jal set_mtvec
    li a0, 0x12345678
    jal set_mscratch

    li s0, 0
    jal enable_interrupts

loop:
    addi s0, s0, 1
    j loop

# This shouldn't be reachable
    li a0, 0xa
    ecall


interuptHandler:
    CSRR t1, mscratch
    CSRR t2, mcause
    CSRR t3, 0x343 # MTVAL
    CSRR t4, mstatus
    CSRR t5, mepc

    # This makes the test not dependent on specific timing values
    li x11, 0x6000 # ~125,000
    SLT s0, x11, s0

    ORI t5, t5, 0x4

    # li x11, 0x3d000 # ~250,000
    # mv t6, t3
    # SLT t3, x11, t3


    li a0, 0xa
    ecall


# From 410kern/asm.S

enable_interrupts:
    li a0, 0x8
    csrrs zero, mstatus, a0
    ret

set_mtvec:
    csrrw zero, mtvec, a0
    ret

set_mscratch:
    csrrw a0, mscratch, a0
    ret

