.text
.global main
main:
    li x1, 0x12345678
    csrrw x2, stvec, x1 # Expects 0
    csrr x3, stvec # Expects 0x1234578

    li x1, 0x55445544
    csrrw x4, sscratch, x1 # Expects 0
    csrr x5, sscratch # Expects 0x55445544

    li x1, 0x77998899
    csrrw x6, sepc, x1 # Expects 0
    csrr x7, sepc # Expects 0x77998899

    li x1, 0x12121212
    csrrw x8, scause, x1 # Expects 0x0
    csrr x9, scause # Expects 0x12121212

    li x1, 0x11223344
    csrrw x11, stval, x1 # Expects 0x0
    csrr x12, stval # Expects 0x11223344

    li x1, 0x55445566
    csrrw x13, medeleg, x1 # Expects 0x0
    csrr x14, medeleg # Expects 0x55445566

    li x1, 0x99000099
    csrrw x15, mideleg, x1 # Expects 0x0
    csrr x16, mideleg # Expects 0x99000099

    li x10, 0xa
    ecall
