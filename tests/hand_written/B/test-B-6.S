# andi test

    .text                           # Declare the code to be in the .text segment
    .global main                    # Make main visible to the linker
main:
    li x1, 2147483647
    li x2, -2147483648
    li x3, 1
    li x4, -1
    andi x4, x1, -1
    andi x5, x2, -1
    andi x6, x1, 1
    andi x7, x2, 1
    // andi x8, x3, 2147483647
    // andi x9, x4, 2147483647
    // andi x10, x3, -2147483648
    // andi x11, x4, -2147483648
    // andi x12, x1, 2147483647
    // andi x13, x2, 2147483647
    // andi x14, x1, -2147483648
    // andi x15, x2, -2147483648
    andi x16, x1, 0
    andi x17, x2, 0
    andi x18, x3, 0
    andi x19, x4, 0
    andi x20, x3, 1
    andi x21, x4, 1
    andi x22, x3, -1
    andi x23, x4, -1

    li a0, 0xa
    ecall
