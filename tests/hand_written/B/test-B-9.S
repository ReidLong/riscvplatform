#and test

    .text                           # Declare the code to be in the .text segment
    .global main                    # Make main visible to the linker
main:
    li x1, 2147483647
    li x2, -2147483648
    li x3, 1
    li x4, -1
    and x5, x1, x2
    and x6, x2, x1
    and x7, x1, x1
    and x8, x1, x2
    and x9, x1, x3
    and x10, x1, x4
    and x11, x2, x1
    and x12, x2, x2
    and x13, x2, x3
    and x14, x2, x4
    and x15, x3, x1
    and x16, x3, x2
    and x17, x3, x3
    and x18, x3, x4
    and x19, x4, x1
    and x20, x4, x2
    and x21, x4, x3
    and x22, x4, x4
    and x23, x0, x1
    and x24, x0, x2
    and x25, x0, x3
    and x26, x0, x4
    and x27, x1, x0
    and x28, x2, x0
    and x29, x3, x0
    and x30, x4, x0


    li a0, 0xa
    ecall