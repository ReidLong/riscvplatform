#sub test

    .text                           # Declare the code to be in the .text segment
    .global main                    # Make main visible to the linker
main:
    li x1, 2147483647
    li x2, -2147483648
    li x3, 1
    li x4, -1
    sub x5, x1, x2
    sub x6, x2, x1
    sub x7, x1, x1
    sub x8, x1, x2
    sub x9, x1, x3
    sub x10, x1, x4
    sub x11, x2, x1
    sub x12, x2, x2
    sub x13, x2, x3
    sub x14, x2, x4
    sub x15, x3, x1
    sub x16, x3, x2
    sub x17, x3, x3
    sub x18, x3, x4
    sub x19, x4, x1
    sub x20, x4, x2
    sub x21, x4, x3
    sub x22, x4, x4
    sub x23, x0, x1
    sub x24, x0, x2
    sub x25, x0, x3
    sub x26, x0, x4
    sub x27, x1, x0
    sub x28, x2, x0
    sub x29, x3, x0
    sub x30, x4, x0


    li a0, 0xa
    ecall