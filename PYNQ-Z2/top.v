module top (
    input wire [1:0] switch,
    output wire [3:0] led,
    output wire [2:0] rgb_led[1:0],
    input wire [3:0] button
);

    assign led = button;

    assign rgb_led[0] = {switch[0], 2'b0};
    assign rgb_led[1] = {switch[1], 2'b0};

endmodule