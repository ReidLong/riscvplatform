#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include <string.h>
#include <video_defines.h>

static const int32_t MAGIC_ROW = 5;
static const int32_t MAGIC_COL = 10;

#define STRLEN(s) (sizeof(s) - 1)
static const char IN_MK[]    = "We are in the microkernel\n";
static const char COLORS[]   = "Look at the pretty colors!\n";
static const char PASSING[]  = "All tests passing\n";
static const char CLEARING[] = "Clearing screen\n\n\n\n\n\n";

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    console_init();
    lprintf("In microkernel");

    lprintf("Trying to print to the console");

    console_putBytes(IN_MK, STRLEN(IN_MK));


    lprintf("Setting color");
    console_setTermColor(FGND_PINK | BGND_LGRAY);

    console_putBytes(COLORS, STRLEN(COLORS));

    lprintf("Moving cursor to 5:10");
    console_setCursor(MAGIC_ROW, MAGIC_COL);

    lprintf("Trying to get the cursor information now");
    int32_t row, col;
    console_getCursor(&row, &col);

    lprintf("Found row, col: (%ld, %ld)", row, col);

    if (row != MAGIC_ROW || col != MAGIC_COL) {
        lprintf("Get Cursor Failed: (%ld, %ld)", row, col);
        environment_exit(-11);
    }

    console_putBytes(PASSING, STRLEN(PASSING));

    lprintf("ALL TESTS PASSED");

    console_putBytes(CLEARING, STRLEN(CLEARING));

    environment_exit(77);

    panic("Exit failed?");
}
