#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include "interrupt.h"
#include "timer.h"
#include <asm.h>
#include <contracts.h>
#include <csr.h>
#include <string.h>
#include <video_defines.h>

static volatile int interrupts = 0;

static void interrupt_dispatch(ureg_t* state);

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    console_init();
    lprintf("In microkernel");

    interrupt_install(&interrupt_dispatch, NULL);

    ASSERT(!machine_interrupts_enabled());

    enable_supervisor_user_access();
    ASSERT(!machine_interrupts_enabled());

    disable_supervisor_user_access();
    ASSERT(!machine_interrupts_enabled());

    lprintf("Interrupts Enabled");
    enable_machine_interrupts();
    ASSERT(machine_interrupts_enabled());

    mstatus_t status = get_mstatus();
    lprintf("status: %#lx", CSR_TO_UINT(status));
    enable_supervisor_user_access();
    status = get_mstatus();
    lprintf("status: %#lx", CSR_TO_UINT(status));
    ASSERT(machine_interrupts_enabled());

    disable_supervisor_user_access();
    ASSERT(machine_interrupts_enabled());

    disable_machine_interrupts();
    lprintf("Interrupts Disabled");

    environment_exit(200);

    panic("Exit failed?");
}

static void interrupt_dispatch(ureg_t* state) {
    interrupts++;
    lprintf("Found an interrupt: %#lx %#lx", state->cause, state->value);
}