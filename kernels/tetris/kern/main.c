/*
    Tetris!

    Controls:
        W/I: rotate counterclockwise
        A/J: move left
        S/K: rotate clockwise
        D/L: move right
        space: hard drop
        enter: swap with hold
*/

#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include "keyboard.h"
#include "timer.h"
#include <asm.h>
#include <csr.h>
#include <drivers.h>
#include <interrupt_cause.h>
#include <string.h>
#include <video_defines.h>
#include "footer.h"
#include <contracts.h>

// #define DEFAULT_COLOR    FGND_LGRAY
#define LAST_SCORE_COLOR FGND_GREEN

typedef struct {
  int row;
  int col;
} tetris_location;

#define NUM_TETROMINOS 7
#define NUM_ORIENTATIONS 4
#define TETRIS 4
tetris_location TETROMINOS[NUM_TETROMINOS][NUM_ORIENTATIONS][TETRIS] = {
  // I
  {{{1, 0}, {1, 1}, {1, 2}, {1, 3}},
   {{0, 2}, {1, 2}, {2, 2}, {3, 2}},
   {{3, 0}, {3, 1}, {3, 2}, {3, 3}},
   {{0, 1}, {1, 1}, {2, 1}, {3, 1}}},
  // J
  {{{0, 0}, {1, 0}, {1, 1}, {1, 2}},
   {{0, 1}, {0, 2}, {1, 1}, {2, 1}},
   {{1, 0}, {1, 1}, {1, 2}, {2, 2}},
   {{0, 1}, {1, 1}, {2, 0}, {2, 1}}},
  // L
  {{{0, 2}, {1, 0}, {1, 1}, {1, 2}},
   {{0, 1}, {1, 1}, {2, 1}, {2, 2}},
   {{1, 0}, {1, 1}, {1, 2}, {2, 0}},
   {{0, 0}, {0, 1}, {1, 1}, {2, 1}}},
  // O
  {{{0, 1}, {0, 2}, {1, 1}, {1, 2}},
   {{0, 1}, {0, 2}, {1, 1}, {1, 2}},
   {{0, 1}, {0, 2}, {1, 1}, {1, 2}},
   {{0, 1}, {0, 2}, {1, 1}, {1, 2}}},
  // S
  {{{0, 1}, {0, 2}, {1, 0}, {1, 1}},
   {{0, 1}, {1, 1}, {1, 2}, {2, 2}},
   {{1, 1}, {1, 2}, {2, 0}, {2, 1}},
   {{0, 0}, {1, 0}, {1, 1}, {2, 1}}},
  // T
  {{{0, 1}, {1, 0}, {1, 1}, {1, 2}},
   {{0, 1}, {1, 1}, {1, 2}, {2, 1}},
   {{1, 0}, {1, 1}, {1, 2}, {2, 1}},
   {{0, 1}, {1, 0}, {1, 1}, {2, 1}}},
  // Z
  {{{0, 0}, {0, 1}, {1, 1}, {1, 2}},
   {{0, 2}, {1, 1}, {1, 2}, {2, 1}},
   {{1, 0}, {1, 1}, {2, 1}, {2, 2}},
   {{0, 1}, {1, 0}, {1, 1}, {2, 0}}},
};

typedef enum {
  TET_I, TET_J, TET_L, TET_O, TET_S, TET_T, TET_Z, TET_INVAL
} tetromino;

// TICKS needed to descend 1 row
const uint32_t SPEED_VALUES[15] = { 200, 159, 124, 95, 71, 52, 38, 27, 19, 13, 9, 6, 4, 2, 1};
static unsigned int CURRENT_LEVEL = 0;
static unsigned int REDRAW_BOARD = 0;
static unsigned int PREV_CLEARED = 0;
static unsigned int COMBO = 0;
static unsigned int SCORE = 0;
static unsigned char BOARD[22][10];
static unsigned int TETROMINO_ROW = 20;
static unsigned int TETROMINO_COL = 4;
static unsigned int TETROMINO_ROT = 0;
enum state { TITLE, PLAY, GAME_OVER } GAME_STATE;

tetromino CURR_TETROMINO = TET_INVAL;
tetromino NEXT_TETROMINO = TET_INVAL;
tetromino HOLD_TETROMINO = TET_INVAL;

int tetromino_color(char c) {
    switch (c) {
        case TET_I: return FGND_CYAN;
        case TET_J: return FGND_BLUE;
        case TET_L: return FGND_BRWN;
        case TET_O: return FGND_YLLW; //0xE0;
        case TET_S: return FGND_GREEN;
        case TET_T: return FGND_MAG;
        case TET_Z: return FGND_RED;
        default: return FGND_BLACK;
    }
}

void draw_tetromino() {
    for (int k = 0; k < 4; k++) {
        int r = TETROMINO_ROW + TETROMINOS[CURR_TETROMINO][TETROMINO_ROT][k].row;
        int c = TETROMINO_COL + TETROMINOS[CURR_TETROMINO][TETROMINO_ROT][k].col;
        int color = tetromino_color(CURR_TETROMINO);
        REQUIRES(r >= 0);
        if (r < 20) {
            console_drawChar(20-r-1, 2*c+0, 'X', color);
            console_drawChar(20-r-1, 2*c+1, 'X', color);
        }
    }

    if (HOLD_TETROMINO != TET_INVAL) {
        for (int r = 1; r <= 5; r++) {
            for (int c = 12; c <= 16; c++) {
                console_drawChar(r, 2*c+0, ' ', BGND_LGRAY);
                console_drawChar(r, 2*c+1, ' ', BGND_LGRAY);    
            }
        }
        for (int k = 0; k < 4; k++) {
            int r = 2 +  TETROMINOS[HOLD_TETROMINO][0][k].row;
            int c = 14 + TETROMINOS[HOLD_TETROMINO][0][k].col;
            int color = tetromino_color(HOLD_TETROMINO);
            REQUIRES(r >= 0);
            console_drawChar(r, 2*c+0, 'X', color);
            console_drawChar(r, 2*c+1, 'X', color);
        }
    }
    
    for (int k = 0; k < 4; k++) {
        int r = 8 +  TETROMINOS[NEXT_TETROMINO][0][k].row;
        int c = 14 + TETROMINOS[NEXT_TETROMINO][0][k].col;
        int color = tetromino_color(NEXT_TETROMINO);
        REQUIRES(r >= 0);
        console_drawChar(r, 2*c+0, 'X', color);
        console_drawChar(r, 2*c+1, 'X', color);
    }
}

void clear_tetromino() {
    for (int k = 0; k < 4; k++) {
        int r = TETROMINO_ROW + TETROMINOS[CURR_TETROMINO][TETROMINO_ROT][k].row;
        int c = TETROMINO_COL + TETROMINOS[CURR_TETROMINO][TETROMINO_ROT][k].col;
        REQUIRES(r >= 0);
        if (r < 20) {
            console_drawChar(20-r-1, 2*c+0, ' ', DEFAULT_COLOR);
            console_drawChar(20-r-1, 2*c+1, ' ', DEFAULT_COLOR);
        }
    }

    if (HOLD_TETROMINO != TET_INVAL) {
        for (int k = 0; k < 4; k++) {
            int r = 2 +  TETROMINOS[HOLD_TETROMINO][0][k].row;
            int c = 14 + TETROMINOS[HOLD_TETROMINO][0][k].col;
            REQUIRES(r >= 0);
            console_drawChar(r, 2*c+0, ' ', DEFAULT_COLOR);
            console_drawChar(r, 2*c+1, ' ', DEFAULT_COLOR);
        }
    }
    
    for (int k = 0; k < 4; k++) {
        int r = 8 +  TETROMINOS[NEXT_TETROMINO][0][k].row;
        int c = 14 + TETROMINOS[NEXT_TETROMINO][0][k].col;
        REQUIRES(r >= 0);
        console_drawChar(r, 2*c+0, ' ', DEFAULT_COLOR);
        console_drawChar(r, 2*c+1, ' ', DEFAULT_COLOR);
    }
}

void draw_board() {
    console_clear();

    for (int r = 0; r < 20; r++) {
        for (int c = 0; c < 10; c++) {
            REQUIRES(20-r-1 >= 0);
            console_drawChar(20-r-1, 2*c+0, 'X', tetromino_color(BOARD[r][c]));
            console_drawChar(20-r-1, 2*c+1, 'X', tetromino_color(BOARD[r][c]));
        }
    }

    draw_tetromino();
}

unsigned int random_state;

unsigned int game_rand() {
    random_state ^= random_state << 13;
    random_state ^= random_state >> 17;
    random_state ^= random_state << 5;
    return random_state;
}

void game_srand(unsigned int seed) {
    random_state = seed;
}

unsigned char BAG[7];
unsigned char BAG_IDX = 7;

void new_tetromino() {
    // New bag!
    if (BAG_IDX == 7) {
        // Reset the bag
        for (int k = 0; k < 7; k++) {
            BAG[k] = k;
        }

        // Shuffle!
        for (int i = 7-1; i > 0; i--) {
            int j = game_rand() % (i+1);

            unsigned char temp = BAG[j];
            BAG[j] = BAG[i];
            BAG[i] = temp;
        }

        BAG_IDX = 0;
    }

    unsigned int piece = BAG[BAG_IDX++];
    switch (piece%7) {
        case 0: NEXT_TETROMINO = TET_I; break;
        case 1: NEXT_TETROMINO = TET_J; break;
        case 2: NEXT_TETROMINO = TET_L; break;
        case 3: NEXT_TETROMINO = TET_O; break;
        case 4: NEXT_TETROMINO = TET_S; break;
        case 5: NEXT_TETROMINO = TET_T; break;
        case 6: NEXT_TETROMINO = TET_Z; break;
    }
}

void hold_tetromino() {
    tetromino temp;
    temp = HOLD_TETROMINO;
    HOLD_TETROMINO = CURR_TETROMINO;
    CURR_TETROMINO = temp;

    if (CURR_TETROMINO == TET_INVAL) {
        CURR_TETROMINO = NEXT_TETROMINO;
        new_tetromino();
    }

    TETROMINO_ROW = 20;
    TETROMINO_COL = 4;
    TETROMINO_ROT = 0;
}

void gravity() {
    for (int r = 0; r < 22; r++) {
        // Check if current row is empty
        int row_empty = 1;
        for (int c = 0; c < 10; c++) {
            if (BOARD[r][c] != TET_INVAL) {
                row_empty = 0;
            }    
        }

        // Shift down all above rows
        if (row_empty) {
            for (int r2 = r; r2 < 21; r2++) {
                for (int c = 0; c < 10; c++) {
                    BOARD[r2][c] = BOARD[r2+1][c];
                }
            }
        }
    }

    if (REDRAW_BOARD) {
        REDRAW_BOARD = 0;
        draw_board();
    }
}

void check_board() {
    unsigned int rows_cleared = 0;
    for (int r = 0; r < 22; r++) {
        int row_full = 1;
        for (int c = 0; c < 10; c++) {
            if (BOARD[r][c] == TET_INVAL) {
                row_full = 0;
            }
        }

        // We've completed a row, clear it
        if (row_full) {
            REDRAW_BOARD = 1;
            rows_cleared++;
            for (int c = 0; c < 10; c++) {
                BOARD[r][c] = TET_INVAL;
            }
        }
    }

    // Scoring rows cleared
    switch (rows_cleared) {
        case 1: SCORE += 100; break;
        case 2: SCORE += 300; break;
        case 3: SCORE += 500; break;
        case 4: SCORE += 800; break;
        default: break;
    }

    if (rows_cleared) {
        if (PREV_CLEARED == rows_cleared) {
            switch (rows_cleared) {
                case 4: SCORE += 1200; // B2B Tetris
                default: break;
            }
        }
        if (COMBO) {
            SCORE += 50 * COMBO;
        }

        COMBO += rows_cleared;
        PREV_CLEARED = rows_cleared;
    } else {
        COMBO = 0;
    }

    gravity();
}

#define NUM_SCORES 5
unsigned int hiscores[NUM_SCORES];
unsigned int score = 0;
int last_score_pos = -1;

void add_score() {
    // save the user's score, and create a variable for the new score index
    int new_hiscore = -1;
    int usr_score = score;

    int n;
    for (n = 0; n < NUM_SCORES; ++n) {
        if (score > hiscores[n]) {
            // save the index of the user's new hiscore
            if (new_hiscore == -1) {
                new_hiscore = n;
            }

            int temp = hiscores[n];
            hiscores[n] = score;
            score = temp;
        }
    }

    // restore global score to be the user's score
    score = usr_score;
    last_score_pos = new_hiscore;

    if (new_hiscore != -1) {
        printf("Enter initials!\n");
        while (1) {
            int key = readchar();
            switch (key) {
                case '\n':
                    return;
                default:
                    printf("%c", key);
            }
        }
    }
}

static uint32_t g_ticks = 0;

int move_down() {    
    // Check for collisions
    for (int k = 0; k < 4; k++) {
        int r = TETROMINO_ROW + TETROMINOS[CURR_TETROMINO][TETROMINO_ROT][k].row;
        int c = TETROMINO_COL + TETROMINOS[CURR_TETROMINO][TETROMINO_ROT][k].col;
        if (r-1 < 0 || BOARD[r-1][c] != TET_INVAL) {
            return 0;
        }
    }

    TETROMINO_ROW--;
    return 1;
}

int move_left() {    
    // Check for collisions
    for (int k = 0; k < 4; k++) {
        int r = TETROMINO_ROW + TETROMINOS[CURR_TETROMINO][TETROMINO_ROT][k].row;
        int c = TETROMINO_COL + TETROMINOS[CURR_TETROMINO][TETROMINO_ROT][k].col;
        if (c-1 < 0 || BOARD[r][c-1] != TET_INVAL) {
            return 0;
        }
    }

    TETROMINO_COL--;
    return 1;
}

int move_right() {    
    // Check for collisions
    for (int k = 0; k < 4; k++) {
        int r = TETROMINO_ROW + TETROMINOS[CURR_TETROMINO][TETROMINO_ROT][k].row;
        int c = TETROMINO_COL + TETROMINOS[CURR_TETROMINO][TETROMINO_ROT][k].col;
        if (c+1 >= 10 || BOARD[r][c+1] != TET_INVAL) {
            return 0;
        }
    }

    TETROMINO_COL++;
    return 1;
}

int rotate(int dir) {
    int rot = (TETROMINO_ROT+4+dir)%4;

    for (int k = 0; k < 4; k++) {
        int r = TETROMINO_ROW + TETROMINOS[CURR_TETROMINO][rot][k].row;
        int c = TETROMINO_COL + TETROMINOS[CURR_TETROMINO][rot][k].col;
        if (r < 0 || c < 0 || r >= 22 || c >= 10 || BOARD[r][c] != TET_INVAL) {
            return 0;
        }
    }

    TETROMINO_ROT = rot;
    return 1;
}

void place_tetromino() {
    for (int k = 0; k < 4; k++) {
        int r = TETROMINO_ROW + TETROMINOS[CURR_TETROMINO][TETROMINO_ROT][k].row;
        int c = TETROMINO_COL + TETROMINOS[CURR_TETROMINO][TETROMINO_ROT][k].col;
        BOARD[r][c] = CURR_TETROMINO;

        if (r >= 20) {
            GAME_STATE = GAME_OVER;
        }
    }

    CURR_TETROMINO = NEXT_TETROMINO;
    new_tetromino();

    TETROMINO_ROW = 20;
    TETROMINO_COL = 4;
    TETROMINO_ROT = 0;

    draw_board();
}

void game_tick(uint32_t ticks) {
    g_ticks = ticks;
    // TODO: Implement
}

void game_run() {
    SCORE = 0;
    game_srand(g_ticks);

    new_tetromino();
    CURR_TETROMINO = NEXT_TETROMINO;
    new_tetromino();

    for (int r = 0; r < 22; r++) {
        for (int c = 0; c < 10; c++) {
            BOARD[r][c] = TET_INVAL;
        }
    }
    
    draw_board();
    
    uint32_t prev_ticks = g_ticks;

    while (1) {
        // Handle movement
        int key = readchar();
        switch (key) {
            case ' ':
                clear_tetromino();
                SCORE += TETROMINO_ROW * 2;
                while (move_down());
                place_tetromino();
                break;
            case '\r': /* fallthrough */
            case '\n':
                clear_tetromino();
                hold_tetromino();
                draw_tetromino();
                break;
            case 'W': /* fallthrough */
            case 'w': /* fallthrough */
            case 'I': /* fallthrough */
            case 'i':
                clear_tetromino();
                rotate(-1);
                draw_tetromino();
                break;
            case 'A': /* fallthrough */
            case 'a': /* fallthrough */
            case 'J': /* fallthrough */
            case 'j':
                clear_tetromino();
                move_left();
                draw_tetromino();
                break;
            case 'S': /* fallthrough */
            case 's': /* fallthrough */
            case 'K': /* fallthrough */
            case 'k':
                clear_tetromino();
                rotate(1);
                draw_tetromino();
                break;
            case 'D': /* fallthrough */
            case 'd': /* fallthrough */
            case 'L': /* fallthrough */
            case 'l':
                clear_tetromino();
                move_right();
                draw_tetromino();
                break;
            default:
                break;
        }

        // Handle time
        if (g_ticks - prev_ticks > SPEED_VALUES[CURRENT_LEVEL]) {
            prev_ticks = g_ticks;

            clear_tetromino();
            if(!move_down()) {
                place_tetromino();
            }

            check_board();
            draw_tetromino();
            footer_draw(DEFAULT_FOOTER);
        }

        // Advance levels based on score
        if (SCORE > (CURRENT_LEVEL+1)*500) {
            CURRENT_LEVEL++;
        }
    }
}

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    drivers_install(game_tick);

    enable_interrupts();

    game_run();

    // panic("Game isn't suppose to stop!");

    environment_exit(911);

    panic("Exit failed?");
}
