#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include "interrupt.h"
#include "timer.h"
#include <asm.h>
#include <contracts.h>
#include <csr.h>
#include <string.h>
#include <video_defines.h>

static void interrupt_dispatch(ureg_t* state);

#define PAGE_SIZE 4096

#define BUFFER_COUNT 2
#define COMPUTE_FILL(i, j) (((i & 0xF) << 4) | (j & 0xF))
#define ROUNDS 256

#define dual_printf(fmt, ...)                                                  \
    do {                                                                       \
        lprintf(fmt, __VA_ARGS__);                                             \
        printf(fmt "\n", __VA_ARGS__);                                         \
    } while (0)

#define dual_print(msg) dual_printf("%s", msg)

static int interrupts = 0;

static char junkBuffer[PAGE_SIZE];

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    environment_init(MACHINE_MODE);
    console_init();
    lprintf("In microkernel");

    interrupt_install(&interrupt_dispatch, NULL);


    // Sanity check and initialize the buffers
    char* buffers[BUFFER_COUNT];
    for (int i = 0; i < BUFFER_COUNT; i++) {
        dual_printf("Setting up buffer: %d", i);
        buffers[i] = smemalign(PAGE_SIZE, PAGE_SIZE);
        for (int j = 0; j < PAGE_SIZE; j++) {
            buffers[i][j] = COMPUTE_FILL(i, j);
        }
        for (int j = 0; j < PAGE_SIZE; j++) {
            ASSERT(buffers[i][j] == COMPUTE_FILL(i, j));
        }
        ASSERT(memcmp(buffers[i], buffers[i], PAGE_SIZE) == 0);
        memcpy(junkBuffer, buffers[i], PAGE_SIZE);
        ASSERT(memcmp(junkBuffer, buffers[i], PAGE_SIZE) == 0);
        for (int j = 0; j < PAGE_SIZE; j++) {
            ASSERT(junkBuffer[j] == COMPUTE_FILL(i, j));
        }
    }

    char* workingCopy = smemalign(PAGE_SIZE, PAGE_SIZE);

    enable_machine_interrupts();


    for (int r = 0; r < ROUNDS; r++) {
        disable_machine_interrupts();
        dual_printf("Starting Round: %d Interrupts: %d", r, interrupts);
        enable_machine_interrupts();
        ASSERT(machine_interrupts_enabled());
        for (int b = 0; b < BUFFER_COUNT; b++) {
            memcpy(workingCopy, buffers[b], PAGE_SIZE);
            ASSERT(memcmp(workingCopy, buffers[b], PAGE_SIZE) == 0);
        }
        disable_machine_interrupts();
        dual_printf("Round Success: %d Interrupts: %d", r, interrupts);
        enable_machine_interrupts();
    }

#ifndef __HARDWARE__
    for (int i = 0; i < 20; i++) {
        printf("Success! Success! Success! Success! Success!\n");
    }
#endif

    disable_machine_interrupts();

    // Update the performance counters
    uint64_t cycles  = csr_getCycles();
    uint64_t retired = csr_getInstructionsRetired();
#ifndef __HARDWARE__
    cycles  = 0xFA7F00D;
    retired = 0xD0BAD;
#endif
    dual_printf(
        "Cycles:  %#16llx Instructions Retired: %#16llx\n", cycles, retired);

    uint64_t iHit  = csr_getICacheHit();
    uint64_t iMiss = csr_getICacheMiss();
    uint64_t dHit  = csr_getDCacheHit();
    uint64_t dMiss = csr_getDCacheMiss();
    uint64_t dVRAM = csr_getDVRAMHit();
#ifndef __HARDWARE__
    iHit  = 100;
    iMiss = 50;
    dHit  = 100;
    dMiss = 75;
    dVRAM = 15;
#endif
    uint64_t dRate = (dHit + dVRAM) * 100 / (dHit + dVRAM + dMiss);
    uint64_t iRate = (iHit * 100) / (iHit + iMiss);
    dual_printf("Data Cache Hit: %8lld%% Instruction Cache Hit: %14lld%%\n",
                dRate,
                iRate);

    environment_exit(200);

    panic("Exit failed?");
}

static void interrupt_dispatch(ureg_t* state) {
    (void)state;
    interrupts++;

    ASSERT(!machine_interrupts_enabled());
    // lprintf("Found an interrupt: %d %lx %lx",
    //         interrupts,
    //         state->cause,
    //         state->value);
}