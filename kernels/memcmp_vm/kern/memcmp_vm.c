#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include "interrupt.h"
#include "timer.h"
#include <asm.h>
#include <contracts.h>
#include <csr.h>
#include <launch.h>
#include <riscv_status.h>
#include <string.h>
#include <video_defines.h>

static void interrupt_dispatch(ureg_t* state);

#define PAGE_SIZE 4096

#define BUFFER_COUNT 16
#define COMPUTE_FILL(i, j) (((i & 0xF) << 4) | (j & 0xF))
#define ROUNDS 32

#define dual_printf(fmt, ...)                                                  \
    do {                                                                       \
        lprintf(fmt, __VA_ARGS__);                                             \
        printf(fmt "\n", __VA_ARGS__);                                         \
    } while (0)

#define dual_print(msg) dual_printf("%s", msg)

static int interrupts = 0;

static char junkBuffer[PAGE_SIZE];

static void supervisor_main(void);

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    environment_init(SUPERVISOR_ENV);

    lprintf("Setting up supervisor mode");

    // 1. Delegate machine interrupts to supervisor mode
    uint32_t mideleg =
        DELEGATION_MASK(TIMER_INTERRUPT) | DELEGATION_MASK(EXTERNAL_INTERRUPT);
    set_mideleg(mideleg);

    interrupt_install(&machine_interrupt_monitor, &interrupt_dispatch);

    console_init();

    set_mepc((uint32_t)(uintptr_t)&supervisor_main);
    set_mpp(SUPERVISOR_MODE);

    // Just keep using the current stack for now
    uint64_t stack;

    launch_mret(&stack);
    panic("Launch didn't?");
}

static void supervisor_main(void) {
    // Setup page table
    // Bottom 16MB direct mapped for kernel
    uint32_t* pageDirectory       = smemalign(PAGE_SIZE, PAGE_SIZE);
    uint32_t* directMapPageTables = smemalign(PAGE_SIZE, PAGE_SIZE * 4);

    // Setup the page table entries (direct mapped)
    for (int pageTableIndex = 0; pageTableIndex < 4096; pageTableIndex++) {
        uint32_t guestFrame                 = pageTableIndex << 10;
        directMapPageTables[pageTableIndex] = guestFrame | 0xf;  // kXWRV
    }

    // Setup the page directory
    for (int pageDirectoryIndex = 0; pageDirectoryIndex < 4;
         pageDirectoryIndex++) {
        // Non-Leaf Page table entry
        uint32_t pageTableNumber =
            (uint32_t)&directMapPageTables[pageDirectoryIndex
                                           * (PAGE_SIZE / sizeof(uint32_t))]
            >> 12;
        pageDirectory[pageDirectoryIndex] = (pageTableNumber << 10) | 0x1;  // V
    }

    dual_print("Enabling Virtual Memory");
    satp_t satp = {
        .ppn                  = ((uint32_t)pageDirectory) >> 12,
        .asid                 = 0,
        .virtualMemoryEnabled = 1,
    };
    set_satp(satp);


    // Sanity check and initialize the buffers
    char* buffers[BUFFER_COUNT];
    for (int i = 0; i < BUFFER_COUNT; i++) {
        dual_printf("Setting up buffer: %d", i);
        buffers[i] = smemalign(PAGE_SIZE, PAGE_SIZE);
        for (int j = 0; j < PAGE_SIZE; j++) {
            buffers[i][j] = COMPUTE_FILL(i, j);
        }
        for (int j = 0; j < PAGE_SIZE; j++) {
            ASSERT(buffers[i][j] == COMPUTE_FILL(i, j));
        }
        ASSERT(memcmp(buffers[i], buffers[i], PAGE_SIZE) == 0);
        memcpy(junkBuffer, buffers[i], PAGE_SIZE);
        ASSERT(memcmp(junkBuffer, buffers[i], PAGE_SIZE) == 0);
        for (int j = 0; j < PAGE_SIZE; j++) {
            ASSERT(junkBuffer[j] == COMPUTE_FILL(i, j));
        }
    }

    char* workingCopy = smemalign(PAGE_SIZE, PAGE_SIZE);

    enable_supervisor_interrupts();


    for (int r = 0; r < ROUNDS; r++) {
        disable_supervisor_interrupts();
        dual_printf("Starting Round: %d Interrupts: %d", r, interrupts);
        enable_supervisor_interrupts();
        ASSERT(supervisor_interrupts_enabled());
        for (int b = 0; b < BUFFER_COUNT; b++) {
            memcpy(workingCopy, buffers[b], PAGE_SIZE);
            ASSERT(memcmp(workingCopy, buffers[b], PAGE_SIZE) == 0);
        }
        disable_supervisor_interrupts();
        dual_printf("Round Success: %d Interrupts: %d", r, interrupts);
        enable_supervisor_interrupts();
    }

    // Update the performance counters
    uint64_t cycles  = csr_getCycles();
    uint64_t retired = csr_getInstructionsRetired();
#ifndef __HARDWARE__
    cycles  = 0xFA7F00D;
    retired = 0xD0BAD;
#endif
    dual_printf(
        "Cycles:  %#16llx Instructions Retired: %#16llx\n", cycles, retired);

    uint64_t iHit  = csr_getICacheHit();
    uint64_t iMiss = csr_getICacheMiss();
    uint64_t dHit  = csr_getDCacheHit();
    uint64_t dMiss = csr_getDCacheMiss();
    uint64_t dVRAM = csr_getDVRAMHit();
#ifndef __HARDWARE__
    iHit  = 100;
    iMiss = 50;
    dHit  = 100;
    dMiss = 75;
    dVRAM = 15;
#endif
    uint64_t dRate = (dHit + dVRAM) * 100 / (dHit + dVRAM + dMiss);
    uint64_t iRate = (iHit * 100) / (iHit + iMiss);
    dual_printf("Data Cache Hit: %8lld%% Instruction Cache Hit: %14lld%%\n",
                dRate,
                iRate);

    environment_exit(200);

    panic("Exit failed?");
}

static void interrupt_dispatch(ureg_t* state) {
    interrupts++;
    ASSERT(state->cause == TIMER_INTERRUPT
           || state->cause == EXTERNAL_INTERRUPT);

    ASSERT(!supervisor_interrupts_enabled());
    // lprintf("Found an interrupt: %d %lx %lx",
    //         interrupts,
    //         state->cause,
    //         state->value);
    // This models a context switch
    sfence_vma_all();

    ASSERT(!supervisor_interrupts_enabled());
}