#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include <string.h>
#include <video_defines.h>

const char* characters =
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";


/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    console_init();
    lprintf("In microkernel");

    lprintf("Trying to print to the console");
    printf("Making a rainbow\n");

    int length = strlen(characters);

    for (int color = 0; color <= 0x7F; color++) {
        console_setTermColor(color);
        console_putByte(characters[color % length]);
    }

    lprintf("ALL TESTS PASSED");

    environment_exit(71);

    panic("Exit failed?");
}
