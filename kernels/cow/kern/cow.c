#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include "interrupt.h"
#include "timer.h"
#include <asm.h>
#include <contracts.h>
#include <csr.h>
#include <launch.h>
#include <riscv_status.h>
#include <string.h>
#include <video_defines.h>

static void interrupt_dispatch(ureg_t* state);

#define PAGE_SIZE 4096
#define COMPUTE_FILL(i, j) ((((i)&0xF) << 4) | ((j)&0xF))
#define MAGIC_OFFSET 0xfcc

#define dual_printf(fmt, ...)                                                  \
    do {                                                                       \
        lprintf(fmt, __VA_ARGS__);                                             \
        printf(fmt "\n", __VA_ARGS__);                                         \
    } while (0)

#define dual_print(msg) dual_printf("%s", msg)

static int interrupts = 0;

#define STACK_SIZE 512

#define COPY_FROM_SEED 0x5
#define USER_BUFFER_SEED 0xb

#define STACK_TOP(stack_low)                                                   \
    ((uint32_t)(((uintptr_t) & (stack_low)[STACK_SIZE - 1])))

static uint64_t machine_stack[STACK_SIZE];

#define PAGE_DIRECTORY_INDEX(virtualAddress) (virtualAddress >> 22)

typedef enum {
    USER_START,
    USER_READ_INVALID,
    USER_FILL_INVALID,
    USER_WRITE_READ_ONLY,
    USER_WRITE_COW,
    USER_DONE,
} TestState_t;

static TestState_t testState;


static void user_code(int   copyFromSeed,
                      char* copyFrom,
                      int   userBufferSeed,
                      char* userBuffer) {
    // When the user code starts, the copyFrom buffer will be marked as invalid
    // the userBuffer will be marked as USER_READ_ONLY
    printf("Copy: %p [seed: %d] User: %p [seed: %d]\n",
           copyFrom,
           copyFromSeed,
           userBuffer,
           userBufferSeed);

    ASSERT(testState == USER_START);

    // 1. The user should verify the contents of the userBuffer are correct
    // according to the index provided.
    for (int j = 0; j < PAGE_SIZE; j++) {
        ASSERT(userBuffer[j] == COMPUTE_FILL(userBufferSeed, j));
    }

    // 2. The user should verify that the copyFrom buffer is correct according
    // to the seed. This will cause a page fault on the first access.
    testState  = USER_READ_INVALID;
    char magic = copyFrom[MAGIC_OFFSET];
    ASSERT(testState == USER_FILL_INVALID);
    ASSERT(magic == COMPUTE_FILL(copyFromSeed, MAGIC_OFFSET));

    for (int j = 0; j < PAGE_SIZE; j++) {
        ASSERT(copyFrom[j] == COMPUTE_FILL(copyFromSeed, j));
    }

    ASSERT(memcmp(userBuffer, copyFrom, PAGE_SIZE) != 0);

    // 3. The user should attempt to copy the from buffer to the user buffer.
    // This will trigger a page fault that will be filled with a clone of the
    // userBuffer in reverse

    char userBuffer0    = userBuffer[0];
    char userBuffer4095 = userBuffer[4095];
    ASSERT(userBuffer0 == COMPUTE_FILL(userBufferSeed, 0));
    ASSERT(userBuffer4095 == COMPUTE_FILL(userBufferSeed, 4095));

    testState                = USER_WRITE_READ_ONLY;
    userBuffer[MAGIC_OFFSET] = 0xbd;
    ASSERT(testState == USER_WRITE_COW);
    ASSERT(userBuffer[MAGIC_OFFSET] == 0xbd);
    ASSERT(userBuffer[0] == userBuffer4095);
    ASSERT(userBuffer[4095] == userBuffer0);

    // Copy the bytes from the copyFrom buffer into the user buffer in reverse
    // order
    for (int j = 0; j < PAGE_SIZE; j++) {
        userBuffer[j] = copyFrom[PAGE_SIZE - j - 1];
    }

    // 5. The user should ecall into the kernel and have it validate that the
    // results are correct
    testState = USER_DONE;
    ecall(0xD0F00D);
}

static void seedBuffer(char* buffer, int seed) {
    for (int j = 0; j < PAGE_SIZE; j++) {
        buffer[j] = COMPUTE_FILL(seed, j);
    }
}

static uint32_t* userBufferEntry;
static uint32_t* copyFromEntry;
static char*     userCopyFrom_kernel;
static char*     userBuffer_kernel;
static uint32_t* blankEntry;
static char*     blank_kernel;

static void supervisor_main(void);

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    environment_init(SUPERVISOR_ENV);

    lprintf("Setting up supervisor mode");

    // 1. Delegate machine interrupts to supervisor mode
    uint32_t mideleg =
        DELEGATION_MASK(TIMER_INTERRUPT) | DELEGATION_MASK(EXTERNAL_INTERRUPT);
    set_mideleg(mideleg);

    uint32_t medeleg = DELEGATION_MASK(USER_ECALL)
                       | DELEGATION_MASK(LOAD_PAGE_FAULT)
                       | DELEGATION_MASK(STORE_AMO_PAGE_FAULT);
    set_medeleg(medeleg);

    interrupt_install(&machine_interrupt_monitor, &interrupt_dispatch);

    console_init();

    // We need to setup the machine stack since we might enter from user-mode
    set_mscratch(STACK_TOP(machine_stack));

    set_mepc((uint32_t)(uintptr_t)&supervisor_main);
    set_mpp(SUPERVISOR_MODE);

    // Just keep using the current stack for now
    uint64_t stack;

    launch_mret(&stack);
    panic("Launch didn't?");
}

static void supervisor_main(void) {
    // Setup page table
    // Bottom 16MB direct mapped for kernel and user to share
    uint32_t* pageDirectory       = smemalign(PAGE_SIZE, PAGE_SIZE);
    uint32_t* directMapPageTables = smemalign(PAGE_SIZE, PAGE_SIZE * 4);

    // Setup the page table entries (direct mapped)
    for (int pageTableIndex = 0; pageTableIndex < 4096; pageTableIndex++) {
        uint32_t guestFrame                 = pageTableIndex << 10;
        directMapPageTables[pageTableIndex] = guestFrame | 0x1f;  // UXWRV
    }

    // Setup the page directory
    for (int pageDirectoryIndex = 0; pageDirectoryIndex < 4;
         pageDirectoryIndex++) {
        // Non-Leaf Page table entry
        uint32_t pageTableNumber =
            (uint32_t)&directMapPageTables[pageDirectoryIndex
                                           * (PAGE_SIZE / sizeof(uint32_t))]
            >> 12;
        pageDirectory[pageDirectoryIndex] = (pageTableNumber << 10) | 0x1;  // V
    }

    enable_supervisor_user_access();

    dual_print("Enabling Virtual Memory");
    satp_t satp = {
        .ppn                  = ((uint32_t)pageDirectory) >> 12,
        .asid                 = 0,
        .virtualMemoryEnabled = 1,
    };
    set_satp(satp);

    dual_print("Initializing Buffers");
    char* userCopyFrom = smemalign(PAGE_SIZE, PAGE_SIZE);
    seedBuffer(userCopyFrom, COPY_FROM_SEED);
    char* userBuffer = smemalign(PAGE_SIZE, PAGE_SIZE);
    seedBuffer(userBuffer, USER_BUFFER_SEED);
    uint64_t* userStack = smemalign(PAGE_SIZE, PAGE_SIZE);
    memset(userStack, 0xCC, PAGE_SIZE);
    uint64_t* kernelStack = smemalign(PAGE_SIZE, PAGE_SIZE);
    memset(kernelStack, 0x77, PAGE_SIZE);
    blank_kernel = smemalign(PAGE_SIZE, PAGE_SIZE);
    memset(blank_kernel, 0xDD, PAGE_SIZE);
    dual_printf("copyFrom: %p userBuffer: %p userStack: %p kernelStack: %p",
                userCopyFrom,
                userBuffer,
                (void*)userStack,
                (void*)kernelStack);
    copyFromEntry       = &directMapPageTables[(uintptr_t)userCopyFrom >> 12];
    userBufferEntry     = &directMapPageTables[(uintptr_t)userBuffer >> 12];
    blankEntry          = &directMapPageTables[(uintptr_t)blank_kernel >> 12];
    userCopyFrom_kernel = userCopyFrom;
    userBuffer_kernel   = userBuffer;

    // Update the page table permissions for initial launch
    *copyFromEntry   = (*copyFromEntry & ~0x3ff);           // Invalid
    *userBufferEntry = (*userBufferEntry & ~0x3ff) | 0x13;  // U..RV
    sfence_vma(userCopyFrom);
    sfence_vma(userBuffer);

    ureg_t userLaunch;
    memset(&userLaunch, 0x88, sizeof(userLaunch));
    userLaunch.sp = STACK_TOP(userStack);
    userLaunch.a0 = COPY_FROM_SEED;
    userLaunch.a1 = (uintptr_t)userCopyFrom;
    userLaunch.a2 = USER_BUFFER_SEED;
    userLaunch.a3 = (uintptr_t)userBuffer;

    ASSERT(!supervisor_interrupts_enabled());
    set_sscratch(STACK_TOP(kernelStack));
    set_sepc((uintptr_t)user_code);
    mstatus_t nextStatus = get_sstatus();
    nextStatus.spie      = 1;          // Enable interrupts on launch
    nextStatus.spp       = USER_MODE;  // Switch to user-mode on launch
    set_sstatus(nextStatus);
    ASSERT(!supervisor_interrupts_enabled());
    testState = USER_START;

    dual_print("Launching");

    launch_ureg_sret(&userLaunch);

    environment_exit(200);

    panic("Exit failed?");
}

static char* cow_kernel;

static void interrupt_dispatch(ureg_t* state) {
    ASSERT(!supervisor_interrupts_enabled());
    ASSERT(get_sscratch() == 0);  // Should be in kernel mode
    interrupts++;

    if (state->cause == TIMER_INTERRUPT) {
        environment_fast(0x7777, state->epc);
        return;
    }
    dual_printf(
        "%d Found an interrupt:%lx %lx", testState, state->cause, state->value);

    switch (testState) {
        case USER_START:
            panic("Unexpected state: %#x %#x %#x",
                  state->epc,
                  state->cause,
                  state->value);
            break;
        case USER_READ_INVALID:
            testState = USER_FILL_INVALID;
            ASSERT(state->cause == LOAD_PAGE_FAULT);
            ASSERT(state->value
                   == (uintptr_t)&userCopyFrom_kernel[MAGIC_OFFSET]);
            *copyFromEntry = (*copyFromEntry & ~0x3ff) | 0x13;  // U..RV
            sfence_vma(userCopyFrom_kernel);
            for (int j = 0; j < PAGE_SIZE; j++) {
                ASSERT(userCopyFrom_kernel[j]
                       == COMPUTE_FILL(COPY_FROM_SEED, j));
            }
            break;
        case USER_FILL_INVALID:
            panic("Unexpected state: %#x %#x %#x",
                  state->epc,
                  state->cause,
                  state->value);
            break;
        case USER_WRITE_READ_ONLY:
            testState = USER_WRITE_COW;
            ASSERT(state->cause == STORE_AMO_PAGE_FAULT);
            ASSERT(state->value == (uintptr_t)&userBuffer_kernel[MAGIC_OFFSET]);
            cow_kernel = smemalign(PAGE_SIZE, PAGE_SIZE);
            ASSERT(cow_kernel != NULL);
            for (int i = 0; i < PAGE_SIZE; i++) {
                cow_kernel[i] = userBuffer_kernel[PAGE_SIZE - i - 1];
                ASSERT(cow_kernel[i]
                       == COMPUTE_FILL(USER_BUFFER_SEED, PAGE_SIZE - i - 1));
            }
            *userBufferEntry = ((uintptr_t)cow_kernel >> 2) | 0x17;  // U.WRV
            sfence_vma(userBuffer_kernel);
            for (int i = 0; i < PAGE_SIZE; i++) {
                ASSERT(userBuffer_kernel[i]
                       == COMPUTE_FILL(USER_BUFFER_SEED, PAGE_SIZE - i - 1));
                ASSERT(userBuffer_kernel[i] == cow_kernel[i]);
            }
            for (int i = 0; i < PAGE_SIZE; i++) {
                ASSERT(blank_kernel[i] == 0xDD);
            }
            *blankEntry = ((uintptr_t)userBuffer_kernel >> 2) | 0x7;  // k.WRV
            sfence_vma(blank_kernel);
            for (int i = 0; i < PAGE_SIZE; i++) {
                ASSERT(blank_kernel[i] == COMPUTE_FILL(USER_BUFFER_SEED, i));
            }
            break;
        case USER_WRITE_COW:
            panic("Unexpected state: %#x %#x %#x",
                  state->epc,
                  state->cause,
                  state->value);
            break;
        case USER_DONE:
            ASSERT(state->cause == USER_ECALL);
            ASSERT(state->a0 == 0xD0F00D);
            for (int i = 0; i < PAGE_SIZE; i++) {
                ASSERT(blank_kernel[i] == COMPUTE_FILL(USER_BUFFER_SEED, i));
            }
            for (int i = 0; i < PAGE_SIZE; i++) {
                ASSERT(userBuffer_kernel[i]
                       == COMPUTE_FILL(COPY_FROM_SEED, PAGE_SIZE - i - 1));
            }
            dual_print("Success!");
            environment_exit(0x1337);
    }

    ASSERT(!supervisor_interrupts_enabled());
}