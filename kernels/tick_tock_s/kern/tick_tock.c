#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include "interrupt.h"
#include <asm.h>
#include <contracts.h>
#include <launch.h>
#include <riscv_status.h>
#include <string.h>
#include <video_defines.h>

#define INTERRUPT_LIMIT 5

static volatile int interrupts = 0;

static void supervisor_interrupt_dispatch(ureg_t* state);

static void supervisor_main(void);

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    // This test is suppose to verify supervisor interrupts work.
    environment_init(SUPERVISOR_ENV);

    lprintf("Setting up supervisor mode");

    // 1. Delegate machine interrupts to supervisor mode
    uint32_t mideleg =
        DELEGATION_MASK(TIMER_INTERRUPT) | DELEGATION_MASK(EXTERNAL_INTERRUPT);
    set_mideleg(mideleg);

    // Install handlers

    interrupt_install(&machine_interrupt_monitor,
                      &supervisor_interrupt_dispatch);

    console_init();

    // Just keep using the current stack
    uint64_t stack;

    set_mepc((uint32_t)(uintptr_t)&supervisor_main);
    set_mpp(SUPERVISOR_MODE);

    launch_mret(&stack);

    panic("Launch didn't?");
}

static void supervisor_main(void) {
    lprintf("Running in supervisor mode");
    ASSERT(get_sscratch() == 0);
    enable_supervisor_interrupts();

    int steps = 0;
    while (interrupts < INTERRUPT_LIMIT) {
        steps++;
    }

    disable_supervisor_interrupts();

    if (steps > 6000) {
        printf("Success: saw %d interrupts\n", interrupts);
    } else {
        printf("Failure: %d steps to see %d interrupts\n", steps, interrupts);
    }

    environment_exit(200);

    panic("Exit failed?");
}

static int ticks = 0;

static void supervisor_interrupt_dispatch(ureg_t* state) {
    lprintf("Supervisor received interrupt: cause: %lx epc: %lx",
            state->cause,
            state->epc);
    interrupts++;
    if (state->cause == TIMER_INTERRUPT) {
        ticks++;
        if (ticks % 50 == 0) {
            printf("Tick: %d [%ld (%lx)]\n", ticks, state->value, state->value);
        }
    } else {
        printf("Tock: %c\n", (char)state->value);
    }
}