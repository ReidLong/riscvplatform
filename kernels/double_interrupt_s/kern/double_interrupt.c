/** The goal of this test is to trigger a nested interrupt while handling an
interrupt from user mode. This will be accomplished by having a timer tick come
in while handling a page fault from user-mode */

#include "launch.h"
#include <asm.h>
#include <assert.h>
#include <common_kern.h>
#include <compiler.h>
#include <console.h>
#include <contracts.h>
#include <csr.h>
#include <environment.h>
#include <interrupt.h>
#include <malloc_internal.h>
#include <page.h>
#include <riscv_status.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STACK_SIZE 1024
#define COOKIE 0xC0051E
#define ECALL_A0 0xEA7B33F
#define SYS_RETURN 0xD0F00D
#define ECALL_DONE 0xD0DEAD
#define USER_BOTTOM 0x66778899
#define KERNEL_BOTTOM 0x11223344

static uint64_t user_stack[STACK_SIZE];
static uint64_t kernel_stack[STACK_SIZE];
static uint64_t machine_stack[STACK_SIZE];

#define STACK_TOP(stack_low)                                                   \
    ((uint32_t)(((uintptr_t) & (stack_low)[STACK_SIZE - 1])))

static void supervisor_interrupt_dispatch(ureg_t* state);
static void user_code(int cookie);

#define dual_printf(fmt, ...)                                                  \
    do {                                                                       \
        lprintf(fmt, __VA_ARGS__);                                             \
        printf(fmt "\n", __VA_ARGS__);                                         \
    } while (0)

#define dual_print(msg) dual_printf("%s", msg);

typedef enum {
    START,
    SYSCALL,
    DID_TICK,
    TICK,
    RETURN,
    DONE,
} State_t;

static State_t testState;

static void supervisor_main(void);

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    environment_init(SUPERVISOR_ENV);

    lprintf("Setting up supervisor mode");

    // 1. Delegate machine interrupts to supervisor mode
    uint32_t mideleg =
        DELEGATION_MASK(TIMER_INTERRUPT) | DELEGATION_MASK(EXTERNAL_INTERRUPT);
    set_mideleg(mideleg);

    uint32_t medeleg = DELEGATION_MASK(USER_ECALL);
    set_medeleg(medeleg);

    interrupt_install(&machine_interrupt_monitor,
                      &supervisor_interrupt_dispatch);

    console_init();

    // We need to setup the machine stack since we might enter from user-mode
    set_mscratch(STACK_TOP(machine_stack));

    set_mepc((uint32_t)(uintptr_t)&supervisor_main);
    set_mpp(SUPERVISOR_MODE);

    // Just keep using the current stack for now
    uint64_t stack;

    launch_mret(&stack);
    panic("Launch didn't?");
}

static void supervisor_main(void) {
    // Setup page table
    // Bottom 16MB direct mapped for both kernel and user access
    uint32_t* pageDirectory       = _smemalign(PAGE_SIZE, PAGE_SIZE);
    uint32_t* directMapPageTables = _smemalign(PAGE_SIZE, PAGE_SIZE * 4);

    for (int pageTableIndex = 0; pageTableIndex < PAGE_SIZE; pageTableIndex++) {
        uint32_t guestFrame = pageTableIndex << 10;
        // User|Executable|Writable|Readable|Valid
        directMapPageTables[pageTableIndex] = guestFrame | 0x1f;  // UXWRV
    }
    for (int pageDirectoryIndex = 0; pageDirectoryIndex < 4;
         pageDirectoryIndex++) {
        // Non-Leaf Page table entry
        uint32_t pageTableNumber =
            (uint32_t)&directMapPageTables[pageDirectoryIndex
                                           * (PAGE_SIZE / sizeof(uint32_t))]
            >> 12;
        pageDirectory[pageDirectoryIndex] = (pageTableNumber << 10) | 0x1;  // V
    }

    dual_print("Setting up page directory");
    // Need to set the SUM bit to 1 so that the kernel can access pages with
    // user permissions
    enable_supervisor_user_access();


    satp_t satp = {
        .ppn                  = ((uint32_t)pageDirectory) >> 12,
        .asid                 = 0,
        .virtualMemoryEnabled = 1,
    };
    STATIC_ASSERT(sizeof(satp) == sizeof(uint32_t));
    set_satp(satp);

    dual_print("Configuring csrs");

    // Go to user mode
    set_sepc((uint32_t)user_code);
    // Force 16-byte alignment
    uint32_t kernel_stack_top = STACK_TOP(kernel_stack);
    uint32_t user_stack_top   = STACK_TOP(user_stack);
    set_sscratch(kernel_stack_top);
    set_spp(USER_MODE);

    lprintf("Kernel Stack Top: %#lx User Stack Top: %#lx",
            kernel_stack_top,
            user_stack_top);

    ureg_t* launchState = (ureg_t*)(user_stack_top - sizeof(ureg_t));
    memset(launchState, 0, sizeof(ureg_t));
    launchState->sp = user_stack_top;
    launchState->a0 = COOKIE;
    launchState->ra = 0xDEADD00D;  // Sanity for assertions

    testState       = START;
    kernel_stack[0] = KERNEL_BOTTOM;
    user_stack[0]   = USER_BOTTOM;

    // Switches argument to sp and then calls MRET
    launch_ureg_sret(launchState);

    panic("Launch didn't?");
}

static void supervisor_interrupt_dispatch(ureg_t* state) {
    ASSERT(!supervisor_interrupts_enabled());
    ASSERT(get_sscratch() == 0);  // Should be in kernel mode
    ASSERT(kernel_stack[0] == KERNEL_BOTTOM);
    ASSERT(user_stack[0] == USER_BOTTOM);
    ureg_t localState;
    memcpy(&localState, state, sizeof(localState));
    ASSERT(memcmp(state, &localState, sizeof(localState)) == 0);
    environment_fast(0x5555, (uintptr_t)state);
    switch (testState) {
        case START:
        case RETURN:
            // Early interrupt, skip it
            environment_fast(state->epc, state->sp);
            ASSERT(state->cause == TIMER_INTERRUPT);
            ASSERT(state->status.spp == USER_MODE);
            break;
        case SYSCALL:
            ASSERT(state->cause == USER_ECALL);
            ASSERT(state->a0 == ECALL_A0);
            ASSERT(state->status.spp == USER_MODE);
            // Enable interrupts and take a timer tick
            testState = TICK;
            dual_printf(
                "Enabling interrupts, expecting a timer tick here epc: %#lx "
                "sp: %#lx",
                state->epc,
                state->sp);
            enable_supervisor_interrupts();
            // Slow down to make sure that a timer tick is ready to fire
            volatile int sum = 0;
            for (int i = 0; i < 10000 && testState == TICK; i++) {
                sum += i;
            }
            // Expect an interrupt here
            ASSERT(testState == DID_TICK);
            disable_supervisor_interrupts();
            ASSERT(memcmp(state, &localState, sizeof(localState)) == 0);
            dual_printf("SYSCALL EPC: %#lx SP: %#lx", state->epc, state->sp);
            state->epc += sizeof(uint32_t);
            ASSERT(state->cause == USER_ECALL);
            ASSERT(state->a0 == ECALL_A0);
            ASSERT(state->status.spp == 0);
            ASSERT(state->epc == localState.epc + 4);
            state->a0 = SYS_RETURN;
            disable_supervisor_interrupts();
            ASSERT(testState == DID_TICK);
            testState = RETURN;
            set_sscratch(STACK_TOP(kernel_stack));
            break;
        case DID_TICK:
            // There was an extra interrupt queued, up so we should process it
            // again
            environment_fast(state->epc, state->sp);
            ASSERT(state->cause == TIMER_INTERRUPT);
            ASSERT(state->status.spp == SUPERVISOR_MODE);
            ASSERT(memcmp(state, &localState, sizeof(localState)) == 0);
            ASSERT(get_sscratch() == 0);  // Should be in kernel mode
            break;
        case TICK:
            ASSERT(state->cause == TIMER_INTERRUPT);
            ASSERT(state->status.spp == SUPERVISOR_MODE);
            ASSERT(memcmp(state, &localState, sizeof(localState)) == 0);
            ASSERT(get_sscratch() == 0);  // Should be in kernel mode
            dual_printf("Tick EPC: %#lx sp: %#lx", state->epc, state->sp);
            testState = DID_TICK;
            break;
        case DONE:
            ASSERT(state->cause == USER_ECALL);
            ASSERT(state->a0 == ECALL_DONE);
            ASSERT(state->status.mpp == 0);
            dual_printf("Done epc: %#lx sp: %#lx", state->epc, state->sp);
            environment_exit(0xD0600D);
            break;
        default:
            panic("Unexpected state: %d", testState);
    }

    ASSERT(!supervisor_interrupts_enabled());
    ASSERT(kernel_stack[0] == KERNEL_BOTTOM);
    ASSERT(user_stack[0] == USER_BOTTOM);
    ASSERT(!state->status.mie);
    set_sstatus(state->status);
    set_sepc(state->epc);
    launch_ureg_sret(state);
    panic("Launch didn't");
}

static void user_code(int cookie) {
    ASSERT(cookie == COOKIE);
    ASSERT(testState == START);
    printf("In user_code: %#x\n", cookie);

    testState = SYSCALL;

    // Send a system call to the "kernel"
    int result = ecall(ECALL_A0);
    ASSERT(result == SYS_RETURN);
    ASSERT(testState == RETURN);

    testState = DONE;

    ecall(ECALL_DONE);
    panic("Shouldn't return from done");
}