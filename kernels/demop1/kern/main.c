#include <common_kern.h>

/* libc includes. */
#include "console.h"
#include <environment.h>
#include <malloc.h>
#include <stdio.h>

#include "keyboard.h"
#include "timer.h"
#include <asm.h>
#include <csr.h>
#include <drivers.h>
#include <interrupt_cause.h>
#include <string.h>
#include <video_defines.h>

#define PERF_SIZE 3

static uint32_t g_ticks = 0;

void tickback(uint32_t ticks) {
    g_ticks = ticks;
}

/** @brief Kernel entrypoint.
 *
 *  This is the entrypoint for the kernel.
 *
 * @return Does not return
 */
int kernel_main(void) {
    console_init();
    drivers_install(tickback);
    // Normally you would assume that memory is initialized at boot; however, if
    // we do our hot-clear hack and only flush the cache this isn't true
    g_ticks = 0;

    enable_interrupts();

    console_setTermColor(FGND_RED);
    printf("Welcome to RTL through OS - Group B9\n");
    printf("Implemented by Reid Long and Teguh Hofstee\n\n");
    printf(
        "\"What I cannot create, I do not understand\" - Richard Feynman\n\n");

    // Just laying out the UI
    int32_t timerRow, perfCounterRow, characterRow, characterCol;
    console_getCursor(&timerRow, &characterCol);
    perfCounterRow = timerRow + 1;
    characterRow   = perfCounterRow + PERF_SIZE;
    (void)characterRow;

    while (1) {
        // for (int i = 0; i < 1; i++) {
        int key = readchar();
        if (key != -1) {
            console_setCursor(characterRow, characterCol);
            console_setTermColor(FGND_MAG);
            console_putByte((char)key);
            console_getCursor(&characterRow, &characterCol);
            if (characterRow < perfCounterRow + PERF_SIZE) {
                characterRow = perfCounterRow + PERF_SIZE;
            }
        }

        console_setTermColor(FGND_BGRN);
        // Update the timer
        char timer[CONSOLE_WIDTH];
        timer_toString(g_ticks, timer, CONSOLE_WIDTH);
        console_setCursor(timerRow, 0);
        printf("Time Since Boot: %s", timer);
        // console_putBytes(timer, strlen(timer));

        // Update the performance counters
        console_setCursor(perfCounterRow, 0);
        uint64_t cycles  = csr_getCycles();
        uint64_t retired = csr_getInstructionsRetired();
        // printf("Cycles: %llx\n", 10llu);
        // uint64_t ipc     = retired * 100 / cycles;
        printf("Cycles:  %#16llx Instructions Retired: %#16llx\n",
               cycles,
               retired);
        // printf("Cycles: %16llx Instructions Retired: %16llx IPC: 0.%02lld\n",
        //        cycles,
        //        retired,
        //        ipc);
        uint64_t iHit  = csr_getICacheHit();
        uint64_t iMiss = csr_getICacheMiss();
        uint64_t dHit  = csr_getDCacheHit();
        uint64_t dMiss = csr_getDCacheMiss();
        uint64_t dVRAM = csr_getDVRAMHit();
        uint64_t dRate = (dHit + dVRAM) * 100 / (dHit + dVRAM + dMiss);
        uint64_t iRate = (iHit * 100) / (iHit + iMiss);
        printf("Data Cache Hit: %8lld%% Instruction Cache Hit: %14lld%%",
               dRate,
               iRate);
    }

    environment_exit(911);

    panic("Exit failed?");
}
