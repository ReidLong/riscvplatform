//# PRINT NEVER
#ifndef __ATTRIBUTES_H
#define __ATTRIBUTES_H

/* Function annotations */
#ifndef NORETURN
#define NORETURN __attribute__((__noreturn__))
#endif

#ifndef NO_RETURN
#define NO_RETURN void NORETURN
#endif

#ifndef PACKED
#define PACKED __attribute__((packed))
#endif

#ifndef MUST_CHECK
#define MUST_CHECK(type) type __attribute__((warn_unused_result))
// TODO: Deprecate this and update all of the code
#define MUST_CHECK_RETURN MUST_CHECK
#endif

#ifndef FALL_THROUGH
#define FALL_THROUGH __attribute__((fallthrough))
#endif

#endif