//# PRINT AFTER riscv_csr.h
#ifndef __RISCV_CAUSE_H
#define __RISCV_CAUSE_H

// Sadly these can't be an enum because enum doesn't like negative values
#define INSTRUCTION_MISALIGNED 0x0
#define INSTRUCTION_ILLEGAL 0x2
#define LOAD_MISALIGNED 0x4
#define STORE_MISALIGNED 0x6
#define USER_ECALL 0x8
#define SUPERVISOR_ECALL 0x9
#define INSTRUCTION_PAGE_FAULT 0xC
#define LOAD_PAGE_FAULT 0xD
#define STORE_AMO_PAGE_FAULT 0xF

#define CAUSE_INDEX_MASK 0xF
#define CAUSE_INTERRUPT_MASK 0x80000000

#define RESERVED_INVALID_CAUSE 0x10

#define TIMER_INTERRUPT 0x80000007
#define EXTERNAL_INTERRUPT 0x80000009

#define DELEGATION_MASK(cause) (1 << ((cause)&CAUSE_INDEX_MASK))

#endif