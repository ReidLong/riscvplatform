//# PRINT NEVER
#ifndef __ALIGNMENT_H
#define __ALIGNMENT_H

#include <stdint.h>
#include <syscall.h>

#define ALIGN_TO(x, multiple) ((uintptr_t)(x) & ~((multiple)-1))
#define IS_ALIGNED_TO(x, multiple) ((uintptr_t)(x) == ALIGN_TO(x, multiple))

#define PAGE_ALIGN(value) (ALIGN_TO(value, PAGE_SIZE))
#define IS_PAGE_ALIGNED(value) (IS_ALIGNED_TO(value, PAGE_SIZE))

#endif