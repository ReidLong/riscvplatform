//# PRINT NEVER
/** @file spec/compiler.h
 *  @brief Useful macros.  Inspired by Linux compiler*.h and kernel.h.
 *  @author bblum S2011
 */

#ifndef _SPEC_COMPILER_H
#define _SPEC_COMPILER_H

/* Force a compilation error if condition is false */
#define STATIC_ZERO_ASSERT(condition)                                          \
    switch (0) {                                                               \
        case 0:                                                                \
        case (condition):;                                                     \
    }
#define STATIC_NULL_ASSERT(condition) STATIC_ZERO_ASSERT(condition)

/* Force a compilation error if condition is false */
#define STATIC_ASSERT(condition) STATIC_ZERO_ASSERT(condition)

#define DEQUALIFY(ptr) ((void *)(uintptr_t)(ptr))

#include <stddef.h>
// This is defined in the <stddef.h>, but is reproduced here for reference
// #define offsetof(TYPE, MEMBER) ((size_t) & ((TYPE *)0)->MEMBER)

#define container_of(ptr, type, member)                                        \
    ((type *)((char *)(ptr)-offsetof(type, member)))

#endif /* !_SPEC_COMPILER_H */
