#include "410kern/inc/exec2obj.h"
#include <stdio.h>
#include <string.h>

#define HEADER "const char "

void print_usage() {
    printf("Usage: exec2obj <file>...\n");
    printf("Creates a .c file containing a const char array for each file\n");
    printf("given as an argument. Each array is initialized to the contents\n");
    printf("of that file. A table of contents is also created. Look at\n");
    printf("exec2obj.h for more information.\n\n");
}
int main(int argc, char** argv) {
    FILE *in, *out;
    int   execsize  = 0;
    int   file_iter = 1;
    char  bucket;
    int   execsizes[MAX_NUM_APP_ENTRIES];
    char  name_ext[128] = "_exec2obj_userapp_code_ptr";
    char  buf[MAX_EXECNAME_LEN];

    /* If there are no executables specified it's an error. */
    if (argc < 2) {
        print_usage();
        return -1;
    }

    if (argc > MAX_NUM_APP_ENTRIES + 1) {
        printf("Too many executables:  The maximum is %d\n",
               MAX_NUM_APP_ENTRIES);
    }

    /* Open output file. */
    out = fopen("user_apps.c", "w");
    if (!out) {
        printf("Could not create output file user_apps.c.\n");
        return -1;
    }

    /* Output include. */
    fprintf(out, "#include \"exec2obj.h\"\n");

    while (file_iter < argc) {
        /* Open the executable. */
        in = fopen(argv[file_iter], "r");
        if (!in) {
            printf("Could not open %s for reading.\n", argv[file_iter]);
            fclose(out);
            return -1;
        }

        /* Find size of executable. */
        fseek(in, 0, SEEK_END);
        execsize = ftell(in);
        fseek(in, 0, SEEK_SET);

        execsizes[file_iter] = execsize;

        /* Output the header of the file. */
        fprintf(out,
                "const char %s%s[%d] = {",
                argv[file_iter],
                name_ext,
                execsize);

        if (!fread(&bucket, 1, 1, in)) {
            printf("The input executable is empty!\n");
            return -1;
        } else
            fprintf(out, "%hhd", bucket);

        /* Output the file. */
        while (fread(&bucket, 1, 1, in)) fprintf(out, ",%hhd", bucket);

        fprintf(out, "};\n\n");
        fclose(in);

        /* Move on to the next file. */
        file_iter++;
    }

    /* Output the table of contents. */
    file_iter = 1;
    fprintf(out, "const int exec2obj_userapp_count = %d;\n", argc - 1);
    fprintf(out,
            "const exec2obj_userapp_TOC_entry exec2obj_userapp_TOC[%d] = {",
            MAX_NUM_APP_ENTRIES);

    /* Output first entry in TOC. */
    strncpy(buf, ".", MAX_EXECNAME_LEN - 1);
    buf[MAX_EXECNAME_LEN - 1] = '\0';
    fprintf(out,
            "{\"%s\",%s%s,%d}\n",
            buf,
            argv[file_iter],
            name_ext,
            execsizes[file_iter]);
    file_iter++;

    /* Output all other entries in TOC. */
    while (file_iter < argc) {
        fprintf(out,
                ",{\"%s\",%s%s,%d}\n",
                argv[file_iter],
                argv[file_iter],
                name_ext,
                execsizes[file_iter]);
        file_iter++;
    }

    /* End TOC. */
    fprintf(out, "};\n");

    fclose(out);
    printf("The file user_apps.c was created successfully.\n");
    return 0;
}
