#ifndef __ENVIRONMENT_H
#define __ENVIRONMENT_H

#include <stdarg.h>

// FIXME: Move this to a more sensible location
#define DEBUG_STRING_LIMIT 256

void environment_vprintf(const char *fmt, va_list ap);
void environment_printf(const char *fmt, ...)
    __attribute__((__format__(__printf__, 1, 2)));

#define lprintf(...) environment_printf(__VA_ARGS__)

#endif
