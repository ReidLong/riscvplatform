410U_MISC_OBJS := environment.o

410U_MISC_OBJS := $(410U_MISC_OBJS:%=$(410UDIR)/libmisc/%)

ALL_410UOBJS += $(410U_MISC_OBJS)
410UCLEANS += $(410UDIR)/libmisc.a

$(410UDIR)/libmisc.a: $(410U_MISC_OBJS)
