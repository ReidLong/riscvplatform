
void *memcpy(void *__to, const void *__from, unsigned int __n) {
    char *to   = (char *)__to;
    char *from = (char *)__from;

    while (__n-- > 0) *to++ = *from++;

    return __to;
}
