/** @file boot/entry.c
 *  @brief Entry point for the C part of the kernel
 *  @author matthewj S2008
 *  @author elly1 S2010
 */

#include <assert.h>
#include <common_kern.h>
#include <lmm/lmm.h>
#include <lmm/lmm_types.h>
#include <malloc/malloc_internal.h>
#include <riscv/page.h>
#include <stdint.h>

/* 410 interface compatibility */
static const int32_t n_phys_frames = (64 * 1024 * 1024) / PAGE_SIZE;


int main(void) {
    // LMM init, short form
    static struct lmm_region region;
    extern char              end[];
    lmm_init(&malloc_lmm);
    lmm_add_region(&malloc_lmm, &region, 0, USER_MEM_START, 0, 1);  // kvm
    lmm_add_free(
        &malloc_lmm, end, (char *)USER_MEM_START - end);  // available kvm

    assert(n_phys_frames > (USER_MEM_START / PAGE_SIZE));

    return kernel_main();
}

int machine_phys_frames(void) {
    return (n_phys_frames);
}
