#ifndef __ENVIRONMENT_LOCK
#define __ENVIRONMENT_LOCK

#include <stdbool.h>

typedef struct {
    bool supervisorMode;
    bool interruptsEnabled;
} EnvironmentLock_t;

void environmentLock_init(EnvironmentLock_t* lock, bool supervisorMode);
void environmentLock_lock(EnvironmentLock_t* lock);
void environmentLock_unlock(EnvironmentLock_t* lock);

#endif