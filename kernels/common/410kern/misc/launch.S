#include <ureg.h>

.global launch_mret
launch_mret:
    mv sp, a0
    mret

.global launch_sret
launch_sret:
    mv sp, a0
    sret

.global launch_ureg_mret
launch_ureg_mret:
    mv sp, a0
    POP_UREG();
    mret

.global launch_ureg_sret
launch_ureg_sret:
    mv sp, a0
    POP_UREG();
    sret

