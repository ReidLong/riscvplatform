#ifndef __LAUNCH_H
#define __LAUNCH_H

#include <stdint.h>
#include <ureg.h>

void launch_mret(uint64_t* stack);
void launch_sret(uint64_t* stack);

void launch_ureg_mret(ureg_t* regs);
void launch_ureg_sret(ureg_t* regs);

#endif