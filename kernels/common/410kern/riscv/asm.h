#ifndef RISCV_ASM_H
#define RISCV_ASM_H

#include <riscv_csr.h>
#include <stdbool.h>
#include <stdint.h>

// Machine Mode Assembly Wrappers

void disable_machine_interrupts(void);
void enable_machine_interrupts(void);
bool machine_interrupts_enabled(void); /* riscv_status.c */

mstatus_t set_mstatus(mstatus_t mstatus);
mstatus_t get_mstatus(void);

void*    set_mtvec(void* base);
uint32_t set_mideleg(uint32_t mideleg);
uint32_t set_medeleg(uint32_t medeleg);

uint32_t set_mscratch(uint32_t scratch);
uint32_t get_mscratch(void);
uint32_t set_mepc(uint32_t mepc);
uint32_t get_mepc(void);
uint32_t get_mcause(void);
uint32_t get_mtval(void);

void     hardware_halt(uint32_t x1);
uint32_t ecall(uint32_t a0);

// Supervisor Mode Assembly Wrappers

void disable_supervisor_interrupts(void);
void enable_supervisor_interrupts(void);
bool supervisor_interrupts_enabled(void); /* riscv_status.c */

// This is cheeky. Invalid fields will be set to zero.
mstatus_t set_sstatus(mstatus_t sstatus);
mstatus_t get_sstatus(void);

void* set_stvec(void* base);

uint32_t set_sscratch(uint32_t scratch);
uint32_t get_sscratch(void);
uint32_t set_sepc(uint32_t mepc);
uint32_t get_sepc(void);
uint32_t get_scause(void);
uint32_t get_stval(void);

// Toggles the SUM bit in the sstatus register
void disable_supervisor_user_access(void);
void enable_supervisor_user_access(void);

satp_t set_satp(satp_t satp);
satp_t get_satp(void);

void sfence_vma_all(void);
void sfence_vma(void* vaddr);

void wait_for_interrupt(void);


#endif /* !X86_ASM_H */
