#ifndef __RISCV_STATUS_H
#define __RISCV_STATUS_H

#define USER_MODE 0
#define SUPERVISOR_MODE 1
#define MACHINE_MODE 3

#include <stdbool.h>

void set_mpp(uint32_t mode);
void set_spp(uint32_t mode);

bool machine_interrupts_enabled(void);
bool supervisor_interrupts_enabled(void);

#endif
