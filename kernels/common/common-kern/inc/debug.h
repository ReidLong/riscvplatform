//# PRINT AFTER inc/log.h
#ifndef __DEBUG_H
#define __DEBUG_H

#include <environment.h>

#ifdef EXTRA_DEBUG

#define WARN_UNLESS(COND, value)                                               \
    do {                                                                       \
        if (!(COND)) {                                                         \
            lprintf("ASSERTION FAILURE|%s:%d| %s [%#lx]",                      \
                    __FILE__,                                                  \
                    __LINE__,                                                  \
                    #COND,                                                     \
                    value);                                                    \
        }                                                                      \
    } while (0)

#else

#define WARN_UNLESS(COND, value) ((void)0)

#endif


#endif
