#include "fifo.h"
#include "keyboard.h"
#include <stdlib.h>

#define BUFFER_SIZE 512
static FIFO_t fifo;

int keyboardDriver_init(void) {
    return fifo_init(&fifo, BUFFER_SIZE);
}

void keyboardDriver_handler(uint32_t ascii) {
    int result = fifo_enqueue(&fifo, (char)ascii);
    if (result != SUCCESS) {
        // Not actually a panic, but for now this is fine
        panic("Unable to insert character: %c", (char)ascii);
    }
}

int keyboardDriver_readchar(void) {
    char data;

    int result = fifo_dequeue(&fifo, &data);

    if (result == SUCCESS) {
        return data;
    } else {
        return -1;
    }
}