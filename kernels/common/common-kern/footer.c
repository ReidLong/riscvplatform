
#include "footer.h"
#include "console.h"
#include "csr.h"
#include "timer.h"
#include <contracts.h>
#include <stdio.h>

void footer_draw(int32_t row) {
    REQUIRES(row + 3 <= CONSOLE_HEIGHT);

    // Save State
    uint8_t originalColor;
    int32_t originalRow, originalCol;
    console_getTermColor(&originalColor);
    console_getCursor(&originalRow, &originalCol);

    // Get ready for footer
    console_setTermColor(FGND_BGRN);
    console_setCursor(row, 0);

    // Update the timer
    char timer[CONSOLE_WIDTH];
    timer_toString(timer_getTicks(), timer, CONSOLE_WIDTH);
    printf("Time Since Boot: %s\n", timer);

    // Update the performance counters
    uint64_t cycles  = csr_getCycles();
    uint64_t retired = csr_getInstructionsRetired();
    uint64_t ipc     = retired * 100 / cycles;
    // printf("Cycles:  %#16llx Instructions Retired: %#16llx\n", cycles,
    // retired);
    printf("Cycles:  %#16llx Instructions Retired: %#16llx IPC: 0.%02lld\n",
           cycles,
           retired,
           ipc);
    uint64_t iHit  = csr_getICacheHit();
    uint64_t iMiss = csr_getICacheMiss();
    uint64_t dHit  = csr_getDCacheHit();
    uint64_t dMiss = csr_getDCacheMiss();
    uint64_t dVRAM = csr_getDVRAMHit();
    uint64_t dRate = (dHit + dVRAM) * 100 / (dHit + dVRAM + dMiss);
    uint64_t iRate = (iHit * 100) / (iHit + iMiss);
    printf("Data Cache Hit: %8lld%% Instruction Cache Hit: %14lld%%",
           dRate,
           iRate);


    // Restore Console State
    console_setTermColor(originalColor);
    console_setCursor(originalRow, originalCol);
}