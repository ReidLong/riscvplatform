



.equ a1_val, 0x42
.equ a2_val, 0xffffffff
.equ a3_val, 0xc001d00d
.equ a4_val, 0x9999abcd
.equ a5_val, 0x12345678
.equ a6_val, 0x87654321
.equ a7_val, 0xbaaaaaaa
.equ t6_val, 0x20

.align 4
saved_sp:
    .space 4

# extern int wait(int* until);
.global wait
wait:
    la t0, saved_sp
    sw sp, 0(t0)
    li t0, 0x0
    li t3, 0x0
    # Magic registers
    li a1, a1_val
    li a2, a2_val
    li a3, a3_val
    li a4, a4_val
    li a5, a5_val
    li a6, a6_val
    li a7, a7_val
    li t6, t6_val
loop_guard:
    lw t1, 0(a0)
    bne t1, zero, done
loop_body:

    # Save all of the caller saved registers
    addi sp, sp, -0x50
    sw ra, 0x4c(sp)
    sw s0, 0x44(sp)
    sw t0, 0x0(sp)
    sw t1, 0x4(sp)
    sw a0, 0xc(sp)
    sw a1, 0x10(sp)
    sw a2, 0x14(sp)
    sw a3, 0x18(sp)
    sw a4, 0x1c(sp)
    sw a5, 0x20(sp)
    sw a6, 0x24(sp)
    sw a7, 0x28(sp)
    sw t3, 0x2c(sp)
    sw t4, 0x30(sp)
    sw t5, 0x34(sp)
    sw t6, 0x38(sp)
    addi s0, sp, 0x50
    call doWork
    mv t2, a0
    lw ra, 0x4c(sp)
    lw s0, 0x44(sp)
    lw t0, 0x0(sp)
    lw t1, 0x4(sp)
    lw a0, 0xc(sp)
    lw a1, 0x10(sp)
    lw a2, 0x14(sp)
    lw a3, 0x18(sp)
    lw a4, 0x1c(sp)
    lw a5, 0x20(sp)
    lw a6, 0x24(sp)
    lw a7, 0x28(sp)
    lw t3, 0x2c(sp)
    lw t4, 0x30(sp)
    lw t5, 0x34(sp)
    lw t6, 0x38(sp)
    addi sp, sp, 0x50

    bne t2, zero, bad_work


check_sp:
    la t4, saved_sp
    lw t5, 0(t4)
    bne t5, sp, bad_sp

check_magic:
    li t2, a1_val
    bne a1, t2, bad_register
    li t2, a2_val
    bne a2, t2, bad_register
    li t2, a3_val
    bne a3, t2, bad_register
    li t2, a4_val
    bne a4, t2, bad_register
    li t2, a5_val
    bne a5, t2, bad_register
    li t2, a6_val
    bne a6, t2, bad_register
    li t2, a7_val
    bne a7, t2, bad_register
    li t2, t6_val
    bne t6, t2, bad_register

    addi t0, t0, 0x1
    j loop_guard
done:
    mv a0, t0
    ret

bad_sp:
    li a0, -1
    ret
bad_register:
    li a0, -2
    ret
bad_work:
    mv a0, t2
    ret
