/** @file game_endGame.h
 * @brief Defintions for the END_GAME state for minesweeper
 *
 * @author Reid Long (relong)
 * @bug
 */

#ifndef __GAME_END_H
#define __GAME_END_H

#include "game.h"

void over_output(void);

GameState_t over_nextState(int key);

#endif
