/** @file game_title.h
 * @brief Defintions for the title state for minesweeper
 *
 * @author Reid Long (relong)
 * @bug
 */

#ifndef __GAME_TITLE_H
#define __GAME_TITLE_H

#include "game.h"

GameState_t title_nextState(int key);

void title_output(void);

#endif
