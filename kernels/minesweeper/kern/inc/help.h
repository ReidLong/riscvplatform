/** @file game_help.h
 * @brief Defintions for the help state for minesweeper
 *
 * @author Reid Long (relong)
 * @bug
 */

#ifndef __GAME_HELP_H
#define __GAME_HELP_H

#include "game.h"


void help_output(void);

GameState_t help_nextState(int key);

#endif
