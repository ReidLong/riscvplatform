/** @file game_endGame.c
 * @brief Implementation of the End Game state for minesweeper
 *
 * @author Reid Long (relong)
 * @bug
 */

#include "console.h"
#include "footer.h"
#include "game.h"
#include <stdio.h>
#include <video_defines.h>

#define END_GAME_COLOR (FGND_BLUE | BGND_BLACK)

/** @brief Impements the next state logic of the End Game state
 *
 * @param key the key that was most recently pressed
 * @return the next gameState to go to
 */
GameState_t over_nextState(int key) {
    (void)key;
    // Always go back to the beginning!
    return TITLE;
}

/** @brief Displays the end game screen */
void over_output(void) {
    console_setTermColor(END_GAME_COLOR);
    console_clear();
    printf("\n\n");
    printf("                CONGRATULATIONS! YOU BEAT EVERY LEVEL!!!!!!\n");
    printf("\n\n");
    printf("                Now..... Can you do it faster?\n");
    printf("\n\n");
    printf("                Press any key to play again!\n");

    footer_draw(DEFAULT_FOOTER);
}