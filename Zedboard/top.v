module top (
    // Basic IO
    input wire [7:0] switch,
    output wire [7:0] led,
    input wire [4:0] button,

        // VGA
    output wire [3:0] vga_r, vga_g, vga_b,
    output wire vga_vs, vga_hs,
    input wire vgaClock, vgaClear_n,
    input wire vramClock,

    // PS/2
    inout wire ps2_clk, ps2_data,

    input wire clock, areset_n
);


endmodule