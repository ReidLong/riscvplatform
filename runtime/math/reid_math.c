
typedef unsigned long long u_quad_t; /* 64 bits, unsigned */

#define RETURN_RESULT(quotient, remainder)                                     \
    do {                                                                       \
        if (r != 0)                                                            \
            *r = remainder;                                                    \
        return quotient;                                                       \
    } while (0)


// a / b
u_quad_t __reid_udivrem(u_quad_t a, u_quad_t b, u_quad_t* r) {
    if (b == 0) {
        RETURN_RESULT(-1, a);
    } else if (b > a) {
        RETURN_RESULT(0, a);
    } else if (a == b) {
        RETURN_RESULT(1, 0);
    } else {
        u_quad_t quotient  = 0;
        u_quad_t remainder = 0;
        for (int i = sizeof(quotient) * 8 - 1; i >= 0; i--) {
            remainder <<= 1;
            remainder |= (a >> i) & 0x1;
            if (remainder >= b) {
                remainder -= b;
                quotient |= 0x1 << i;
            }
        }
        RETURN_RESULT(quotient, remainder);
    }
}

// unsigned int __udivsi3(unsigned int a, unsigned int b) {
//     return (unsigned int)__reid_udivrem((u_quad_t)a, (u_quad_t)b, 0);
// }

u_quad_t __udivdi3(u_quad_t a, u_quad_t b) {
    return __reid_udivrem(a, b, 0);
}

// ftp://ftp3.us.freebsd.org/pub/NetBSD/misc/joerg/GENERIC/src/src/sys/external/bsd/compiler_rt/dist/lib/builtins/divsi3.c.html
// int __divsi3(int a, int b) {
//     const int bits_in_word_m1 = (int)(sizeof(int) * 8) - 1;
//     int       s_a             = a >> bits_in_word_m1; /* s_a = a < 0 ? -1 : 0
//     */ int       s_b             = b >> bits_in_word_m1; /* s_b = b < 0 ? -1
//     : 0 */ a                         = (a ^ s_a) - s_a;      /* negate if s_a
//     == -1 */ b                         = (b ^ s_b) - s_b;      /* negate if
//     s_b == -1 */ s_a ^= s_b;                                       /* sign of
//     quotient */
//     /*
//      * On CPUs without unsigned hardware division support,
//      *  this calls __udivsi3 (notice the cast to su_int).
//      * On CPUs with unsigned hardware division support,
//      *  this uses the unsigned division instruction.
//      */
//     return ((unsigned)a / (unsigned)b ^ s_a) - s_a; /* negate if s_a == -1 */
// }

// return a % b;
u_quad_t __umoddi3(u_quad_t a, u_quad_t b) {
    u_quad_t r;
    __reid_udivrem(a, b, &r);
    return r;
}

// unsigned int __umodsi3(unsigned int a, unsigned int b) {
//     u_quad_t r;
//     __reid_udivrem((u_quad_t)a, (u_quad_t)b, &r);
//     return (unsigned int)r;
// }


// ftp://ftp3.us.freebsd.org/pub/NetBSD/misc/joerg/GENERIC/src/src/sys/external/bsd/compiler_rt/dist/lib/builtins/modsi3.c.html
// int __modsi3(int a, int b) {
//     return a - __divsi3(a, b) * b;
// }

// int __mulsi3(int a, int b) {
//     /* Dumb Version
//     int result = 0;
//     while (a > 0) {
//         result += b;
//         a--;
//     }
//     return result;
//     */
//     // Ancient Egyptian Multiplication
//     // https://en.wikipedia.org/wiki/Ancient_Egyptian_multiplication
//     long long result = 0;
//     int       sign   = 0;
//     long long x, y;
//     if (a < 0) {
//         sign = !sign;
//         x    = -(long long)a;
//     } else {
//         x = a;
//     }
//     if (b < 0) {
//         sign = !sign;
//         y    = -(long long)b;
//     } else {
//         y = b;
//     }

//     while (y > 0) {
//         if (y & 1) {
//             result += x;
//         }
//         x <<= 1;
//         y >>= 1;
//     }
//     if (sign) {
//         result = -result;
//     }
//     return (int)result;
// }