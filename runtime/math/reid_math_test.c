
#include <stdio.h>

typedef unsigned long long u_quad_t; /* 64 bits, unsigned */

extern u_quad_t __reid_udivrem(u_quad_t a, u_quad_t b, u_quad_t* r);


static int test(u_quad_t a, u_quad_t b_low, u_quad_t b_high) {
    for (u_quad_t b = b_low; b <= b_high; b++) {
        u_quad_t remainder = -1;
        u_quad_t result    = __reid_udivrem(a, b, &remainder);

        if (result != a / b) {
            printf("a: %llx b: %llx result: %llx remainder: %llx\n",
                   a,
                   b,
                   result,
                   remainder);
            return 1;
        }

        if (remainder != a % b) {
            printf("a: %llx b: %llx result: %llx remainder: %llx\n",
                   a,
                   b,
                   result,
                   remainder);
            return 2;
        }

        if (b == b_high) {
            break;
        }
    }

    return 0;
}


int main(void) {
    test(0llu, 1llu, 0xFFFFFFFFFFllu);
    test(1llu, 1llu, 0xFFFFFFFFFFllu);

    for (u_quad_t a = 0x7F000000llu; a <= 0xFFFFFFFFFFllu; a++) {
        test(a, 1llu, 1000llu);
        test(a, 0x7F000000llu, 0xFFFFFFFFFFllu);
    }

    for (u_quad_t a = 0x7FFFFF0000000000llu; a <= 0xFFFFFFFFFFFFFFFF; a++) {
        test(a, 1llu, 1000llu);
        test(a, 0x7F000000llu, 0xFFFFFFFFFFllu);
    }

    return 0;
}
