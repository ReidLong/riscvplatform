#!/bin/bash

set -e

printf "Arg: $1 $2\n"

if  [[ $1 != kernels/* ]] ;
then
    exit 1
fi

cd $1

LINK_PATHS='410kern 410user spec'

for file in $LINK_PATHS; do
printf "Linking $file\n";
rm -f $file;
ln -s $2/$file;
done

printf "Linking Successful $1"

