#!/bin/python3

import sys
import os
import shutil
import subprocess
import serial

if len(sys.argv) < 4:
    print('Usage: {} <version> <board_name> <sdk_bin> <output.reg>'.format(sys.argv[0]))
    exit(2)


versionFolder = sys.argv[1]
board_name = sys.argv[2]
sdk_bin = sys.argv[3]
output_path = sys.argv[4]

projectRoot = os.getcwd()
vivadoRoot = os.path.join(projectRoot, 'vivado')
versionPath = os.path.join(vivadoRoot, versionFolder)

def run_sdk(path):
    print('\nExecuting: {} SDK: {}'.format(path, sdk_bin))
    child = subprocess.Popen(['cmd.exe'], cwd=versionPath, stderr=subprocess.STDOUT, stdin=subprocess.PIPE)
    try:
        child.communicate(input='set PATH=%PATH%;{}\n xsdk -batch -source {}\n'.format(sdk_bin, path).encode(), timeout=1)
    except subprocess.TimeoutExpired:
        pass
    return child

# Deploy the code to the board
deploy = run_sdk('sdk_deploy.tcl')
deploy.wait()
print('\nDeploy: {}'.format(deploy.returncode))

dump = []
dumping = False
# connect to the serial terminal
with serial.Serial('/dev/ttyS4', 115200, timeout=60) as ser:
    # Start the binary
    child = run_sdk('sdk_run.tcl')
    while True:
        raw_line = ser.readline()
        # print('[{}] {}'.format(len(raw_line), raw_line))

        if len(raw_line) == 0:
            print('Giving up: {}'.format(child.returncode))
            try:
                child.kill()
            except:
                print('Unable to kill child')
            exit(1)

        line = raw_line.decode("utf-8").strip('\n\r\t ')
        print('+ {}'.format(line))
        if "***EXIT***" in line:
            print("EXITING")
            break
        elif '***DUMP***' in line:
            dumping = True
        elif dumping:
            dump.append(line)
    try:
        child.kill()
    except:
        print('Unable to kill child')

output_reg = open(output_path, 'w')

for line in dump:
    print('>{}'.format(line))
    output_reg.write(line)
    output_reg.write('\n')

output_reg.close()