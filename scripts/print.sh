#!/bin/bash

set -e

ROOT=$1
OUTPUT=$2
NAME=$3

echo "Printing ${ROOT}"

files=$(python3 scripts/make-list.py ${ROOT})
printf '%s\n' "${files[@]}" > ${OUTPUT}/${NAME}.list

./scripts/print-files.sh $NAME $files > ${OUTPUT}/${NAME}.ps

