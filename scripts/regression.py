#!/usr/bin/python

_447TestsPath = 'tests/447'
_447Tests = [
    "additest.S",
    "addtest.S",
    "arithtest.S",
    "brtest0.S",
    "brtest1.S",
    "brtest2.S",
    "depend.S",
    "dependLow.S",
    "memtest0.S",
    "memtest1.S",
    "shifttest.S",
    "fib.c",
    "matrix_mult.c",
    # "syscalltest.S",
]

handWrittenPath = 'tests/hand_written'

handWrittenTests = [
    "additest-nop.S",
    "additest-safe.S",
    "additest-stall.S",
    "bad-store.S",
    "jumptest-2.S",
    "jumptest-basic.S",
    "predictTest.S",
    "small-arithtest.S",
    "test-branch.S",
    "test-branch2.S",
    "test-branch3.S",
    "test-branch5.S",
    "test-jump.S",
    "test-loop-small.S",
    "test-loop.S",
    "test-loop2.S",
    "test-loop3.S",
    "test-loop4.S",
    "test-tricky-branch.S",
    "simple_memory.S",
    "atomic_store.S",
    "atomic_load.S",
    # "mul-test.S",
    # "mulh-test.S",
    # "mulhu-test.S",
    # "mulhsu-test.S",
    # "div-test.S",
    # "divu-test.S",
    # "rem-test.S",
    # "remu-test.S",
    "test-40-debug.S",
    "csr_test.S",
    "csr_test2.S",
    "csr_test3.S",
    'timer_tick.S',
    'timer_tick2.S',
    'math_ull.c',
    'math_ll.c',
    'math_ul.c',
    'math_l.c',
    'satp_test.S',
    'instruction_fault_test.S',
    'csr_test4.S',
    'load_fault.S',
    'sfence.S',
    'sfence_all.S',
    'sfence_nop.S',
    'sfence_all_nop.S',
    'cache_test.S',
    'simple_recur.c',
    'csr_test_s.S',
    'hello_world.S',
]

handWrittenTests += ['B/test-B-{}.S'.format(i) for i in xrange(1, 21)]

benchmarkPath = "tests/benchmarks"

benchmarks = [
    "fibi.c",
    "fibm.c",
    "fibr.c",
    "mixed.c",
]

generatedPath = 'tests/generated'

generatedTests = ['test-{}.S'.format(i) for i in xrange(1,69)]

kernels = [
    'hello',
    'dumper',
    'console',
    'tick_tock_m',
    'tick_tock_s',
    'rainbow',
    'station_m',
    'station_s',
    'simple',
    'timing_m',
    'timing_s',
    'link_check',
    'teeny',
    'vast_x',
    'fondle_s',
    'asm_test',
    'double_interrupt_m',
    'double_interrupt_s',
    'vm_alias',
    'cow',
    # Long running tests
    'interrupt_test2_m',
    'interrupt_test2_s',
    'interrupt_test_m',
    'interrupt_test_s',
    'tick_tock_registers_m',
    'tick_tock_registers_s',
    'memcmp',
    'memcmp_vm',
    'logos',
]

kernelTests = ['kernels/{}/{}.mk'.format(k, k) for k in kernels]


import subprocess
import re
import sys
import os

def executeTest(test, script):
    # bashCommand = 'touch {}.log'.format(os.path.splitext(test)[0])
    bashCommand = 'make assemble TEST={}'.format(test)
    exit_status = subprocess.call(bashCommand.split())
    if exit_status != 0:
        print bashCommand
        exit(exit_status)
    bashCommand = 'make {} TEST={} OUTPUT=output_test'.format(script, test)
    print bashCommand
    exit_status = subprocess.call(bashCommand.split())
    if exit_status != 0:
        print('Test: {} failed!'.format(test))
        exit(exit_status)

def generateGolden(test, ignored):
    bashCommand = './generateGolden.py {}'.format(test)
    exitStatus = subprocess.call(bashCommand.split())
    if exitStatus != 0:
        print bashCommand
        exit(exitStatus)

def runTestSet(testArray, path, script):
    for file in testArray:
        if path:
            executeTest('{}/{}'.format(path, file), script)
        else:
            executeTest(file, script)
        # generateGolden('{}/{}'.format(path, file), script)


if len(sys.argv) < 2:
    print('USAGE: {} [arch-verify/verify]'.format(sys.argv[0]))
    exit(2)

RUN_MODES = ['arch-verify', 'verify', 'arch-run', 'sdk-run']

if sys.argv[1] in RUN_MODES:
    command = '{} {}'.format(sys.argv[1], sys.argv[2] if len(sys.argv) > 2 else '')
    runTestSet(_447Tests, _447TestsPath, command)
    runTestSet(handWrittenTests, handWrittenPath, command)
    runTestSet(benchmarks, benchmarkPath, command)
    runTestSet(generatedTests, generatedPath, command)
    runTestSet(kernelTests, None, command)
    print "All tests passed"






